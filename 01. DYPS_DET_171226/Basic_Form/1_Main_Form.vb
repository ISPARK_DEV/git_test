﻿Imports INFITF
Imports PLMAccessIDLItf
Imports ProductStructureClientIDL
Imports VPMEditorContextIDL
Imports CATEngConnectionIDL
Imports PLMApplicationContextIDL
Imports PLMModelerBaseIDL
Imports HybridShapeTypeLib              'HybridShapeFactory
Imports MECMOD                          'AxisSystem
'Imports System.Runtime.InteropServices.COMException


Public Class Main_Form

#Region "---------- DLL 참조 "
    'Declare Function SetCursorPos Lib "user32" _
    '    (ByVal x As Long, ByVal y As Long) As Long
    '----------------------------------------------------------------------- 윈도우 창 찾기.
    Private Declare Function FindWindow Lib "user32.dll" Alias "FindWindowA" _
        (ByVal IpClassName As String, _
         ByVal IpWindowName As String) As IntPtr
    '----------------------------------------------------------------------- 찾은 윈도우 창 안에 세부내역 찾기.
    Private Declare Auto Function FindWindowEx Lib "user32.dll" Alias "FindWindowExA" _
        (ByVal hwndParent As IntPtr, _
         ByVal hwndChildAfter As IntPtr, _
         ByVal lpszClass As String, _
         ByVal lpszWindow As String) As IntPtr
    '----------------------------------------------------------------------- 이벤트 만들기.
    Private Declare Function PostMessage Lib "user32" Alias "PostMessageA" _
        (ByVal Hwnd As IntPtr, _
         ByVal wMsg As UInteger, _
         ByVal wParam As UInteger, _
         ByVal lParam As UInteger) As UInteger
    '----------------------------------------------------------------------- 
    Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal Hwnd As IntPtr, _
                                                                            ByVal wMsg As Integer, _
                                                                            ByVal wParam As IntPtr, _
                                                                            ByVal lParam As IntPtr) As Int32
    '----------------------------------------------------------------------- 
    Public Const WM_PASTE As Long = &H302
    Public Const BM_CLICK As Integer = &HF5
    Public oF3_Key As String = "F3"
#End Region

#Region "---------- 타워 높이 "
    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    '  기본정보 갱신 버튼.
    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    Private Sub BTN_BASIC_INFO_Click(sender As Object, e As EventArgs) Handles BTN_BASIC_INFO.Click
        Try
            Call PROGRESSBAR_CHANGE("기본정보 갱신 중...", 30)

            '---------- ---------- ---------- ---------- ---------- ---------- 기본정보 엑셀 값 가져오기
            Dim Exist_Result = THISFILEEXIST(TXT_EXCEL_PATH.Text, TXT_EXCEL_FILE_NAME.Text)
            If Exist_Result = "" Then
                Dim MSG_Content As String = "기본정보 엑셀을 찾을 수 없어 종료합니다!"
                MsgBox(MSG_Content & vbCrLf & _
                       "-> " & TXT_EXCEL_PATH.Text & "\" & TXT_EXCEL_FILE_NAME.Text, _
                       vbOKOnly Or vbInformation, "ISPark CATIA Automation")
                Exit Sub
            End If

            Call OPEN_EXCEL_FILE(TXT_EXCEL_PATH.Text, TXT_EXCEL_FILE_NAME.Text, False)
            Call GET_BASIC_INFO()
            Call CLOSE_EXCEL(False, "")
            Call PROGRESSBAR_CHANGE("기본정보 갱신 완료.", 100)
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "ISPark CATIA Automation")
        End Try
    End Sub

    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    '  템플릿 열기 버튼.
    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    Private Sub BTN_TEMPLATE_OPEN_Click(sender As Object, e As EventArgs) Handles BTN_TEMPLATE_OPEN.Click
        On Error Resume Next
        Call PROGRESSBAR_CHANGE("템플릿 데이터 가져오는 중...", 30)

        '---------- ---------- ---------- ---------- ---------- ---------- 불러올 파일들.
        Dim tmpArr(3)
        'tmpArr(0) = oActiveObject.PLMEntity.GetAttributeValue("V_Name")
        tmpArr(0) = TXT_ASSY.Text
        tmpArr(1) = TXT_SKEL.Text
        tmpArr(2) = TXT_PALLET_VIRTUAL.Text

        '---------- ---------- ---------- ---------- ---------- ---------- Exist Component.
        Call CATIA_SEARCH(sSearch_Type, tmpArr(0), True)
        Call CATIA_BASIC_V6()
        oSelection.Clear()
        For i = 1 To oEntity.Instances.Count
            oSelection.Add(oEntity.Instances.Item(i))
        Next
        oSelection.Delete()
        oSelection.Clear()

        Dim MyContext = CATIA.ActiveEditor.GetService("PLMProductContext")
        Dim myMCXParent As VPMReference = MyContext.RootOccurrence.ReferenceRootOccurrenceOf
        Dim RootInstanceConnections As EngConnections = myMCXParent.GetItem("CATEngConnections")
        For i = 1 To RootInstanceConnections.Count
            oSelection.Add(RootInstanceConnections.Item(i))
        Next
        oSelection.Delete()

        For i = 1 To 2
            Call CATIA_SEARCH(sSearch_Type, tmpArr(i), True)

            If Not i = 0 Then
                Call CATIA_BASIC_V6()

                oSelection.Clear()
                oSelection.Add(oActiveObject.parent.father)
                oSelection.Copy()
                oSelection.Clear()

                Call ACTIVATE_NEXT_WINDOW(tmpArr(0) & " A.1", False)
                Call CATIA_BASIC_V6()

                oSelection.Clear()
                oSelection.Add(oActiveObject)
                oSelection.Paste()
                oSelection.Clear()

                Call ACTIVATE_NEXT_WINDOW(tmpArr(i) & " A.1", True)
            End If
        Next

        '---------- ---------- ---------- ---------- ---------- ---------- 워크벤치 체크.
        If Not CATIA.GetWorkbenchId = "Assembly" Then CATIA.StartWorkbench("Assembly")

        '---------- ---------- ---------- ---------- ---------- ---------- Fit All In.
        Dim Viewer As Viewer = CATIA.ActiveWindow.ActiveViewer.Reframe()

        '---------- ---------- ---------- ---------- ---------- ---------- Save.
        CATIA.GetSessionService("PLMPropagateService").Save()

        Call PROGRESSBAR_CHANGE("템플릿 데이터 가져오기 완료.", 100)
        On Error GoTo 0
    End Sub

    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    '  높이 계산 버튼 클릭.
    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    Private Sub BTN_CALCULATION_Click(sender As Object, e As EventArgs) Handles BTN_CALCULATION.Click
        Try
            Dim iTmp = Val(TXT_TOP_SD.Text) + Val(TXT_TOP_RV.Text)
            If iTmp = 0 Then
                Dim MSG_Content As String = "입력된 차량 댓수를 확인 후 다시 실행해 주세요."
                MsgBox(MSG_Content, vbOKOnly Or vbInformation, "ISPark CATIA Automation")
                Call PROGRESSBAR_CHANGE("ERROR : " & MSG_Content, 50)
                Exit Sub
            End If

            '---------- ---------- ---------- ---------- ---------- ---------- 차량 댓수가 짝수가 아니면 에러..
            Dim tmpNum As Integer

            tmpNum = IS_EVEN_NUMBER(TXT_TOP_SD)
            If Not tmpNum = 0 Then Exit Sub
            tmpNum = IS_EVEN_NUMBER(TXT_TOP_RV)
            If Not tmpNum = 0 Then Exit Sub
            tmpNum = IS_EVEN_NUMBER(TXT_BOTTOM_SD)
            If Not tmpNum = 0 Then Exit Sub
            tmpNum = IS_EVEN_NUMBER(TXT_BOTTOM_RV)
            If Not tmpNum = 0 Then Exit Sub

            '---------- ---------- ---------- ---------- ---------- ---------- 템플릿이 없으면 에러.
            Dim Exist_Doc As Boolean = CATIA_BASIC_V6()
            If Exist_Doc = False Then
                Dim MSG_Content As String = "템플릿 파일을 가져온 후 다시 실행해 주세요."
                MsgBox(MSG_Content, vbOKOnly Or vbInformation, "ISPark CATIA Automation")
                Call PROGRESSBAR_CHANGE("ERROR : " & MSG_Content, 50)
                Exit Sub
            Else

            End If

            '---------- ---------- ---------- ---------- ---------- ---------- 워크벤치 체크.
            If Not CATIA.GetWorkbenchId = "Assembly" Then CATIA.StartWorkbench("Assembly")

            Call PROGRESSBAR_CHANGE("타워 높이 계산 중...", 20)

            TXT_PIT.Text = PIT_CALCULATION()
            TXT_EH.Text = EH_CALCULATION()
            TXT_GH.Text = GH_CALCULATION()
            TXT_OH.Text = OH_CALCULATION()
            TXT_RESULT.Text = Val(TXT_PIT.Text) + Val(TXT_BM.Text) + Val(TXT_EH.Text) + Val(TXT_GH.Text) + Val(TXT_OH.Text)

            Call PROGRESSBAR_CHANGE("타워 위치 수정 중...", 50)
            Call TOWER_POSITIONING()
            Call PROGRESSBAR_CHANGE("타워 위치 수정 중...", 80)
            CATIA.StartCommand("Update")

            Dim Viewer As Viewer = CATIA.ActiveWindow.ActiveViewer.Reframe()

            Call PROGRESSBAR_CHANGE("타워 높이 계산 완료.", 100)
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "ISPark CATIA Automation")
        End Try
    End Sub
#End Region

#Region "---------- Calculation "
    '---------- ---------- ---------- ---------- ---------- ---------- 엑셀에 옵션 갯수.
    Public iPIT = 3, iEH = 2, iGH = 2, iOH = 2
    Public iLIFT = 15, iPALL = 8, iTURN = 9, iWEIG = 10
    Public iTRAC = 18, iIDLE_1 = 8, iIDLE_2 = 8
    '---------- ---------- ---------- ---------- ---------- ---------- 행 갯수.
    Public iPIT_Row, iEH_Row, iGH_Row, iOH_Row
    Public iLIFT_Row, iPALL_Row, iTURN_Row, iWEIG_Row
    Public iTRAC_Row, iIDLE_1_Row, iIDLE_2_Row
    '---------- ---------- ---------- ---------- ---------- ---------- 팔렛트 생성하기 위해..
    Public Pit_SD, Pit_RV
    Public EH_Value, GH_SD, GH_RV
    '---------- ---------- ---------- ---------- ---------- ---------- 엑셀에서 읽은 각각의 정보들.
    Public Pit_Arr(iPIT, 1), EH_Arr(iEH, 1), GH_Arr(iGH, 1), OH_Arr(iOH, 1)
    Public LIFT_CAGE_Arr(iLIFT, 1), PALLET_Arr(iPALL, 1), TURN_TABLE_Arr(iTURN, 1), WEIGHT_Arr(iWEIG, 1)
    Public TRACTION_Arr(iTRAC, 1), IDLE_SHEAVE_1_Arr(iIDLE_1, 1), IDLE_SHEAVE_2_Arr(iIDLE_2, 1)
    '---------- ---------- ---------- ---------- ---------- ---------- 
    Public iWIRE As Integer = 5
    Public Wire_Arr(1, 1)

    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    '  PIT_CALCULATION
    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    Public Function PIT_CALCULATION()
        On Error Resume Next
        Dim PIT As Integer = 0, tmpNum As Integer = 0
        Dim SD As Integer = 0, RV As Integer = 0
        Dim SD_1 As Integer = Me.TXT_BOTTOM_SD.Text / 2
        Dim RV_1 As Integer = Me.TXT_BOTTOM_RV.Text / 2

        '---------- ---------- ---------- ---------- ---------- ---------- 기본 값
        For i = 1 To Pit_Arr.Length ^ (1 / 2) + 1
            If Pit_Arr(1, i) = "기본" Then
                TXT_BM.Text = Pit_Arr(2, i)
                'PIT = Pit_Arr(2, i)
                Exit For
            End If
        Next
        '---------- ---------- ---------- ---------- ---------- ---------- 지하 1단 값
        If Me.GBX_BOTTOM.Enabled = True Then
            For i = 1 To Pit_Arr.Length ^ (1 / 2)
                If Pit_Arr(1, i) = "지하 1단" Then
                    tmpNum = i
                    Exit For
                End If
            Next

            If Me.RBN_BOTTOM_SD.Checked = True Then
                'TXT_BM.Text = Pit_Arr(2, tmpNum)
                SD_1 = SD_1
                'SD_1 = SD_1 - 1
            Else
                'TXT_BM.Text = Pit_Arr(3, tmpNum)
                RV_1 = RV_1
                'RV_1 = RV_1 - 1
            End If
        End If

        '---------- ---------- ---------- ---------- ---------- ---------- 하부 격랍층 값
        For i = 1 To Pit_Arr.Length ^ (1 / 2)
            If Pit_Arr(1, i) = "일반" Then
                tmpNum = i
                Exit For
            End If
        Next

        Pit_SD = Pit_Arr(2, tmpNum)
        Pit_RV = Pit_Arr(3, tmpNum)

        '---------- ---------- ---------- ---------- ---------- ---------- 최종 계산
        tmpNum = PIT + (Pit_SD * SD_1) + (Pit_RV * RV_1)
        'tmpNum = PIT + (Pit_SD * SD_1) + (Pit_RV * RV_1) + Basement_Pit
        On Error GoTo 0

        Return tmpNum
    End Function

    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    '  EH_CALCULATION
    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    Public Function EH_CALCULATION()
        On Error Resume Next
        Dim SD = 0
        Dim SD_RV = 0
        Dim FR_C = 0

        Dim i As Integer = 0
        Err.Clear()
        For i = 1 To 6
            If Not Err.Number = 0 Then
                Exit For
            End If

            If EH_Arr(1, i) = "SEDAN" Then
                SD = EH_Arr(2, i)
            End If

            If EH_Arr(1, i) = "SEDAN+RV" Then
                SD_RV = EH_Arr(2, i)
            End If

            If EH_Arr(1, i) = "프리셀" Then
                FR_C = EH_Arr(2, i)
            End If
        Next

        '---------- ---------- ---------- ---------- ---------- ---------- 최종 계산
        If Me.GBX_FREECELL.Enabled = True Then
            If Me.RBN_FREECELL_Y.Checked = True Then
                EH_Value = FR_C
            Else
                EH_Value = SD_RV
            End If
        Else
            EH_Value = SD
        End If
        On Error GoTo 0

        Return EH_Value
    End Function

    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    '  GH_CALCULATION
    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    Public Function GH_CALCULATION()
        On Error Resume Next
        Dim tmpNum As Integer = 0
        Dim SD_1 As Integer = 0, RV_1 As Integer = 0

        For i = 1 To GH_Arr.Length ^ (1 / 2) + 1
            If Not GH_Arr(2, i) = "" Then
                If GH_Arr(1, i) = "SEDAN" Then
                    GH_SD = GH_Arr(2, i)
                End If

                If GH_Arr(1, i) = "RV" Then
                    GH_RV = GH_Arr(2, i)
                End If
            End If
        Next

        SD_1 = Me.TXT_TOP_SD.Text / 2
        RV_1 = Me.TXT_TOP_RV.Text / 2

        If Me.TXT_TOP_RV.Text = 0 Then
            SD_1 = SD_1 - 1
        Else
            RV_1 = RV_1 - 1
        End If

        '---------- ---------- ---------- ---------- ---------- ---------- 최종 계산
        tmpNum = (GH_SD * SD_1) + (GH_RV * RV_1)
        On Error GoTo 0

        Return tmpNum
    End Function

    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    '  OH_CALCULATION
    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    Public Function OH_CALCULATION()
        On Error Resume Next
        Dim tmpNum As Integer = 0
        Dim SD As Integer = 0
        Dim SD_RV As Integer = 0

        For i = 1 To OH_Arr.Length ^ (1 / 2)
            If OH_Arr(1, i) = "SEDAN" Then
                SD = OH_Arr(2, i)
            End If

            If OH_Arr(1, i) = "SEDAN+RV" Then
                SD_RV = OH_Arr(2, i)
            End If
        Next

        '---------- ---------- ---------- ---------- ---------- ---------- 최종 계산
        If Me.GBX_FREECELL.Enabled = True Then
            tmpNum = SD_RV
        Else
            tmpNum = SD
        End If
        On Error GoTo 0

        Return tmpNum
    End Function
#End Region

#Region "---------- 기계 장치 "
    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    '  기계 장치 생성 버튼 클릭.
    '---------- ---------- ---------- ---------- ---------- ---------- ----------     
    Private Sub BTN_MACHINE_CREATE_Click(sender As Object, e As EventArgs) Handles BTN_MACHINE_CREATE.Click
        Call PROGRESSBAR_CHANGE("기계 장치 불러오는 중...", 25)

        Dim Exist_Doc As Boolean = CATIA_BASIC_V6()
        If Exist_Doc = False Then
            Dim MSG_Content As String = "템플릿 파일을 가져온 후 다시 실행해 주세요."
            MsgBox(MSG_Content, vbOKOnly Or vbInformation, "ISPark CATIA Automation")
            Call PROGRESSBAR_CHANGE("ERROR : " & MSG_Content, 50)
            Exit Sub
        End If

        '---------- ---------- ---------- ---------- ---------- ---------- Template_Name 가져오기.
        Call GET_TEMPLATE_NAME()
        Template_Part_Name = oEntity.Instances.Item(1).Name

        Dim Repeat_Num As Integer = 8
        Dim oSearch_items(Repeat_Num + 3) As String
        oSearch_items(1) = Template_Name
        oSearch_items(2) = TXT_LIFT.Text
        oSearch_items(3) = TXT_PALL.Text
        oSearch_items(4) = TXT_TURN.Text
        oSearch_items(5) = TXT_WEIG.Text
        oSearch_items(6) = TXT_TRAC.Text
        oSearch_items(7) = TXT_IDE1.Text
        oSearch_items(8) = TXT_IDE2.Text
        oSearch_items(9) = Template_Part_Name
        oSearch_items(10) = TXT_PALLET_VIRTUAL.Text
        oSearch_items(11) = TXT_CAR_RV.Text

        '---------- ---------- ---------- ---------- ---------- ---------- 해당 데이터 있는지 확인.
        Dim No_Doc_List As String = "해당 데이터들이 존재하지 않습니다." & vbNewLine & _
                                    "구속 생성에 오류가 있을 수 있습니다. 계속 진행 하시겠습니까?" & vbNewLine & vbNewLine
        Dim Is_No_Doc As Boolean = False
        Dim Is_No_Doc_Count As Integer = 0
        For i = 2 To Repeat_Num
            Dim oTmp = CATIA_SEARCH(sSearch_Type, oSearch_items(i), False)

            If oTmp = 0 And Not i = 4 Then
                Is_No_Doc = True
                Is_No_Doc_Count += 1

                Dim sItem_Name = oSearch_items(i)
                If sItem_Name = "" Then
                    sItem_Name = "선택된 옵션에 해당 데이터가 없습니다."
                End If

                If i = 2 Then
                    No_Doc_List = No_Doc_List + "- LIFT : " & sItem_Name & vbNewLine
                ElseIf i = 3 Then
                    No_Doc_List = No_Doc_List + "- PALLET : " & sItem_Name & vbNewLine
                ElseIf i = 4 Then
                ElseIf i = 5 Then
                    No_Doc_List = No_Doc_List + "- COUNT WEIGHT : " & sItem_Name & vbNewLine
                ElseIf i = 6 Then
                    No_Doc_List = No_Doc_List + "- TRACTION : " & sItem_Name & vbNewLine
                ElseIf i = 7 Then
                    No_Doc_List = No_Doc_List + "- IDLE SHEAVE 1 : " & sItem_Name & vbNewLine
                ElseIf i = 8 Then
                    No_Doc_List = No_Doc_List + "- IDLE SHEAVE 2 : " & sItem_Name & vbNewLine
                End If

                oSearch_items(i) = ""
            End If
        Next

        '---------- ---------- ---------- ---------- ---------- ---------- 해당 데이터 없으면 종료 묻기.
        If Is_No_Doc = True Then
            Dim iTmp
            iTmp = MsgBox(No_Doc_List, vbYesNo Or vbInformation, "ISPark CATIA Automation")

            If iTmp = vbNo Then
                Call PROGRESSBAR_CHANGE("기계 장치 생성 취소.", 50)
                Exit Sub
            End If
        End If

        If Is_No_Doc_Count > 5 Then
            Dim iTmp
            iTmp = MsgBox("하나 이상의 데이터가 있어야 합니다." & vbNewLine & _
                            "기계 장치 옵션을 확인하여 주세요.", vbOKOnly Or vbInformation, "ISPark CATIA Automation")
            Call PROGRESSBAR_CHANGE("기계 장치 생성 취소.", 50)
            Exit Sub
        End If

        '---------- ---------- ---------- ---------- ---------- ---------- 워크벤치 체크.
        If Not CATIA.GetWorkbenchId = "Assembly" Then CATIA.StartWorkbench("Assembly")

        '---------- ---------- ---------- ---------- ---------- ---------- Exist Component.
        Dim oSearch_Result(Repeat_Num) As Object
        Dim oPallet_Instance As AnyObject = Nothing
        For i = 2 To Repeat_Num
            If Not oSearch_items(i) = "" Then
                oSearch_Result(i) = CATIA_SEARCH(sSearch_Type, oSearch_items(i), True)

                Call CATIA_BASIC_V6()

                oSelection.Clear()
                oSelection.Add(oActiveObject)
                oSelection.Copy()
                oSelection.Clear()

                Call ACTIVATE_NEXT_WINDOW(oSearch_items(1) & " A.1", False)
                Call CATIA_BASIC_V6()

                oSelection.Clear()
                oSelection.Add(oActiveObject)
                oSelection.Paste()

                If i = 3 Then
                    oPallet_Instance = oSelection.Item(1).Value
                End If

                oSelection.Clear()

                Call ACTIVATE_NEXT_WINDOW(oSearch_items(i) & " A.1", True)
            End If
        Next

        CATIA.StartCommand("Shading")
        '---------- ---------- ---------- ---------- ---------- ---------- 
        Call PROGRESSBAR_CHANGE("기계 장치 위치 수정 중...", 50)
        Call MACHINE_POSITIONNING()
        Call PULLY_POSITIONNING()
        CATIA.StartCommand("Update")
        Call PROGRESSBAR_CHANGE("기계 장치 구속 조건(DET_Constraints.xlsx) 생성 중...", 60)
        Call OPEN_EXCEL_FILE(TXT_EXCEL_PATH.Text, TXT_CONST_EXCEL.Text, False)

        '---------- ---------- ---------- ---------- ---------- ---------- Constraint 엑셀 생성.
        xlSheets = xlBook.Worksheets("Constraints_List")
        xlApp.Range("A1:I" & xlSheets.UsedRange.Rows.Count).Select()
        xlApp.Selection.Replace(What:="Template_Name", Replacement:=Template_Name)
        xlApp.Selection.Replace(What:="Template_Part_Name", Replacement:=Template_Part_Name)
        xlApp.Selection.Replace(What:="TXT_PALLET_VIRTUAL", Replacement:=TXT_PALLET_VIRTUAL.Text)
        xlApp.Selection.Replace(What:="TXT_LIFT", Replacement:=TXT_LIFT.Text)
        xlApp.Selection.Replace(What:="TXT_PALL", Replacement:=TXT_PALL.Text)
        xlApp.Selection.Replace(What:="TXT_TURN", Replacement:=TXT_TURN.Text)
        xlApp.Selection.Replace(What:="TXT_WEIG", Replacement:=TXT_WEIG.Text)
        xlApp.Selection.Replace(What:="TXT_TRAC", Replacement:=TXT_TRAC.Text)
        xlApp.Selection.Replace(What:="TXT_IDE1", Replacement:=TXT_IDE1.Text)
        xlApp.Selection.Replace(What:="TXT_IDE2", Replacement:=TXT_IDE2.Text)
        'If Dummy_Car = "SD" Then
        '    xlApp.Selection.Replace(What:="TXT_CAR", Replacement:=TXT_CAR_SD.Text)
        'Else
        '    xlApp.Selection.Replace(What:="TXT_CAR", Replacement:=TXT_CAR_RV.Text)
        'End If

        '---------- ---------- ---------- ---------- ---------- ---------- 
        Dim sTemp = Val(TXT_RESULT.Text) - Val(TXT_OH.Text) - Val(TXT_BM.Text)
        xlSheets.Range("J" & 9).Value = "0/0/" & sTemp
        xlSheets.Range("J" & 12).Value = -Val(sTemp) & "/0/0"

        '---------- ---------- ---------- ---------- ---------- ---------- 엑셀 값 가져오기.
        Dim IsExist As Boolean = False
        Dim Exc_Col As Integer = 10, Exc_Row As Integer = 1
        Dim Const_Arr(Exc_Col, 1)

        Do
            '---------- ---------- ---------- ---------- ---------- ---------- 반복 될 때 마다 배열 크기 수정.
            Exc_Row += 1
            ReDim Preserve Const_Arr(Exc_Col, Exc_Row)

            '---------- ---------- ---------- ---------- ---------- ---------- 배열에 엑셀 값 넣기.
            For i = 1 To Exc_Col
                Dim Row_String As String = Microsoft.VisualBasic.ChrW(64 + i)   'A, B, C 순으로 증가..

                Const_Arr(i, Exc_Row) = xlSheets.Range(Row_String & Exc_Row).Value
            Next

            '---------- ---------- ---------- ---------- ---------- ---------- 엑셀에 빈칸이면 종료.
            If xlSheets.Range("A" & Exc_Row + 1).Value = "" Then
                IsExist = True
            End If
        Loop Until IsExist = True

        Call CLOSE_EXCEL(False, "")

        Call CATIA_BASIC_V6()
        Dim MyContext = CATIA.ActiveEditor.GetService("PLMProductContext")
        Dim myMCXParent As VPMReference = MyContext.RootOccurrence.ReferenceRootOccurrenceOf
        Dim RootInstanceConnections As EngConnections = myMCXParent.GetItem("CATEngConnections")

        Dim Eng_Type As CatEngConnectionType = 0
        Dim Assy_Type As CatAssemblyConstraintType = 0

        '---------- ---------- ---------- ---------- ---------- ---------- 
        Dim Root_Connections As EngConnection = Nothing
        Dim Sub_Connections As AssemblyConstraint = Nothing

        Dim Window_HWND, Button_HWND
        On Error Resume Next
        For i = 2 To Exc_Row
            '---------- ---------- ---------- ---------- ---------- ---------- 
            If Const_Arr(Exc_Col - 3, i) = "FIX" Then
                Eng_Type = CatEngConnectionType.catFix
            ElseIf Const_Arr(Exc_Col - 3, i) = "RIGID" Then
                Eng_Type = CatEngConnectionType.catRigid
            ElseIf Const_Arr(Exc_Col - 3, i) = "PRISMATIC" Then
                Eng_Type = CatEngConnectionType.catPrismatic
            ElseIf Const_Arr(Exc_Col - 3, i) = "CABLE" Then
                Eng_Type = CatEngConnectionType.catUserDefined
            ElseIf Const_Arr(Exc_Col - 3, i) = "REVOLUTE" Then
                Eng_Type = CatEngConnectionType.catRevolute
            ElseIf Const_Arr(Exc_Col - 3, i) = "UPDATE" Then
                Eng_Type = CatEngConnectionType.catCylindrical
            ElseIf Const_Arr(Exc_Col - 3, i) = "" Then
                Eng_Type = CatEngConnectionType.catCylindrical
            End If
            '---------- ---------- ---------- ---------- ---------- ---------- 
            If Const_Arr(Exc_Col - 2, i) = "COINCIDENCE" Then
                If Const_Arr(Exc_Col - 3, i) = "RIGID" Then
                    Assy_Type = CatAssemblyConstraintType.catCoincidenceAxisSystemAxisSystem
                    'ElseIf Const_Arr(Exc_Col - 1, i) = "CAR_CONST" Then
                    '    Assy_Type = CatAssemblyConstraintType.catCoincidencePlanePlane
                ElseIf Const_Arr(Exc_Col - 3, i) = "PRISMATIC" Or Const_Arr(Exc_Col - 3, i) = "REVOLUTE" Then
                    Assy_Type = CatAssemblyConstraintType.catCoincidenceLineLine
                End If
            ElseIf Const_Arr(Exc_Col - 2, i) = "CONTACT" Then
                Assy_Type = CatAssemblyConstraintType.catContactPlanePlane
            ElseIf Const_Arr(Exc_Col - 2, i) = "OFFSET" Then
                Assy_Type = CatAssemblyConstraintType.catDistancePlanePlane
            ElseIf Const_Arr(Exc_Col - 2, i) = "ANGLE" Then
                Assy_Type = CatAssemblyConstraintType.catAnglePlanePlane
            ElseIf Const_Arr(Exc_Col - 2, i) = "COMPOUND" Then
                Assy_Type = CatAssemblyConstraintType.catUserDefinedConstraint
            ElseIf Const_Arr(Exc_Col - 2, i) = "FIX" Then
                Assy_Type = CatAssemblyConstraintType.catFixInstance
            End If

            '---------- ---------- ---------- ---------- ---------- ---------- 
            If Const_Arr(Exc_Col - 2, i) = "FIX" Then
                Dim Root_Eng_Arr(1)
                Root_Eng_Arr(0) = Const_Arr(2, i)

                Dim Sub_Eng_Arr(1)
                Sub_Eng_Arr(0) = Const_Arr(2, i) & "/" & Const_Arr(3, i)

                Root_Connections = RootInstanceConnections.Add(Eng_Type, Root_Eng_Arr)
                Sub_Connections = Root_Connections.AssemblyConstraints.Add(Assy_Type, Sub_Eng_Arr)

                If Not Const_Arr(Exc_Col - 1, i) = "" Then
                    Root_Connections.Name = Const_Arr(Exc_Col - 1, i)
                End If
            Else
                If Not Const_Arr(4, i) = "" Or Not Const_Arr(4, i) = Nothing Then
                    Dim Root_Eng_Arr(2)
                    Root_Eng_Arr(0) = Template_Name
                    Root_Eng_Arr(1) = Const_Arr(2, i)
                    Root_Eng_Arr(2) = Const_Arr(4, i)

                    Dim Sub_Eng_Arr(1)
                    Sub_Eng_Arr(0) = Const_Arr(2, i) & "/" & Const_Arr(3, i)
                    Sub_Eng_Arr(1) = Const_Arr(4, i) & "/" & Const_Arr(5, i)

                    If Eng_Type = CatEngConnectionType.catUserDefined Then
                        CATIA.StartCommand("Update")
                        CATIA.StartCommand("Engineering Connection")
                        System.Threading.Thread.Sleep(2000)
                        oSelection.Search("Name='LIFT_CAGE', all")
                        System.Threading.Thread.Sleep(2000)
                        oSelection.Search("Name='COUNT_WEIGHT', all")
                        System.Threading.Thread.Sleep(2000)
                        oSelection.Clear()

                        Window_HWND = FindWindow(Nothing, "Engineering Connection Definition")
                        Button_HWND = FindWindowEx(Window_HWND, IntPtr.Zero, "Button", "OK")
                        PostMessage(Button_HWND, &H201, 0, IntPtr.Zero)
                        System.Threading.Thread.Sleep(2000)
                        PostMessage(Button_HWND, &H202, 0, IntPtr.Zero)
                        System.Threading.Thread.Sleep(2000)
                        oSelection.Clear()

                        Root_Connections = RootInstanceConnections.Item(RootInstanceConnections.Count)
                        Root_Connections.Name = Const_Arr(Exc_Col - 1, i)
                        Sub_Connections = Root_Connections.AssemblyConstraints.Item(1)
                        Sub_Connections.SetValue(1, -1)
                        Sub_Connections.SetValue(2, 1)
                    ElseIf Not Eng_Type = CatEngConnectionType.catCylindrical Then
                        Root_Connections = RootInstanceConnections.Add(Eng_Type, Root_Eng_Arr)
                        Root_Connections.Name = Const_Arr(Exc_Col - 1, i)
                    End If

                    If Not Eng_Type = CatEngConnectionType.catUserDefined Then
                        Sub_Connections = Root_Connections.AssemblyConstraints.Add(Assy_Type, Sub_Eng_Arr)
                    End If

                    '---------- ---------- ---------- ---------- ---------- ---------- 컨트롤 모드 & 방향 옵션.
                    If Const_Arr(Exc_Col - 2, i) = "OFFSET" Or Const_Arr(Exc_Col - 2, i) = "ANGLE" Then
                        Sub_Connections.Mode = CatAssemblyConstraintMode.catControlledMode

                        Dim tmpNum = Split(Const_Arr(Exc_Col, i), "/")
                        Sub_Connections.SetMinValue(1, Val(tmpNum(0)))
                        Sub_Connections.SetValue(1, Val(tmpNum(1)))
                        Sub_Connections.SetMaxValue(1, Val(tmpNum(2)))

                        Root_Connections.AssemblyConstraints.Item(1).SetOption(1, 1)
                        Root_Connections.AssemblyConstraints.Item(2).SetOption(1, 1)
                        Root_Connections.AssemblyConstraints.Item(3).SetOption(1, 1)
                        Root_Connections.AssemblyConstraints.Item(3).SetOption(2, 1)
                    End If
                End If
            End If
        Next
        On Error GoTo 0

        '---------- ---------- ---------- ---------- ---------- ---------- 
        CATIA.StartCommand("Update")
        CATIA.StartCommand("Update")
        Dim Viewer As Viewer = CATIA.ActiveWindow.ActiveViewer.Reframe()

        Call PROGRESSBAR_CHANGE("Mechanism 생성 중...", 75)

        '---------- ---------- ---------- ---------- ---------- ---------- 워크벤치 체크.
        If Not CATIA.GetWorkbenchId = "CATKinMechanismWorkbench" Then CATIA.StartWorkbench("CATKinMechanismWorkbench")
        CATIA.StartCommand("Mechanism Representation")

        System.Threading.Thread.Sleep(1000)
        Window_HWND = FindWindow(Nothing, "Mechanism Representation")
        Button_HWND = FindWindowEx(Window_HWND, IntPtr.Zero, "Button", "OK")
        PostMessage(Button_HWND, &H201, 0, IntPtr.Zero)
        System.Threading.Thread.Sleep(1000)
        PostMessage(Button_HWND, &H202, 0, IntPtr.Zero)
        System.Threading.Thread.Sleep(1000)

        '---------- ---------- ---------- ---------- ---------- ---------- 생성된 메케니즘 커멘드 조정
        Call CATIA_BASIC_V6()
        oSelection.Clear()
        oSelection.Search("Name='Commands', all")

        Dim oCommand = oSelection.Item(1).Value.Commands
        For i = 1 To oCommand.Count
            If oCommand.Item(i).Joint.Name = "COUNT_WEIGHT" Then
                oCommand.remove(i)
                Exit For
            End If
        Next

        '---------- ---------- ---------- ---------- ---------- ---------- 커멘드 이름 변경.
        oCommand.Item(1).Name = "LIFTING"
        oCommand.Item(2).Name = "SHIFTING"

        Call CATIA_BASIC_V6()
        Call PROGRESSBAR_CHANGE("Pallet 생성 중...", 85)
        Call CREATE_PALLET_POSITION(oPallet_Instance)
        Call PROGRESSBAR_CHANGE("기계 장치 생성 완료.", 100)
    End Sub

    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    '  기계 장치 위치 수정 버튼 클릭.
    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    Private Sub BTN_MACHINE_POSITION_Click(sender As Object, e As EventArgs) Handles BTN_MACHINE_POSITION.Click
        On Error Resume Next
        Call PROGRESSBAR_CHANGE("기계 장치 위치 변경 중...", 20)

        Dim tmpNum As Integer = MACHINE_POSITIONNING()
        If tmpNum = 1 Then
            Exit Sub
        End If

        Call PROGRESSBAR_CHANGE("기계 장치 위치 변경 중...", 80)

        '---------- ---------- ---------- ---------- ---------- ---------- 워크벤치 체크.
        If Not CATIA.GetWorkbenchId = "Assembly" Then CATIA.StartWorkbench("Assembly")

        CATIA.StartCommand("Update")
        Call PROGRESSBAR_CHANGE("기계 장치 위치 변경 완료.", 100)
        On Error GoTo 0
    End Sub

    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    '  Pully 위치 값 수정.
    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    Public Sub PULLY_POSITIONNING()
        Call CATIA_BASIC_V6()

        Dim oVPMOccurrences As VPMOccurrence = oActiveObject.Occurrences.Item(1)
        Dim oSkel_Part As Part = oVPMOccurrences.RepOccurrences.Item(1).RelatedRepInstance.ReferenceInstanceOf.GetItem("Part")
        Dim oWire_Body As HybridBody = oSkel_Part.HybridBodies.Item("#WIRE ROPE")
        Dim oWire_1st As HybridBody = oWire_Body.HybridBodies.Item("1ST GROUP WIRE")
        Dim oWire_4st As HybridBody = oWire_Body.HybridBodies.Item("4TH GROUP WIRE")

        Dim iFile_Position As Integer = 5

        '---------- ---------- ---------- ---------- ---------- ---------- Lift Cage.
        Dim oLift_Cage As AnyObject = oWire_4st.HybridBodies.Item("#LIFT CAGE ASSY PULLY").HybridShapes.Item("PULLY#PT")
        For j = 2 To Val(LIFT_CAGE_Arr(0, 0))
            If TXT_LIFT.Text = LIFT_CAGE_Arr(iLIFT - iFile_Position, j) Then
                oLift_Cage.Y.Value = LIFT_CAGE_Arr(8, j)
                oLift_Cage.Z.Value = LIFT_CAGE_Arr(9, j)
                Exit For
            End If
        Next

        '---------- ---------- ---------- ---------- ---------- ---------- Counter Weight.
        Dim oCounter_Weight_1 As AnyObject = oWire_1st.HybridBodies.Item("#COUNTER WEIGHT ASSY PULLY").HybridShapes.Item("PULLY#PT")
        Dim oCounter_Weight_2 As AnyObject = oWire_4st.HybridBodies.Item("#COUNTER WEIGHT ASSY PULLY").HybridShapes.Item("PULLY#PT")
        For j = 2 To Val(WEIGHT_Arr(0, 0))
            If TXT_WEIG.Text = WEIGHT_Arr(iWEIG - iFile_Position, j) Then
                oCounter_Weight_1.Z.Value = WEIGHT_Arr(4, j)
                oCounter_Weight_2.Z.Value = WEIGHT_Arr(4, j)
                Exit For
            End If
        Next

        '---------- ---------- ---------- ---------- ---------- ---------- Main Traction.
        Dim oMain_Traction_1 As AnyObject = oWire_1st.HybridBodies.Item("#MAIN TRACTION ASSY PULLY").HybridShapes.Item("PULLY#PT#1")
        Dim oMain_Traction_2 As AnyObject = oWire_1st.HybridBodies.Item("#MAIN TRACTION ASSY PULLY").HybridShapes.Item("PULLY#PT#2")
        Dim oMain_Traction_3 As AnyObject = oWire_4st.HybridBodies.Item("#MAIN TRACTION ASSY PULLY").HybridShapes.Item("PULLY#PT#1")
        Dim oMain_Traction_4 As AnyObject = oWire_4st.HybridBodies.Item("#MAIN TRACTION ASSY PULLY").HybridShapes.Item("PULLY#PT#2")
        For j = 2 To Val(TRACTION_Arr(0, 0))
            If TXT_TRAC.Text = TRACTION_Arr(iTRAC - iFile_Position, j) Then
                oMain_Traction_1.X.Value = TRACTION_Arr(7, j)
                oMain_Traction_1.Y.Value = TRACTION_Arr(8, j)
                oMain_Traction_1.Z.Value = TRACTION_Arr(9, j)
                oMain_Traction_2.X.Value = TRACTION_Arr(10, j)
                oMain_Traction_2.Y.Value = TRACTION_Arr(11, j)
                oMain_Traction_2.Z.Value = TRACTION_Arr(12, j)
                oMain_Traction_3.X.Value = TRACTION_Arr(7, j)
                oMain_Traction_3.Y.Value = TRACTION_Arr(8, j)
                oMain_Traction_3.Z.Value = TRACTION_Arr(9, j)
                oMain_Traction_4.X.Value = TRACTION_Arr(10, j)
                'oMain_Traction_4.Y.Value = TRACTION_Arr(11, j)
                oMain_Traction_4.Z.Value = TRACTION_Arr(12, j)
                Exit For
            End If
        Next

        '---------- ---------- ---------- ---------- ---------- ---------- Socket Plate.
        Dim oSocket_Plate_1 As AnyObject = oWire_1st.HybridBodies.Item("#STRUCTURE PLATE").HybridShapes.Item("MAIN TRACTION#PLATE#PT")
        Dim oSocket_Plate_2 As AnyObject = oWire_4st.HybridBodies.Item("#STRUCTURE PLATE").HybridShapes.Item("MAIN TRACTION#PLATE#PT")
        For j = 2 To Val(TRACTION_Arr(0, 0))
            If TXT_TRAC.Text = TRACTION_Arr(iTRAC - iFile_Position, j) Then
                oSocket_Plate_1.X.Value = TRACTION_Arr(4, j)
                oSocket_Plate_1.Y.Value = TRACTION_Arr(5, j)
                oSocket_Plate_1.Z.Value = TRACTION_Arr(6, j)
                oSocket_Plate_2.X.Value = TRACTION_Arr(4, j)
                oSocket_Plate_2.Y.Value = TRACTION_Arr(5, j)
                oSocket_Plate_2.Z.Value = TRACTION_Arr(6, j)
                Exit For
            End If
        Next

        CATIA.StartCommand("Update")
        CATIA.StartCommand("Update")
    End Sub

    Private Sub TXT_BOTTOM_SD_TextChanged(sender As Object, e As EventArgs) Handles TXT_BOTTOM_SD.TextChanged
        If TXT_BOTTOM_SD.Text = "0" Then
            If TXT_BOTTOM_RV.Text = "0" Then
                GBX_BOTTOM.Enabled = False
            Else
                RBN_BOTTOM_RV.Checked = True
            End If
        Else
            GBX_BOTTOM.Enabled = True

            RBN_BOTTOM_SD.Checked = True
        End If
    End Sub
    Private Sub TXT_BOTTOM_RV_TextChanged(sender As Object, e As EventArgs) Handles TXT_BOTTOM_RV.TextChanged
        If TXT_BOTTOM_RV.Text = "0" Then
            If TXT_BOTTOM_SD.Text = "0" Then
                GBX_BOTTOM.Enabled = False
            Else
                RBN_BOTTOM_SD.Checked = True
            End If

            '---------- ---------- ---------- ---------- ---------- ---------- 프리셀 그룹 박스
            If TXT_TOP_RV.Text = "0" Then
                GBX_FREECELL.Enabled = False
            End If
        Else
            GBX_BOTTOM.Enabled = True

            If TXT_BOTTOM_SD.Text = "0" Then
                RBN_BOTTOM_RV.Checked = True
            Else
                RBN_BOTTOM_SD.Checked = True
            End If

            '---------- ---------- ---------- ---------- ---------- ---------- 프리셀 그룹 박스
            GBX_FREECELL.Enabled = True
        End If
    End Sub
    Private Sub TXT_TOP_RV_TextChanged(sender As Object, e As EventArgs) Handles TXT_TOP_RV.TextChanged
        '---------- ---------- ---------- ---------- ---------- ---------- 프리셀 그룹 박스
        If TXT_TOP_RV.Text = "0" Then
            If TXT_BOTTOM_RV.Text = "0" Then
                GBX_FREECELL.Enabled = False
            End If
        Else
            GBX_FREECELL.Enabled = True
        End If
    End Sub

#Region "---------- 콤보박스 값 변경시 "
    Private Sub CMB_LIFT_1_SelectedIndexChanged(sender As Object, e As EventArgs) _
        Handles CMB_LIFT_1.SelectedIndexChanged, CMB_LIFT_2.SelectedIndexChanged, CMB_LIFT_3.SelectedIndexChanged, _
                    CMB_LIFT_4.SelectedIndexChanged, CMB_LIFT_5.SelectedIndexChanged

        Dim TextBox_Main(6) As Object
        TextBox_Main(1) = CMB_LIFT_1
        TextBox_Main(2) = CMB_LIFT_2
        TextBox_Main(3) = CMB_LIFT_3
        TextBox_Main(4) = CMB_LIFT_4
        TextBox_Main(5) = CMB_LIFT_5

        Dim TextBox_Location(2) As Object
        TextBox_Location(0) = TXB_LIFT_X
        TextBox_Location(1) = TXB_LIFT_Y
        TextBox_Location(2) = TXB_LIFT_Z

        TXT_LIFT.Text = MACHINE_LOCATION(LIFT_CAGE_Arr, 12, 5, 3, TextBox_Main, TextBox_Location)
    End Sub

    Private Sub CMB_PALL_SelectedIndexChanged(sender As Object, e As EventArgs) _
        Handles CMB_PALL.SelectedIndexChanged

        Dim TextBox_Main(3) As Object
        TextBox_Main(1) = CMB_PALL

        Dim TextBox_Location(2) As Object
        TextBox_Location(0) = TXB_PALL_X
        TextBox_Location(1) = TXB_PALL_Y
        TextBox_Location(2) = TXB_PALL_Z

        TXT_PALL.Text = MACHINE_LOCATION(PALLET_Arr, 5, 1, 1, TextBox_Main, TextBox_Location)
    End Sub

    Private Sub CMB_TURN_1_SelectedIndexChanged(sender As Object, e As EventArgs) _
        Handles CMB_TURN_1.SelectedIndexChanged, CMB_TURN_2.SelectedIndexChanged

        Dim TextBox_Main(3) As Object
        TextBox_Main(1) = CMB_TURN_1
        TextBox_Main(2) = CMB_TURN_2

        Dim TextBox_Location(2) As Object
        TextBox_Location(0) = TXB_TURN_X
        TextBox_Location(1) = TXB_TURN_Y
        TextBox_Location(2) = TXB_TURN_Z

        TXT_TURN.Text = MACHINE_LOCATION(TURN_TABLE_Arr, 6, 2, 2, TextBox_Main, TextBox_Location)
    End Sub

    Private Sub CMB_WEIG_1_SelectedIndexChanged(sender As Object, e As EventArgs) _
        Handles CMB_WEIG_1.SelectedIndexChanged, CMB_WEIG_2.SelectedIndexChanged

        Dim TextBox_Main(3) As Object
        TextBox_Main(1) = CMB_WEIG_1
        TextBox_Main(2) = CMB_WEIG_2

        Dim TextBox_Location(2) As Object
        TextBox_Location(0) = TXB_WEIG_X
        TextBox_Location(1) = TXB_WEIG_Y
        TextBox_Location(2) = TXB_WEIG_Z

        TXT_WEIG.Text = MACHINE_LOCATION(WEIGHT_Arr, 7, 2, 1, TextBox_Main, TextBox_Location)
    End Sub

    Private Sub CMB_IDLE_1_SelectedIndexChanged(sender As Object, e As EventArgs) _
        Handles CMB_IDLE_1.SelectedIndexChanged

        Dim TextBox_Main(3) As Object
        TextBox_Main(1) = CMB_IDLE_1

        Dim TextBox_Location(2) As Object
        TextBox_Location(0) = TXB_IDLE1_X
        TextBox_Location(1) = TXB_IDLE1_Y
        TextBox_Location(2) = TXB_IDLE1_Z

        TXT_IDE1.Text = MACHINE_LOCATION(IDLE_SHEAVE_1_Arr, 5, 1, 1, TextBox_Main, TextBox_Location)
    End Sub

    Private Sub CMB_IDLE_2_SelectedIndexChanged(sender As Object, e As EventArgs) _
        Handles CMB_IDLE_2.SelectedIndexChanged

        Dim TextBox_Main(3) As Object
        TextBox_Main(1) = CMB_IDLE_2

        Dim TextBox_Location(2) As Object
        TextBox_Location(0) = TXB_IDLE2_X
        TextBox_Location(1) = TXB_IDLE2_Y
        TextBox_Location(2) = TXB_IDLE2_Z

        TXT_IDE2.Text = MACHINE_LOCATION(IDLE_SHEAVE_2_Arr, 5, 1, 1, TextBox_Main, TextBox_Location)
    End Sub

    Private Sub CMB_TRAC_1_SelectedIndexChanged(sender As Object, e As EventArgs) _
        Handles CMB_TRAC_1.SelectedIndexChanged, CMB_TRAC_2.SelectedIndexChanged

        Dim TextBox_Main(2) As Object
        TextBox_Main(1) = CMB_TRAC_1
        TextBox_Main(2) = CMB_TRAC_2

        Dim TextBox_Location(2) As Object
        TextBox_Location(0) = TXB_TRAC_X
        TextBox_Location(1) = TXB_TRAC_Y
        TextBox_Location(2) = TXB_TRAC_Z

        TXT_TRAC.Text = MACHINE_LOCATION(TRACTION_Arr, 15, 2, 2, TextBox_Main, TextBox_Location)
    End Sub
#End Region
#End Region

#Region "---------- Simulation & BOM"
    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    '  BOM 선택 버튼 클릭.
    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    Private Sub BTN_BOM_SELECTION_Click(sender As Object, e As EventArgs) Handles BTN_BOM_SELECTION.Click
        Try
            Call PROGRESSBAR_CHANGE("BOM 출력 대상 선택 중...", 40)
            Call CATIA_BASIC_V6()

            Dim oPLMEntity As PLMEntity = oSelection.Item(1).Value.PLMEntity
            If oSelection.Item(1).Type = "VPMOccurrence" Then
                oPLMEntity = oPLMEntity.ReferenceInstanceOf
            End If

            '---------- ---------- ---------- ---------- ---------- ---------- 
            TXT_BOM_SELECTION.Text = oPLMEntity.GetAttributeValue("V_Name")

            Call PROGRESSBAR_CHANGE("BOM 출력 대상 선택 완료.", 100)
        Catch ex As Exception
            If oSelection.Count = 0 Then
                MsgBox("선택된 대상이 없습니다!", vbInformation, "ISPark CATIA Automation")
            Else
                MsgBox(ex.Message, vbInformation, "ISPark CATIA Automation")
            End If
        End Try
    End Sub

    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    '  BOM 생성 버튼 클릭.
    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    Private Sub BTN_BOM_CREATE_Click(sender As Object, e As EventArgs) Handles BTN_BOM_CREATE.Click
        Try
            Call PROGRESSBAR_CHANGE("BOM 생성 중...", 30)
            '---------- ---------- ---------- ---------- ---------- ---------- BOM Excel 존재 여부.
            Dim Exist_Result = THISFILEEXIST(TXT_EXCEL_PATH.Text, TXT_BOM_EXCEL.Text)
            If Exist_Result = "" Then
                Dim MSG_Content As String = "BOM 엑셀을 찾을 수 없어 종료합니다!"
                MsgBox(MSG_Content & vbCrLf & _
                       "-> " & TXT_EXCEL_PATH.Text & "\" & TXT_BOM_EXCEL.Text, _
                       vbOKOnly Or vbInformation, "ISPark CATIA Automation")

                Call PROGRESSBAR_CHANGE(MSG_Content, 50)
                End
            End If
            '---------- ---------- ---------- ---------- ---------- ---------- Template 오픈 여부.
            Dim Exist_Doc As Boolean = CATIA_BASIC_V6()
            If Exist_Doc = False Then
                Dim MSG_Content As String = "BOM 출력을 할 데이터를 먼저 열어주세요."
                MsgBox(MSG_Content, vbOKOnly Or vbInformation, "ISPark CATIA Automation")

                Call PROGRESSBAR_CHANGE(MSG_Content, 50)
                Exit Sub
            End If
            '---------- ---------- ---------- ---------- ---------- ---------- 대상이 전체인지 선택한 것만 인지.
            If RBN_BOM_OPT1.Checked = True Then
                oSelection.Clear()
                oSelection.Add(oActiveObject)
            ElseIf RBN_BOM_OPT2.Checked = True Then
                If oSelection.Count = 0 Then
                    Dim MSG_Content As String = "BOM 출력을 할 Product를 선택해 주세요."
                    MsgBox(MSG_Content, vbOKOnly Or vbInformation, "ISPark CATIA Automation")

                    Call PROGRESSBAR_CHANGE(MSG_Content, 50)
                    Exit Sub
                End If
            End If
            '---------- ---------- ---------- ---------- ---------- ---------- 
            Call PROGRESSBAR_CHANGE("BOM 생성 중...", 45)
            '---------- ---------- ---------- ---------- ---------- ---------- Tree 정보 읽기.
            Dim oPLMEntity As PLMEntity = oSelection.Item(1).Value.PLMEntity
            Dim Separate_Item As AnyObject = Nothing
            Dim Item_Count As Integer = 1
            Dim Now_Lv As Integer = 1
            Bom_Exc_Lv = 1
            '---------- ---------- ---------- ---------- ---------- ---------- Root 인지 아닌지.
            If oSelection.Item(1).Type = "VPMRootOccurrence" Then
                Separate_Item = oPLMEntity
            ElseIf oSelection.Item(1).Type = "VPMOccurrence" Then
                Separate_Item = oPLMEntity.ReferenceInstanceOf
            End If

            '---------- ---------- ---------- ---------- ---------- ---------- BOM 배열에 값 저장.
            Dim tmpStr = Split(Separate_Item.GetAttributeValue("V_Name"), "#")
            BOM_VALUE(1, 0) = Separate_Item.GetAttributeValue("V_Name")
            BOM_VALUE(1, 1) = Bom_Exc_Lv
            BOM_VALUE(1, 2) = tmpStr(0)
            If Not tmpStr.Length = 1 Then
                BOM_VALUE(1, 3) = tmpStr(1)
            End If
            BOM_VALUE(1, 4) = "ROOT_ASSY"
            BOM_VALUE(1, 5) = Separate_Item.GetAttributeValue("revision")
            BOM_VALUE(1, 6) = 1
            BOM_VALUE(1, 7) = 1
            tmpStr = Split(Separate_Item.GetAttributeValue("V_description"), "#")
            If tmpStr.Length = 1 Then
                BOM_VALUE(1, 9) = Replace(tmpStr(0), Chr(10), "")
            ElseIf tmpStr.Length = 2 Then
                BOM_VALUE(1, 9) = Replace(tmpStr(0), Chr(10), "")
                BOM_VALUE(1, 10) = Replace(tmpStr(1), Chr(10), "")
            ElseIf tmpStr.Length = 3 Then
                BOM_VALUE(1, 9) = Replace(tmpStr(0), Chr(10), "")
                BOM_VALUE(1, 10) = Replace(tmpStr(1), Chr(10), "")
                BOM_VALUE(1, 11) = Replace(tmpStr(2), Chr(10), "")
            End If

            'Bom_Exc_Lv += 1
            '---------- ---------- ---------- ---------- ---------- ---------- 재귀함수로 트리 읽기.
            Item_Count = SEPARATE_SYMBOL(Separate_Item, Item_Count)

            '---------- ---------- ---------- ---------- ---------- ---------- BOM 정보 엑셀에 입력.
            Call PROGRESSBAR_CHANGE("BOM 생성 중...", 85)
            Call OPEN_EXCEL_FILE(TXT_EXCEL_PATH.Text, TXT_BOM_EXCEL.Text, True)
            xlSheets = xlBook.Worksheets("BOM")
            Call INSERT_BOM_EXCEL(BOM_VALUE, Item_Count, Wire_Arr)
            '---------- ---------- ---------- ---------- ---------- ---------- 선택 칸만 넓이 조절.
            xlSheets.Columns("K").AutoFit()
            xlSheets.Columns("L").AutoFit()
            xlSheets.Columns("M").AutoFit()
            xlSheets.Columns("N").AutoFit()
            xlSheets.Columns("O").AutoFit()
            xlSheets.Columns("S").AutoFit()
            xlSheets.Columns("V").AutoFit()
            xlApp.Visible = True
            '---------- ---------- ---------- ---------- ---------- ---------- 
            Call PROGRESSBAR_CHANGE("BOM 생성 완료.", 100)
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "ISPark CATIA Automation")
        End Try
    End Sub

    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    '  BOM 출력 대상 선택시.
    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    Private Sub RBN_BOM_OPT1_CheckedChanged(sender As Object, e As EventArgs) Handles RBN_BOM_OPT1.CheckedChanged
        On Error Resume Next
        TXT_BOM_SELECTION.Enabled = False
        BTN_BOM_SELECTION.Enabled = False
        TXT_BOM_SELECTION.Text = ""
        oSelection.Clear()
        On Error GoTo 0
    End Sub
    Private Sub RBN_BOM_OPT2_CheckedChanged(sender As Object, e As EventArgs) Handles RBN_BOM_OPT2.CheckedChanged
        On Error Resume Next
        TXT_BOM_SELECTION.Enabled = True
        BTN_BOM_SELECTION.Enabled = True
        On Error GoTo 0
    End Sub
#End Region

#Region "---------- 생성한 함수들 "
    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    '  GET_BASIC_INFO.
    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    Public Sub GET_BASIC_INFO()
        Pit_Arr = GET_EXCEL_VALUE("PIT", iPIT, iPIT_Row)
        EH_Arr = GET_EXCEL_VALUE("EH", iEH, iEH_Row)
        GH_Arr = GET_EXCEL_VALUE("격랍층", iGH, iGH_Row)
        OH_Arr = GET_EXCEL_VALUE("OH", iOH, iOH_Row)

        Wire_Arr = GET_EXCEL_VALUE("WIRE", iWIRE, 1)

        LIFT_CAGE_Arr = GET_EXCEL_VALUE("LIFT_CAGE", iLIFT, iLIFT_Row)
        PALLET_Arr = GET_EXCEL_VALUE("PALLET", iPALL, iPALL_Row)
        TURN_TABLE_Arr = GET_EXCEL_VALUE("TURN_TABLE", iTURN, iTURN_Row)
        WEIGHT_Arr = GET_EXCEL_VALUE("COUNTER_WEIGHT", iWEIG, iWEIG_Row)
        TRACTION_Arr = GET_EXCEL_VALUE("MAIN_TRACTION", iTRAC, iTRAC_Row)
        IDLE_SHEAVE_1_Arr = GET_EXCEL_VALUE("IDLE_SHEAVE_1", iIDLE_1, iIDLE_1_Row)
        IDLE_SHEAVE_2_Arr = GET_EXCEL_VALUE("IDLE_SHEAVE_2", iIDLE_2, iIDLE_2_Row)

        '---------- ---------- ---------- ---------- ---------- ---------- ComboBox 채우기
        Call INSERT_COMBOBOX_VALUE(LIFT_CAGE_Arr, Me.CMB_LIFT_1, Me.LBX_Combobox_Sort)
        Call INSERT_COMBOBOX_VALUE(LIFT_CAGE_Arr, Me.CMB_LIFT_2, Me.LBX_Combobox_Sort)
        Call INSERT_COMBOBOX_VALUE(LIFT_CAGE_Arr, Me.CMB_LIFT_3, Me.LBX_Combobox_Sort)
        Call INSERT_COMBOBOX_VALUE(LIFT_CAGE_Arr, Me.CMB_LIFT_4, Me.LBX_Combobox_Sort)
        Call INSERT_COMBOBOX_VALUE(LIFT_CAGE_Arr, Me.CMB_LIFT_5, Me.LBX_Combobox_Sort)
        Call INSERT_COMBOBOX_VALUE(PALLET_Arr, Me.CMB_PALL, Me.LBX_Combobox_Sort)
        Call INSERT_COMBOBOX_VALUE(TURN_TABLE_Arr, Me.CMB_TURN_1, Me.LBX_Combobox_Sort)
        Call INSERT_COMBOBOX_VALUE(TURN_TABLE_Arr, Me.CMB_TURN_2, Me.LBX_Combobox_Sort)
        Call INSERT_COMBOBOX_VALUE(WEIGHT_Arr, Me.CMB_WEIG_1, Me.LBX_Combobox_Sort)
        Call INSERT_COMBOBOX_VALUE(WEIGHT_Arr, Me.CMB_WEIG_2, Me.LBX_Combobox_Sort)
        Call INSERT_COMBOBOX_VALUE(TRACTION_Arr, Me.CMB_TRAC_1, Me.LBX_Combobox_Sort)
        Call INSERT_COMBOBOX_VALUE(TRACTION_Arr, Me.CMB_TRAC_2, Me.LBX_Combobox_Sort)
        Call INSERT_COMBOBOX_VALUE(IDLE_SHEAVE_1_Arr, Me.CMB_IDLE_1, Me.LBX_Combobox_Sort)
        Call INSERT_COMBOBOX_VALUE(IDLE_SHEAVE_2_Arr, Me.CMB_IDLE_2, Me.LBX_Combobox_Sort)
    End Sub

    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    '  MACHINE_POSITIONNING.
    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    Public Function MACHINE_POSITIONNING()
        On Error Resume Next
        Dim Exist_Doc As Boolean = CATIA_BASIC_V6()
        If Exist_Doc = False Then
            Dim MSG_Content As String = "템플릿 파일을 가져온 후 다시 실행해 주세요."
            MsgBox(MSG_Content, vbOKOnly Or vbInformation, "ISPark CATIA Automation")
            Call PROGRESSBAR_CHANGE("ERROR : " & MSG_Content, 50)
            Return 1
        End If

        Call SKELETON_SEARCH(False)
        '---------- ---------- ---------- ---------- ---------- ---------- #POINTS FOR MACHINE
        Dim oMachine As Object = Skeleton_Body.parent.item("#MACHINE").hybridbodies.item("#POINTS FOR MACHINE")
        '---------- ---------- ---------- ---------- ---------- ---------- #POINTS FOR LAY-OUT
        Dim oLAY_OUT As Object = Skeleton_Body.parent.item("#LAY-OUT").hybridbodies.item("#POINTS FOR LAY-OUT")

        Dim tmpObj(7) As Object
        tmpObj(1) = oMachine.hybridshapes.item("LIFT CAGE PT")
        tmpObj(2) = oMachine.hybridshapes.item("PALLET PT ON LIFT CAGE")
        tmpObj(3) = oLAY_OUT.hybridshapes.item("FIT & TURN TABLE PT")
        tmpObj(4) = oMachine.hybridshapes.item("WEIGHT PT")
        tmpObj(5) = oMachine.hybridshapes.item("TRACTION PT")
        tmpObj(6) = oMachine.hybridshapes.item("IDLE SHEAVE1 PT")
        tmpObj(7) = oMachine.hybridshapes.item("IDLE SHEAVE2 PT")

        '---------- ---------- ---------- ---------- ---------- ---------- LIFT CAGE
        tmpObj(1).x.value = TXB_LIFT_X.Text
        tmpObj(1).y.value = TXB_LIFT_Y.Text
        tmpObj(1).z.value = TXB_LIFT_Z.Text
        '---------- ---------- ---------- ---------- ---------- ---------- PALLET
        tmpObj(2).x.value = TXB_PALL_X.Text
        tmpObj(2).y.value = TXB_PALL_Y.Text
        tmpObj(2).z.value = TXB_PALL_Z.Text
        '---------- ---------- ---------- ---------- ---------- ---------- TURN TABLE
        tmpObj(3).x.value = TXB_TURN_X.Text
        tmpObj(3).y.value = TXB_TURN_Y.Text
        tmpObj(3).z.value = -(Val(TXT_PIT.Text) + Val(TXT_BM.Text))
        TXB_TURN_Z.Text = tmpObj(3).z.value
        '---------- ---------- ---------- ---------- ---------- ---------- COUNT WEIGHT
        tmpObj(4).x.value = TXB_WEIG_X.Text
        tmpObj(4).y.value = TXB_WEIG_Y.Text
        tmpObj(4).z.value = -Val(TXB_WEIG_Z.Text)
        '---------- ---------- ---------- ---------- ---------- ---------- MAIN TRACTION
        tmpObj(5).x.value = TXB_TRAC_X.Text
        tmpObj(5).y.value = TXB_TRAC_Y.Text
        tmpObj(5).z.value = TXB_TRAC_Z.Text
        '---------- ---------- ---------- ---------- ---------- ---------- IDLE SHEAVE 1
        tmpObj(6).x.value = TXB_IDLE1_X.Text
        tmpObj(6).y.value = TXB_IDLE1_Y.Text
        tmpObj(6).z.value = TXB_IDLE1_Z.Text
        '---------- ---------- ---------- ---------- ---------- ---------- IDLE SHEAVE 2
        tmpObj(7).x.value = TXB_IDLE2_X.Text
        tmpObj(7).y.value = TXB_IDLE2_Y.Text
        tmpObj(7).z.value = TXB_IDLE2_Z.Text
        On Error GoTo 0
        Return 0
    End Function

    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    '  TOWER_POSITIONING.
    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    Public Sub TOWER_POSITIONING()
        Call SKELETON_SEARCH(False)

        '---------- ---------- ---------- ---------- ---------- ---------- #POINTS FOR LAY-OUT
        Dim oLAY_OUT = Skeleton_Body.parent.item("#LAY-OUT").hybridbodies.item("#POINTS FOR LAY-OUT").hybridshapes

        '---------- ---------- ---------- ---------- ---------- ---------- E.H
        oLAY_OUT.item("ENTRANCE HEAD PT").z.value = TXT_EH.Text
        '---------- ---------- ---------- ---------- ---------- ---------- 격랍층
        oLAY_OUT.item("PARKING LEVEL PT").z.value = TXT_GH.Text
        '---------- ---------- ---------- ---------- ---------- ---------- O.H
        oLAY_OUT.item("OVER HEAD PT").z.value = TXT_OH.Text
        '---------- ---------- ---------- ---------- ---------- ---------- PIT
        oLAY_OUT.item("BASEMENT PARKING LEVEL PT").z.value = Val(TXT_PIT.Text) * -1
        '---------- ---------- ---------- ---------- ---------- ---------- B.M
        oLAY_OUT.item("FIT & TURN TABLE PT").z.value = (Val(TXT_BM.Text) + Val(TXT_PIT.Text)) * -1
    End Sub

    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    '  IS_EVEN_NUMBER.
    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    Public Function IS_EVEN_NUMBER(Input_Num As Object)
        Try
            Dim tmpNum As Integer = Input_Num.text Mod 2
            Dim tmp_TXT As String = ""

            If Not tmpNum = 0 Then
                If Input_Num.name = "TXT_TOP_SD" Then
                    tmp_TXT = "격랍층 - 상부 : SEDAN"
                ElseIf Input_Num.name = "TXT_TOP_RV" Then
                    tmp_TXT = "격랍층 - 상부 : RV"
                ElseIf Input_Num.name = "TXT_BOTTOM_SD" Then
                    tmp_TXT = "격랍층 - 하부 : SEDAN"
                ElseIf Input_Num.name = "TXT_BOTTOM_RV" Then
                    tmp_TXT = "격랍층 - 하부 : RV"
                End If

                Dim MSG_Content As String = tmp_TXT & "의 입력 값은 짝수여야 합니다!"
                MsgBox(MSG_Content, vbOKOnly Or vbInformation, "ISPark CATIA Automation")
                Call PROGRESSBAR_CHANGE("ERROR : 차량 대수 입력 값을 확인하세요!", 40)
                Return 7
            End If
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "ISPark CATIA Automation")
        End Try
        Return 0
    End Function

    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    '  CREATE_PALLET_POSITION.
    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    Public Sub CREATE_PALLET_POSITION(oPallet_Instance)
        Call CATIA_BASIC_V6()
        '---------- ---------- ---------- ---------- ---------- ---------- 스켈레톤 파트 찾기.
        Dim Skel_Part As VPMInstances = oActiveObject.PLMEntity.Instances
        Dim oPallet_Part As AnyObject = Nothing
        For i = 1 To Skel_Part.Count
            If InStr(1, Skel_Part.Item(i).Name, "#DET#SKELETON") = 1 Then
                oSelection.Clear()
                oSelection.Add(Skel_Part.Item(i))

                oPallet_Part = oSelection.Item(1).Value.RepOccurrences.Item(1).RelatedRepInstance.ReferenceInstanceOf.GetItem("Part")
                oSelection.Clear()
                Exit For
            End If
        Next
        '---------- ---------- ---------- ---------- ---------- ---------- 
        oSelection.Search("Name='ENTRANCE AXS', all")
        Dim oEntrance_AXIS = oSelection.Item(1).Value
        Dim oEntrance_PT = oPallet_Part.HybridBodies.GetItem("#LAY-OUT").HybridBodies.GetItem("#POINTS FOR LAY-OUT").HybridShapes.GetItem("ENTRANCE PT")
        Dim oAXIS_Reference As Reference = oPallet_Part.CreateReferenceFromObject(oEntrance_AXIS)
        Dim oPT_Reference As Reference = oPallet_Part.CreateReferenceFromObject(oEntrance_PT)
        oSelection.Clear()
        '---------- ---------- ---------- ---------- ---------- ---------- 각 지오메트리셋 얻기.
        Dim oPallet_Body = oPallet_Part.HybridBodies.GetItem("#PALLETS")
        Dim oParking_Level = oPallet_Body.HybridBodies.GetItem("#PARKING LEVEL")
        Dim oBasement_Level = oPallet_Body.HybridBodies.GetItem("#BASEMENT PARKING LEVEL")

        '---------- ---------- ---------- ---------- ---------- ---------- 파랫트용 지오메트리컬 초기화
        Dim oSD_Leftt = oParking_Level.HybridBodies.GetItem("@POINTS FOR PALLET").HybridBodies.GetItem("@LEFT SIDE BASE ON DRIVE-IN").HybridBodies.GetItem("@SEDAN")
        Dim oSD_Right = oParking_Level.HybridBodies.GetItem("@POINTS FOR PALLET").HybridBodies.GetItem("@RIGHT SIDE BASE ON DRIVE-IN").HybridBodies.GetItem("@SEDAN")
        Dim oRV_Leftt = oParking_Level.HybridBodies.GetItem("@POINTS FOR PALLET").HybridBodies.GetItem("@LEFT SIDE BASE ON DRIVE-IN").HybridBodies.GetItem("@RV")
        Dim oRV_Right = oParking_Level.HybridBodies.GetItem("@POINTS FOR PALLET").HybridBodies.GetItem("@RIGHT SIDE BASE ON DRIVE-IN").HybridBodies.GetItem("@RV")

        On Error Resume Next
        oSelection.Clear()
        For i = 1 To oSD_Leftt.HybridShapes.Count
            oSelection.Add(oSD_Leftt.HybridShapes.Item(i))
        Next
        For i = 1 To oSD_Right.HybridShapes.Count
            oSelection.Add(oSD_Right.HybridShapes.Item(i))
        Next
        For i = 1 To oRV_Leftt.HybridShapes.Count
            oSelection.Add(oRV_Leftt.HybridShapes.Item(i))
        Next
        For i = 1 To oRV_Right.HybridShapes.Count
            oSelection.Add(oRV_Right.HybridShapes.Item(i))
        Next
        oSelection.Delete()

        oSD_Leftt = oBasement_Level.HybridBodies.GetItem("@POINTS FOR PALLET").HybridBodies.GetItem("@LEFT SIDE BASE ON DRIVE-IN").HybridBodies.GetItem("@SEDAN")
        oSD_Right = oBasement_Level.HybridBodies.GetItem("@POINTS FOR PALLET").HybridBodies.GetItem("@RIGHT SIDE BASE ON DRIVE-IN").HybridBodies.GetItem("@SEDAN")
        oRV_Leftt = oBasement_Level.HybridBodies.GetItem("@POINTS FOR PALLET").HybridBodies.GetItem("@LEFT SIDE BASE ON DRIVE-IN").HybridBodies.GetItem("@RV")
        oRV_Right = oBasement_Level.HybridBodies.GetItem("@POINTS FOR PALLET").HybridBodies.GetItem("@RIGHT SIDE BASE ON DRIVE-IN").HybridBodies.GetItem("@RV")

        oSelection.Clear()
        For i = 1 To oSD_Leftt.HybridShapes.Count
            oSelection.Add(oSD_Leftt.HybridShapes.Item(i))
        Next
        For i = 1 To oSD_Right.HybridShapes.Count
            oSelection.Add(oSD_Right.HybridShapes.Item(i))
        Next
        For i = 1 To oRV_Leftt.HybridShapes.Count
            oSelection.Add(oRV_Leftt.HybridShapes.Item(i))
        Next
        For i = 1 To oRV_Right.HybridShapes.Count
            oSelection.Add(oRV_Right.HybridShapes.Item(i))
        Next
        oSelection.Delete()

        oSelection.Clear()
        oSelection.Search("Name='pallet axis for*', all")
        oSelection.Delete()
        On Error GoTo 0

        '---------- ---------- ---------- ---------- ---------- ---------- PT
        Call CREATE_PALLET_POINT(oPallet_Part, oAXIS_Reference, oPT_Reference, TXT_TOP_SD, TXT_TOP_RV, oParking_Level, EH_Value, GH_SD, GH_RV, 1)
        Call CREATE_PALLET_POINT(oPallet_Part, oAXIS_Reference, oPT_Reference, TXT_BOTTOM_SD, TXT_BOTTOM_RV, oBasement_Level, TXT_BM.Text, Pit_SD, Pit_RV, 3)
        '---------- ---------- ---------- ---------- ---------- ---------- AXIS
        Call CREATE_PALLET_AXIS(oPallet_Instance, oPallet_Part, oEntrance_AXIS, oEntrance_PT, oParking_Level, 1)
        Call CREATE_PALLET_AXIS(oPallet_Instance, oPallet_Part, oEntrance_AXIS, oEntrance_PT, oBasement_Level, 3)

        oSelection.Clear()
    End Sub

    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    '  Create PT for Pallet.
    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    Public Sub CREATE_PALLET_POINT(oPallet_Part, oAXIS_Reference, oPT_Reference, TXT_SD, TXT_RV, oBasement_Level, Default_Value, Increment_SD, Increment_RV, iTmp)
        Dim oHybridShapeFactory = oPallet_Part.HybridShapeFactory

        Dim oSD_Leftt = oBasement_Level.HybridBodies.GetItem("@POINTS FOR PALLET").HybridBodies.GetItem("@LEFT SIDE BASE ON DRIVE-IN").HybridBodies.GetItem("@SEDAN")
        Dim oSD_Right = oBasement_Level.HybridBodies.GetItem("@POINTS FOR PALLET").HybridBodies.GetItem("@RIGHT SIDE BASE ON DRIVE-IN").HybridBodies.GetItem("@SEDAN")
        Dim oRV_Leftt = oBasement_Level.HybridBodies.GetItem("@POINTS FOR PALLET").HybridBodies.GetItem("@LEFT SIDE BASE ON DRIVE-IN").HybridBodies.GetItem("@RV")
        Dim oRV_Right = oBasement_Level.HybridBodies.GetItem("@POINTS FOR PALLET").HybridBodies.GetItem("@RIGHT SIDE BASE ON DRIVE-IN").HybridBodies.GetItem("@RV")

        Dim iZ_Value As Integer = Default_Value + 43.5
        Dim HybridShapePointCoord As HybridShapePointCoord = Nothing

        '---------- ---------- ---------- ---------- ---------- ---------- -인지 +인지..
        Dim iEvenNum = 1
        If oBasement_Level.Name = "#BASEMENT PARKING LEVEL" Then
            iEvenNum = iEvenNum * -1
        End If

        '---------- ---------- ---------- ---------- ---------- ---------- SD
        For i = 1 To Val(TXT_SD.Text) / 2
            If Not i = 1 Then iZ_Value += Increment_SD

            '---------- ---------- ---------- ---------- ---------- ---------- 
            HybridShapePointCoord = oHybridShapeFactory.AddNewPointCoord(2320.0#, 0.0#, iZ_Value * iEvenNum)
            HybridShapePointCoord.Name = "PALLET PT FOR SEDAN#" & iTmp & "-" & i
            HybridShapePointCoord.PtRef = oPT_Reference
            HybridShapePointCoord.RefAxisSystem = oAXIS_Reference
            oSD_Leftt.AppendHybridShape(HybridShapePointCoord)
            '---------- ---------- ---------- ---------- ---------- ---------- 
            HybridShapePointCoord = oHybridShapeFactory.AddNewPointCoord(-2320.0#, 0.0#, iZ_Value * iEvenNum)
            HybridShapePointCoord.Name = "PALLET PT FOR SEDAN#" & iTmp + 1 & "-" & i
            HybridShapePointCoord.PtRef = oPT_Reference
            HybridShapePointCoord.RefAxisSystem = oAXIS_Reference
            oSD_Right.AppendHybridShape(HybridShapePointCoord)
        Next
        '---------- ---------- ---------- ---------- ---------- ---------- RV
        For i = 1 To Val(TXT_RV.Text) / 2
            '---------- ---------- ---------- ---------- ---------- ---------- SD이 없을 경우 디폴트 값 다시 입력.
            If iZ_Value = 0 Then
                iZ_Value = Default_Value + 43.5
            ElseIf (oBasement_Level.name = "#PARKING LEVEL") And (i = 1) Then
                iZ_Value += Increment_SD
            Else
                iZ_Value += Increment_RV
            End If

            '---------- ---------- ---------- ---------- ---------- ---------- 
            HybridShapePointCoord = oHybridShapeFactory.AddNewPointCoord(2320.0#, 0.0#, iZ_Value * iEvenNum)
            HybridShapePointCoord.Name = "PALLET PT FOR RV#" & iTmp & "-" & i
            HybridShapePointCoord.PtRef = oPT_Reference
            HybridShapePointCoord.RefAxisSystem = oAXIS_Reference
            oRV_Leftt.AppendHybridShape(HybridShapePointCoord)
            '---------- ---------- ---------- ---------- ---------- ---------- 
            HybridShapePointCoord = oHybridShapeFactory.AddNewPointCoord(-2320.0#, 0.0#, iZ_Value * iEvenNum)
            HybridShapePointCoord.Name = "PALLET PT FOR RV#" & iTmp + 1 & "-" & i
            HybridShapePointCoord.PtRef = oPT_Reference
            HybridShapePointCoord.RefAxisSystem = oAXIS_Reference
            oRV_Right.AppendHybridShape(HybridShapePointCoord)
        Next

        '---------- ---------- ---------- ---------- ---------- ---------- 격납층일 때 오른쪽 제일 위 PT 삭제.
        If oBasement_Level.Name = "#PARKING LEVEL" Then
            oSelection.Clear()
            If Not TXT_RV.Text = 0 Then
                oSelection.Add(oRV_Right.HybridShapes.Item(oRV_Right.HybridShapes.Count))
            Else
                oSelection.Add(oSD_Right.HybridShapes.Item(oSD_Right.HybridShapes.Count))
            End If
            oSelection.Delete()
            oSelection.Clear()
        End If

        oPallet_Part.Update()
    End Sub

    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    '  Create AXIS for Pallet.
    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    Public Sub CREATE_PALLET_AXIS(oPallet_Instance, oPallet_Part, oEntrance_AXIS, oEntrance_PT, oBasement_Level, iTmp)
        Dim oSD_Left = oBasement_Level.HybridBodies.GetItem("@POINTS FOR PALLET").HybridBodies.GetItem("@LEFT SIDE BASE ON DRIVE-IN").HybridBodies.GetItem("@SEDAN")
        Dim oRV_Left = oBasement_Level.HybridBodies.GetItem("@POINTS FOR PALLET").HybridBodies.GetItem("@LEFT SIDE BASE ON DRIVE-IN").HybridBodies.GetItem("@RV")
        Dim oSD_Right = oBasement_Level.HybridBodies.GetItem("@POINTS FOR PALLET").HybridBodies.GetItem("@RIGHT SIDE BASE ON DRIVE-IN").HybridBodies.GetItem("@SEDAN")
        Dim oRV_Right = oBasement_Level.HybridBodies.GetItem("@POINTS FOR PALLET").HybridBodies.GetItem("@RIGHT SIDE BASE ON DRIVE-IN").HybridBodies.GetItem("@RV")

        Dim oSD_Left_Axis = oBasement_Level.HybridBodies.GetItem("@AXIS FOR  PALLET").HybridBodies.GetItem("@LEFT SIDE BASE ON DRIVE-IN").HybridBodies.GetItem("@SEDAN")
        Dim oRV_Left_Axis = oBasement_Level.HybridBodies.GetItem("@AXIS FOR  PALLET").HybridBodies.GetItem("@LEFT SIDE BASE ON DRIVE-IN").HybridBodies.GetItem("@RV")
        Dim oSD_Right_Axis = oBasement_Level.HybridBodies.GetItem("@AXIS FOR  PALLET").HybridBodies.GetItem("@RIGHT SIDE BASE ON DRIVE-IN").HybridBodies.GetItem("@SEDAN")
        Dim oRV_Right_Axis = oBasement_Level.HybridBodies.GetItem("@AXIS FOR  PALLET").HybridBodies.GetItem("@RIGHT SIDE BASE ON DRIVE-IN").HybridBodies.GetItem("@RV")

        Dim oRoot_AxisSystems As AxisSystems = oPallet_Part.AxisSystems

        '---------- ---------- ---------- ---------- ---------- ---------- AxisSystem에 있는거 복사 붙이기.
        '---------- ---------- ---------- ---------- ---------- ---------- oSD_Left_Axis
        For i = 1 To oSD_Left.HybridShapes.Count
            oSelection.Clear()
            oSelection.Add(oEntrance_AXIS)
            oSelection.Copy()
            oSelection.Clear()
            oSelection.Add(oSD_Left_Axis)
            oSelection.PasteSpecial("CATPrtResultWithOutLink")
            Dim oClone_Axis As AxisSystem = oSelection.Item(1).Value
            oSelection.Clear()

            Dim oRef_PT As Reference = oPallet_Part.CreateReferenceFromGeometry(oSD_Left.HybridShapes.Item(i))
            Dim Axis_Reference As Reference = oPallet_Part.CreateReferenceFromBRepName("REdge:(Edge:(Face:(Brp:(" & oEntrance_AXIS.Name & ";3);None:();Cf11:());Face:(Brp:(" & oEntrance_AXIS.Name & ";2);None:();Cf11:());None:(Limits1:();Limits2:());Cf11:());WithPermanentBody;WithoutBuildError;WithSelectingFeatureSupport;MFBRepVersion_CXR15)", oEntrance_AXIS)
            Dim Axis_Y_Reference As Reference = oPallet_Part.CreateReferenceFromBRepName("REdge:(Edge:(Face:(Brp:(" & oEntrance_AXIS.Name & ";2);None:();Cf11:());Face:(Brp:(" & oEntrance_AXIS.Name & ";1);None:();Cf11:());None:(Limits1:();Limits2:());Cf11:());WithPermanentBody;WithoutBuildError;WithSelectingFeatureSupport;MFBRepVersion_CXR15)", oEntrance_AXIS)

            oClone_Axis.Name = "PALLET AXIS FOR SEDAN#" & iTmp & "-" & i

            oClone_Axis.IsCurrent = False

            oClone_Axis.Type = CATAxisSystemMainType.catAxisSystemAxisRotation
            oClone_Axis.OriginType = CATAxisSystemOriginType.catAxisSystemOriginByPoint
            oClone_Axis.OriginPoint = oRef_PT

            oClone_Axis.AxisRotationAngle.Value = 1.0#
            oClone_Axis.AxisRotationReference = Axis_Reference

            oClone_Axis.YAxisDirection = Axis_Y_Reference
            oClone_Axis.XAxisType = CATAxisSystemAxisType.catAxisSystemAxisSameDirection
            oClone_Axis.ZAxisType = CATAxisSystemAxisType.catAxisSystemAxisSameDirection

            '---------- ---------- ---------- ---------- ---------- ---------- Instance 추가하고 구속 생성.
            Call CREATE_PALLET_INSTANCE(oPallet_Instance, oClone_Axis)
        Next
        '---------- ---------- ---------- ---------- ---------- ---------- oRV_Left_Axis
        For i = 1 To oRV_Left.HybridShapes.Count
            oSelection.Clear()
            oSelection.Add(oEntrance_AXIS)
            oSelection.Copy()
            oSelection.Clear()
            oSelection.Add(oRV_Left_Axis)
            oSelection.PasteSpecial("CATPrtResultWithOutLink")
            Dim oClone_Axis As AxisSystem = oSelection.Item(1).Value
            oSelection.Clear()

            Dim oRef_PT As Reference = oPallet_Part.CreateReferenceFromGeometry(oRV_Left.HybridShapes.Item(i))
            Dim Axis_Reference As Reference = oPallet_Part.CreateReferenceFromBRepName("REdge:(Edge:(Face:(Brp:(" & oEntrance_AXIS.Name & ";3);None:();Cf11:());Face:(Brp:(" & oEntrance_AXIS.Name & ";2);None:();Cf11:());None:(Limits1:();Limits2:());Cf11:());WithPermanentBody;WithoutBuildError;WithSelectingFeatureSupport;MFBRepVersion_CXR15)", oEntrance_AXIS)
            Dim Axis_Y_Reference As Reference = oPallet_Part.CreateReferenceFromBRepName("REdge:(Edge:(Face:(Brp:(" & oEntrance_AXIS.Name & ";2);None:();Cf11:());Face:(Brp:(" & oEntrance_AXIS.Name & ";1);None:();Cf11:());None:(Limits1:();Limits2:());Cf11:());WithPermanentBody;WithoutBuildError;WithSelectingFeatureSupport;MFBRepVersion_CXR15)", oEntrance_AXIS)

            oClone_Axis.Name = "PALLET AXIS FOR RV#" & iTmp & "-" & i

            oClone_Axis.IsCurrent = False

            oClone_Axis.Type = CATAxisSystemMainType.catAxisSystemAxisRotation
            oClone_Axis.OriginType = CATAxisSystemOriginType.catAxisSystemOriginByPoint
            oClone_Axis.OriginPoint = oRef_PT

            oClone_Axis.AxisRotationAngle.Value = 1.0#
            oClone_Axis.AxisRotationReference = Axis_Reference

            oClone_Axis.YAxisDirection = Axis_Y_Reference
            oClone_Axis.XAxisType = CATAxisSystemAxisType.catAxisSystemAxisSameDirection
            oClone_Axis.ZAxisType = CATAxisSystemAxisType.catAxisSystemAxisSameDirection

            '---------- ---------- ---------- ---------- ---------- ---------- Instance 추가하고 구속 생성.
            Call CREATE_PALLET_INSTANCE(oPallet_Instance, oClone_Axis)
        Next

        '---------- ---------- ---------- ---------- ---------- ---------- oSD_Right_Axis
        For i = 1 To oSD_Right.HybridShapes.Count
            oSelection.Clear()
            oSelection.Add(oEntrance_AXIS)
            oSelection.Copy()
            oSelection.Clear()
            oSelection.Add(oSD_Right_Axis)
            oSelection.PasteSpecial("CATPrtResultWithOutLink")
            Dim oClone_Axis As AxisSystem = oSelection.Item(1).Value
            oSelection.Clear()

            Dim oRef_PT As Reference = oPallet_Part.CreateReferenceFromGeometry(oSD_Right.HybridShapes.Item(i))
            Dim Axis_Reference As Reference = oPallet_Part.CreateReferenceFromBRepName("REdge:(Edge:(Face:(Brp:(" & oEntrance_AXIS.Name & ";3);None:();Cf11:());Face:(Brp:(" & oEntrance_AXIS.Name & ";2);None:();Cf11:());None:(Limits1:();Limits2:());Cf11:());WithPermanentBody;WithoutBuildError;WithSelectingFeatureSupport;MFBRepVersion_CXR15)", oEntrance_AXIS)
            Dim Axis_Y_Reference As Reference = oPallet_Part.CreateReferenceFromBRepName("REdge:(Edge:(Face:(Brp:(" & oEntrance_AXIS.Name & ";2);None:();Cf11:());Face:(Brp:(" & oEntrance_AXIS.Name & ";1);None:();Cf11:());None:(Limits1:();Limits2:());Cf11:());WithPermanentBody;WithoutBuildError;WithSelectingFeatureSupport;MFBRepVersion_CXR15)", oEntrance_AXIS)

            oClone_Axis.Name = "PALLET AXIS FOR SEDAN#" & iTmp + 1 & "-" & i

            oClone_Axis.IsCurrent = False

            oClone_Axis.Type = CATAxisSystemMainType.catAxisSystemAxisRotation
            oClone_Axis.OriginType = CATAxisSystemOriginType.catAxisSystemOriginByPoint
            oClone_Axis.OriginPoint = oRef_PT

            oClone_Axis.AxisRotationAngle.Value = -1.0#
            oClone_Axis.AxisRotationReference = Axis_Reference

            oClone_Axis.YAxisDirection = Axis_Y_Reference
            oClone_Axis.XAxisType = CATAxisSystemAxisType.catAxisSystemAxisSameDirection
            oClone_Axis.ZAxisType = CATAxisSystemAxisType.catAxisSystemAxisSameDirection

            '---------- ---------- ---------- ---------- ---------- ---------- Instance 추가하고 구속 생성.
            Call CREATE_PALLET_INSTANCE(oPallet_Instance, oClone_Axis)
        Next
        '---------- ---------- ---------- ---------- ---------- ---------- oRV_Right_Axis
        For i = 1 To oRV_Right.HybridShapes.Count
            oSelection.Clear()
            oSelection.Add(oEntrance_AXIS)
            oSelection.Copy()
            oSelection.Clear()
            oSelection.Add(oRV_Right_Axis)
            oSelection.PasteSpecial("CATPrtResultWithOutLink")
            Dim oClone_Axis As AxisSystem = oSelection.Item(1).Value
            oSelection.Clear()

            Dim oRef_PT As Reference = oPallet_Part.CreateReferenceFromGeometry(oRV_Right.HybridShapes.Item(i))
            Dim Axis_Reference As Reference = oPallet_Part.CreateReferenceFromBRepName("REdge:(Edge:(Face:(Brp:(" & oEntrance_AXIS.Name & ";3);None:();Cf11:());Face:(Brp:(" & oEntrance_AXIS.Name & ";2);None:();Cf11:());None:(Limits1:();Limits2:());Cf11:());WithPermanentBody;WithoutBuildError;WithSelectingFeatureSupport;MFBRepVersion_CXR15)", oEntrance_AXIS)
            Dim Axis_Y_Reference As Reference = oPallet_Part.CreateReferenceFromBRepName("REdge:(Edge:(Face:(Brp:(" & oEntrance_AXIS.Name & ";2);None:();Cf11:());Face:(Brp:(" & oEntrance_AXIS.Name & ";1);None:();Cf11:());None:(Limits1:();Limits2:());Cf11:());WithPermanentBody;WithoutBuildError;WithSelectingFeatureSupport;MFBRepVersion_CXR15)", oEntrance_AXIS)

            oClone_Axis.Name = "PALLET AXIS FOR RV#" & iTmp + 1 & "-" & i

            oClone_Axis.IsCurrent = False

            oClone_Axis.Type = CATAxisSystemMainType.catAxisSystemAxisRotation
            oClone_Axis.OriginType = CATAxisSystemOriginType.catAxisSystemOriginByPoint
            oClone_Axis.OriginPoint = oRef_PT

            oClone_Axis.AxisRotationAngle.Value = -1.0#
            oClone_Axis.AxisRotationReference = Axis_Reference

            oClone_Axis.YAxisDirection = Axis_Y_Reference
            oClone_Axis.XAxisType = CATAxisSystemAxisType.catAxisSystemAxisSameDirection
            oClone_Axis.ZAxisType = CATAxisSystemAxisType.catAxisSystemAxisSameDirection

            '---------- ---------- ---------- ---------- ---------- ---------- Instance 추가하고 구속 생성.
            Call CREATE_PALLET_INSTANCE(oPallet_Instance, oClone_Axis)
        Next

        oPallet_Part.Update()
        CATIA.StartCommand("Update")
        CATIA.StartCommand("Update")
    End Sub

    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    '  CREATE_PALLET_INSTANCE.
    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    Public Sub CREATE_PALLET_INSTANCE(oPallet_Instance, oClone_Axis)
        Call CATIA_BASIC_V6()

        Dim MyContext = CATIA.ActiveEditor.GetService("PLMProductContext")
        Dim myMCXParent As VPMReference = MyContext.RootOccurrence.ReferenceRootOccurrenceOf
        Dim RootInstanceConnections As EngConnections = myMCXParent.GetItem("CATEngConnections")

        Dim Eng_Type As CatEngConnectionType = CatEngConnectionType.catRigid
        Dim Assy_Type As CatAssemblyConstraintType = CatAssemblyConstraintType.catCoincidenceAxisSystemAxisSystem

        Dim Root_Connections As EngConnection = Nothing
        Dim Sub_Connections As AssemblyConstraint = Nothing

        oSelection.Clear()
        oSelection.Add(oPallet_Instance)
        oSelection.Copy()
        oSelection.Clear()
        oSelection.Add(oActiveObject)
        oSelection.Paste()
        System.Threading.Thread.Sleep(1000)

        Dim oPallet_Instace = oSelection.Item(1).Value

        '---------- ---------- ---------- ---------- ---------- ---------- 
        Dim Root_Eng_Arr(2)
        Root_Eng_Arr(0) = "DET#MACHINE ASSY"
        Root_Eng_Arr(1) = oPallet_Instace.Name
        Root_Eng_Arr(2) = "#DET#SKELETON.1"

        Dim Sub_Eng_Arr(1)
        Sub_Eng_Arr(0) = Root_Eng_Arr(1) & "/" & "#PALLET ASSY#SKELETON#AXIS"
        Sub_Eng_Arr(1) = Root_Eng_Arr(2) & "/" & "#DET#SKELETON#3D SHAPE/" & oClone_Axis.Name

        Root_Connections = RootInstanceConnections.Add(Eng_Type, Root_Eng_Arr)
        Sub_Connections = Root_Connections.AssemblyConstraints.Add(Assy_Type, Sub_Eng_Arr)

        Root_Connections.Name = "Pallets"
    End Sub

#End Region

#Region "---------- 버튼 이벤트 "
    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    '  폼 로드.
    '---------- ---------- ---------- ---------- ---------- ---------- ----------
    Private Sub Main_Form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            '---------- ---------- ---------- ---------- ---------- ---------- 
            Me.Visible = False
            Init_Form.Show()

            '---------- ---------- ---------- ---------- ---------- ---------- 기본정보 엑셀 값 가져오기
            Dim Exist_Result = THISFILEEXIST(TXT_EXCEL_PATH.Text, TXT_EXCEL_FILE_NAME.Text)
            If Exist_Result = "" Then
                Dim MSG_Content As String = "기본정보 엑셀을 찾을 수 없어 종료합니다!"
                MsgBox(MSG_Content & vbCrLf & _
                       "-> " & TXT_EXCEL_PATH.Text & "\" & TXT_EXCEL_FILE_NAME.Text, _
                       vbOKOnly Or vbInformation, "ISPark CATIA Automation")
                End
            End If

            Call OPEN_EXCEL_FILE(TXT_EXCEL_PATH.Text, TXT_EXCEL_FILE_NAME.Text, False)
            Call GET_BASIC_INFO()
            Call CLOSE_EXCEL(False, "")

            '---------- ---------- ---------- ---------- ---------- ---------- 
            TXT_TOP_SD.Text = "0"
            TXT_TOP_RV.Text = "0"
            TXT_BOTTOM_SD.Text = "0"
            TXT_BOTTOM_RV.Text = "0"

            '---------- ---------- ---------- ---------- ---------- ---------- 
            Init_Form.Close()
            Me.Visible = True

            Call CATIA_BASIC_V6()
            Call PROGRESSBAR_CHANGE("동양메닉스 승강기식 자동 조립 프로그램.", 100)
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "ISPark CATIA Automation")
        End Try
    End Sub

    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    '  폼 크기 변경시 적용 안됨.
    '---------- ---------- ---------- ---------- ---------- ---------- ----------
    Private Sub Main_Form_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        Me.Height = 672

        If Admin_Manu = 1 Then
            Me.Width = 919
            Button1.Visible = True
            GBX_SHEAVE_1.Visible = True
            GBX_SHEAVE_2.Visible = True
        Else
            Me.Width = 608
            Button1.Visible = False
            GBX_SHEAVE_1.Visible = False
            GBX_SHEAVE_2.Visible = False
        End If
    End Sub

    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    '  탭 변경시 관리자 메뉴의 탭도 같이 변환.
    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    Private Sub TabControl1_Click(sender As Object, e As EventArgs) Handles TabControl1.Click
        If TabControl1.SelectedIndex = 0 Then
            TabControl2.SelectedIndex = 0
        ElseIf TabControl1.SelectedIndex = 1 Then
            TabControl2.SelectedIndex = 1
        ElseIf TabControl1.SelectedIndex = 2 Then
            TabControl2.SelectedIndex = 2
        End If
    End Sub

    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    '  관리자 메뉴 보이기.
    '---------- ---------- ---------- ---------- ---------- ---------- ----------
    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click
        If Admin_Manu = 1 Then
            Admin_Manu = 0
        Else
            Admin_Manu = 1
        End If

        Me.Width = 607
    End Sub

    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    '  종료 버튼 클릭.
    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    Public Function PROGRAM_END() Handles Me.Disposed, BTN_EXIT.Click
        On Error Resume Next
        Dim tmpStr = MsgBox("프로그램을 종료합니다.", vbOKCancel Or vbInformation, "ISPark CATIA Automation")

        If tmpStr = vbOK Then
            Me.Close()
        End If
        On Error GoTo 0
        Return 0
    End Function

    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    '  테스트용 파트 넘버 넣기.
    '---------- ---------- ---------- ---------- ---------- ---------- ----------
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        TXT_LIFT.Text = "M1EALT001A#LIFT ASSY"
        TXT_PALL.Text = "M2EAPT001A#PALLET ASSY"
        TXT_TURN.Text = "" '"M2EATT001A#TURN TABLE ASSY"
        TXT_WEIG.Text = "M2EACW001A#COUNTER WEIGHT ASSY"
        TXT_TRAC.Text = "M2EALD001A#MAIN TRACTION ASSY"
        TXT_IDE1.Text = "M2EALD100A#IDLE SHEAVE 1 ASSY"
        TXT_IDE2.Text = "M2EALD200A#IDLE SHEAVE 2 ASSY"
    End Sub
#End Region


End Class


