﻿Module Excel
    '---------- ---------- ---------- ---------- ---------- ---------- Excel-related Variables
    Public xlApp, xlBook, xlSheets, Run_xlApp_Count As Object

    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    '  Excel File Open
    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    Public Sub OPEN_EXCEL_FILE(xlFilePath As String, xlFileName As String, Visible_Option As Boolean)
        Try
            '---------- ---------- ---------- ---------- ---------- ---------- 프로그램 시작전 엑셀 수.
            Run_xlApp_Count = GET_EXCEL_COUNT("EXCEL.EXE")

            xlApp = CreateObject("excel.application")
            xlApp.Visible = Visible_Option

            xlBook = xlApp.Workbooks.Open(xlFilePath & "\" & xlFileName, , True)
            'xlSheets = xlBook.Worksheets(1)
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "ISPark CATIA Automation")
        End Try
    End Sub

    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    '  Excel App Close
    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    Public Sub CLOSE_EXCEL(IsSave As Boolean, sfile As String)
        Try
            xlApp.DisplayAlerts = False

            If IsSave = True Then
                xlBook.Close(True, sfile)
            Else
                xlBook.Close(False)
            End If

            xlApp.DisplayAlerts = True
            xlApp.Quit()

            xlSheets = Nothing
            xlBook = Nothing
            xlApp = Nothing

            Call KILL_EXCEL("EXCEL.EXE", Run_xlApp_Count)
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "ISPark CATIA Automation")
        End Try
    End Sub

    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    '  GET_TEMPLATE_NAME.
    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    Public Sub GET_TEMPLATE_NAME()
        Template_Name = oEntity.GetAttributeValue("V_Name")
        Template_Part_Name = oEntity.Instances.Item(1).ReferenceInstanceOf.GetAttributeValue("V_Name")
    End Sub

    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    '  GET_EXCEL_VALUE
    '---------- ---------- ---------- ---------- ---------- ---------- ---------- 
    Public Function GET_EXCEL_VALUE(Sheet_Name As String, Exc_Col As Integer, Exc_Row As Integer)
        Dim Excel_Arr(Exc_Col, 1) As String

        Try
            xlSheets = xlBook.Worksheets(Sheet_Name)

            '---------- ---------- ---------- ---------- ---------- ---------- 엑셀 값 가져오기.
            Dim IsExist As Boolean = False
            Exc_Row = 0

            Do
                '---------- ---------- ---------- ---------- ---------- ---------- 반복 될 때 마다 배열 크기 수정.
                Exc_Row += 1
                ReDim Preserve Excel_Arr(Exc_Col, Exc_Row)

                '---------- ---------- ---------- ---------- ---------- ---------- 배열에 엑셀 값 넣기.
                For i = 1 To Exc_Col
                    Dim Row_String As String = Microsoft.VisualBasic.ChrW(64 + i)   'A, B, C 순으로 증가..

                    Excel_Arr(i, Exc_Row) = xlSheets.Range(Row_String & Exc_Row).Value
                Next

                '---------- ---------- ---------- ---------- ---------- ---------- 엑셀에 빈칸이면 종료.
                If xlSheets.Range("A" & Exc_Row + 1).Value = "" Then
                    IsExist = True
                End If
            Loop Until IsExist = True

            Excel_Arr(0, 0) = Exc_Row
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "ISPark CATIA Automation")
        End Try

        Return Excel_Arr
    End Function
End Module
