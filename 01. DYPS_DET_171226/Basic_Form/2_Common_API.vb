﻿Imports INFITF
Imports PLMModelerBaseIDL
Imports ProductStructureClientIDL
Imports PLMAccessIDLItf


Module Common_API

#Region "---------- 전역 변수들 "
    '---------- ---------- ---------- ---------- ---------- ---------- CATIA V6 Variables
    Public CATIA As Object
    Public oEditor As Object, oActiveObject As Object, oSelection As Selection, oEntity As PLMEntity
    '---------- ---------- ---------- ---------- ---------- ---------- 
    Public Template_Name As String, Template_Part_Name As String, Dummy_Car As String
    Public sSearch_Type As String = "V_Name"
    Public Admin_Manu As Integer = 0
    Public Skeleton_Body As Object, oSelected_BOM As Object
    '---------- ---------- ---------- ---------- ---------- ---------- 
    Public Bom_Exc_Lv As Integer
    Public BOM_VALUE(1000, 11)
#End Region

#Region "---------- 카티아 실행 여부 판단. "
    '---------- ---------- ---------- ---------- V6
    Public Function CATIA_BASIC_V6()
        On Error Resume Next
        Err.Clear()
        CATIA = GetObject(, "CATIA.Application")
        If Err.Number = 0 Then
            oEditor = CATIA.ActiveEditor
            oActiveObject = oEditor.ActiveObject
            If Not Err.Number = 0 Then
                Return False
            End If

            oEntity = oActiveObject.PLMEntity
            oSelection = oEditor.Selection

            'MsgBox("CATIA 실행 후 프로그램을 사용해주세요.", vbInformation, "ISPark CATIA Automation")
            'System.Windows.Forms.Application.Exit()
        End If
        On Error GoTo 0
        Return True
    End Function
#End Region

#Region "---------- V6 Search & Open. "
    Public Function CATIA_SEARCH(Search_Type As String, Search_Name As String, IsOpen As Boolean) As Object
        Try
            '---------- ---------- ---------- ---------- ---------- ---------- Open 대상 찾기.
            Dim oSearchService As Object = CATIA.GetSessionService("Search")
            Dim oSearches As DatabaseSearch = oSearchService.DatabaseSearch
            oSearches.BaseType = "VPMReference"
            oSearches.AddEasyCriteria(Search_Type, Search_Name)      '"V_Name"
            oSearches.AddEasyCriteria("revision", "A.1")             '검색시 결과가 여러개 나올수 있어서...
            oSearchService.Search()

            '---------- ---------- ---------- ---------- ---------- ---------- 검색 결과된 데이터가 없으면 종료.
            If oSearches.SearchCount = 0 Then Return 0

            '---------- ---------- ---------- ---------- ---------- ---------- 최신 리비전으로 열기.
            Dim oEntity As PLMEntity = Nothing
            Dim iRight_Number As Integer        ' 오른쪽 3번째 부터..
            Dim sOri_Value = ""                 ' 최신 리비전 값.
            Dim sSep_Value = ""                 ' 비교 값.

            '---------- ---------- ---------- ---------- ---------- ---------- 검색 결과가 2개 이상이면..
            If oSearches.SearchCount >= 2 Then
                For i = 1 To oSearches.SearchCount
                    iRight_Number = 3

                    '---------- ---------- ---------- ---------- ---------- ---------- sOri_Value 값 저장.
                    If sOri_Value = "" Then
                        For j = iRight_Number To 5
                            Dim sItem_Name = oSearch.EditedContent.Item(i).Name
                            '---------- ---------- ---------- ---------- ---------- ---------- 파일명에서 띄워쓰기 찾기.
                            If Mid(sItem_Name, Len(sItem_Name) - j, 1) = " " Then
                                sOri_Value = Mid(sItem_Name, Len(sItem_Name) - j + 1, Len(sItem_Name))

                                oEntity = oSearch.EditedContent.Item(i)
                                Exit For
                            End If
                        Next
                    End If

                    '---------- ---------- ---------- ---------- ---------- ---------- sSep_Value 값 찾기.
                    If Not i = 1 Then
                        For j = iRight_Number To 5
                            Dim sItem_Name = oSearch.EditedContent.Item(i).Name
                            '---------- ---------- ---------- ---------- ---------- ---------- 파일명에서 띄워쓰기 찾기.
                            If Mid(sItem_Name, Len(sItem_Name) - j, 1) = " " Then
                                sSep_Value = Mid(sItem_Name, Len(sItem_Name) - j + 1, Len(sItem_Name))
                                Exit For
                            End If
                        Next

                        '---------- ---------- ---------- ---------- ---------- ---------- sOri_Value, sSep_Value 비교.
                        If Left(sOri_Value, 1) = Left(sSep_Value, 1) Then
                            '---------- ---------- ---------- ---------- ---------- ---------- 문자가 같으면 뒤에 숫자 비교.
                            Dim sTmp_1
                            sTmp_1 = Split(sOri_Value, ".")
                            Dim sTmp_2
                            sTmp_2 = Split(sSep_Value, ".")

                            If sTmp_1(1) < sTmp_2(1) Then
                                sOri_Value = sSep_Value
                                oEntity = oSearch.EditedContent.Item(i)
                            End If
                        ElseIf Left(sOri_Value, 1) < Left(sSep_Value, 1) Then
                            '---------- ---------- ---------- ---------- ---------- ---------- 문자가 크면 최신 리비전.
                            sOri_Value = sSep_Value
                            oEntity = oSearch.EditedContent.Item(i)
                        End If
                    End If
                Next
            Else
                oEntity = oSearch.EditedContent.Item(1)
            End If

            If IsOpen = True Then
                '---------- ---------- ---------- ---------- ---------- ---------- Open 대상 열기.
                Dim oEditor As Object = Nothing
                Dim oSessionService 'As PLMOpenService
                oSessionService = CATIA.GetSessionService("PLMOpenService")
                oSessionService.PLMOpen(oEntity, oEditor)
                System.Threading.Thread.Sleep(500)

                Return oEditor
            Else
                Return 1
            End If
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "ISPark CATIA Automation")
        End Try
        Return 0
    End Function
#End Region

#Region "---------- Activate Next Window. "
    Public Sub ACTIVATE_NEXT_WINDOW(Window_Title As String, IsClose As Boolean)
        '---------- ---------- ---------- ---------- ---------- ---------- 열려있는 탭 수 만큼 반복.
        For i = 1 To CATIA.Windows.Count
            If CATIA.Windows.Item(i).Name = Window_Title Then
                If IsClose = True Then
                    CATIA.Windows.Item(i).Close()
                    System.Threading.Thread.Sleep(1000)
                Else
                    CATIA.Windows.Item(i).Activate()
                    System.Threading.Thread.Sleep(1000)
                End If
                Exit For
            End If
        Next i
    End Sub
#End Region

#Region "---------- 실행되어 있는 엑셀 프로세스 갯수 체크. "
    Public Function GET_EXCEL_COUNT(Program_Name As String)
        On Error Resume Next
        Dim wmi As Object = GetObject("winmgmts:")
        Dim sQuery As String = "select * from win32_process where name='" & Program_Name & "'"
        Dim Processes As Object = wmi.execquery(sQuery)

        On Error GoTo 0
        Return Processes.Count
    End Function
#End Region

#Region "---------- 프로세스 강제 종료. "
    Public Sub KILL_EXCEL(Program_Name As String, Process_Count As Integer)
        Try
            '---------- ---------- ---------- ---------- ---------- ---------- 현재 프로세스 갯수.
            Dim wmi As Object = GetObject("winmgmts:")
            Dim sQuery As String = "select * from win32_process where name='" & Program_Name & "'"
            Dim processes = wmi.execquery(sQuery)

            '---------- ---------- ---------- ---------- ---------- ---------- 수량 비교 후 Process 종료.
            Dim Exc_PID, PID_Len, process
            Dim processes_count_value = 0
            Dim run_excel_value = 1

            If Not Process_Count = processes.Count Then
                For Each process In processes
                    If run_excel_value = processes.Count Then
                        Exc_PID = Split(process.path_.relpath, "=")
                        PID_Len = Len(Exc_PID(1)) - 2
                        Exc_PID = Mid(Exc_PID(1), 2, PID_Len)
                        Shell("taskkill /f /im " & Exc_PID, AppWinStyle.Hide)
                    End If
                    run_excel_value = run_excel_value + 1
                Next
            End If

            wmi = Nothing
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "ISPark CATIA Automation")
        End Try
    End Sub
#End Region

#Region "---------- 파일이 존재하는지 여부 체크. "
    Public Function THISFILEEXIST(FolderPath As String, FilePath As String) As String
        Dim sResult As String = ""
        Try
            Dim fso As Object = CreateObject("Scripting.FileSystemObject")

            If Not (fso.FileExists(FolderPath & "\" & FilePath)) Then
                Return sResult
            Else
                Return FolderPath & "\" & FilePath
            End If
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "ISPark CATIA Automation")
        End Try
        Return sResult
    End Function
#End Region

#Region "---------- INSERT_COMBOBOX_VALUE. "
    Public Sub INSERT_COMBOBOX_VALUE(Arr_Value As Object, ComboBox As ComboBox, ListBox As ListBox)
        On Error Resume Next
        ComboBox.Items.Clear()
        ListBox.Items.Clear()

        Dim Row_Count As Integer = 2
        If ComboBox.Name = "CMB_LIFT_2" Then
            Row_Count = 3
        ElseIf ComboBox.Name = "CMB_LIFT_3" Then
            Row_Count = 4
        ElseIf ComboBox.Name = "CMB_LIFT_4" Then
            Row_Count = 6
        ElseIf ComboBox.Name = "CMB_LIFT_5" Then
            Row_Count = 7
        ElseIf ComboBox.Name = "CMB_TURN_2" Then
            Row_Count = 3
        ElseIf ComboBox.Name = "CMB_WEIG_2" Then
            Row_Count = 3
        ElseIf ComboBox.Name = "CMB_TRAC_2" Then
            Row_Count = 3
        End If

        '---------- ---------- ---------- ---------- ---------- ---------- 중복 제거를 위해..
        Dim i As Integer
        For i = 2 To Arr_Value(0, 0)
            If Not Arr_Value(Row_Count, i) = "" Then
                Dim IsExist As Boolean = False

                '---------- ---------- ---------- ---------- ---------- ---------- Listbox에 있는지..
                For j = 0 To ListBox.Items.Count
                    Dim tmpStr As String = ListBox.Items(j)

                    If ListBox.Items.Count = 0 Then
                        IsExist = False
                        Exit For
                    ElseIf tmpStr = Arr_Value(Row_Count, i) Then
                        IsExist = True
                        Exit For
                    End If
                Next

                '---------- ---------- ---------- ---------- ---------- ---------- 없으면 추가.
                If IsExist = False Then
                    ListBox.Items.Add(Arr_Value(Row_Count, i))
                End If
            Else
                Exit For
            End If
        Next

        '---------- ---------- ---------- ---------- ---------- ---------- ComboBox에 추가.
        For i = 0 To ListBox.Items.Count
            ComboBox.Items.Add(ListBox.Items(i))
        Next

        ComboBox.Text = ComboBox.Items(0)
        On Error GoTo 0
    End Sub
#End Region

#Region "---------- # 기호 구분하기. "
    Public Function SEPARATE_SYMBOL(Separate_Item, Item_Count) As Integer
        Dim IsFirstPallet As Boolean = False
        Bom_Exc_Lv += 1

        On Error Resume Next
        For i = 1 To Separate_Item.instances.Count
            Dim IsOverLap As Boolean = False         'BOM 출력시 파트 넘버가 중복인지 검사..
            Dim IsOverLap_Num As Integer = 0
            Dim oVPMRef As AnyObject = Separate_Item.instances.Item(i)
            Dim oVPMRef_Name As String = oVPMRef.ReferenceInstanceOf.GetAttributeValue("V_Name")

            '---------- ---------- ---------- ---------- ---------- ---------- 2lv 대상들..
            Dim tmp_Value = Split(oVPMRef_Name, "#")
            If tmp_Value(1) = "COUNTER WEIGHT ASSY" Or tmp_Value(1) = "MAIN TRACTION ASSY" Or _
                    tmp_Value(1) = "IDLE SHEAVE 1 ASSY" Or tmp_Value(1) = "IDLE SHEAVE 2 ASSY" Or _
                    tmp_Value(1) = "PALLET ASSY" Then
                Bom_Exc_Lv = 2
            End If


            'If Not (Left(oVPMRef_Name, 1) = "#") And Not (IsFirstPallet = True) Then
            If Not Left(oVPMRef_Name, 1) = "#" Then
                'If IsFirstPallet = False And tmp_Value(1) = "PALLET ASSY" Then
                '    IsFirstPallet = True
                'End If

                For j = Item_Count To 1 Step -1
                    '---------- ---------- ---------- ---------- ---------- ---------- 현재 레벨에서만 검사.
                    'If BOM_VALUE(j, 1) = Bom_Exc_Lv - 1 Then
                    '    Exit For
                    'End If
                    '---------- ---------- ---------- ---------- ---------- ---------- 품번과 레벨 비교.
                    If (BOM_VALUE(j, 0) = oVPMRef_Name) And _
                        (BOM_VALUE(j, 1) = Bom_Exc_Lv) Then
                        IsOverLap = True
                        IsOverLap_Num = j
                        Exit For
                    End If
                Next

                Dim oTmp_Instances As VPMReference = Nothing
                If IsOverLap = True Then
                    '---------- ---------- ---------- ---------- ---------- ---------- 파트 넘버 중복이면..
                    BOM_VALUE(IsOverLap_Num, 6) += 1

                    Dim tmp_Count As Integer = 0
                    For j = IsOverLap_Num To Item_Count
                        tmp_Count += 1

                        '---------- ---------- ---------- ---------- ---------- ---------- 중복 대상이 Assy이면..
                        oTmp_Instances = oVPMRef.ReferenceInstanceOf
                        If Not oTmp_Instances.Instances.Count = 0 Then
                            Call OVERLAP_COUNT(oTmp_Instances, IsOverLap_Num, BOM_VALUE(IsOverLap_Num, 1), Item_Count)
                            Exit For
                        Else
                            Exit For
                        End If
                    Next
                Else
                    '---------- ---------- ---------- ---------- ---------- ---------- 파트 넘버 중복 아니면..
                    Item_Count += 1

                    Dim Split_Name = Split(oVPMRef_Name, "#")
                    BOM_VALUE(Item_Count, 0) = oVPMRef_Name
                    BOM_VALUE(Item_Count, 1) = Bom_Exc_Lv
                    BOM_VALUE(Item_Count, 2) = Split_Name(0)
                    BOM_VALUE(Item_Count, 3) = Split_Name(1)
                    BOM_VALUE(Item_Count, 5) = oVPMRef.ReferenceInstanceOf.GetAttributeValue("revision")
                    BOM_VALUE(Item_Count, 6) = 1
                    BOM_VALUE(Item_Count, 7) = 1
                    '---------- ---------- ---------- ---------- ---------- ---------- 사이즈, 길이, 비고.
                    Split_Name = Split(oVPMRef.ReferenceInstanceOf.GetAttributeValue("V_description"), "#")
                    If Split_Name.Length = 1 Then
                        BOM_VALUE(Item_Count, 9) = Replace(Split_Name(0), Chr(10), "")
                    ElseIf Split_Name.Length = 2 Then
                        BOM_VALUE(Item_Count, 9) = Replace(Split_Name(0), Chr(10), "")
                        BOM_VALUE(Item_Count, 10) = Replace(Split_Name(1), Chr(10), "")
                    ElseIf Split_Name.Length = 3 Then
                        BOM_VALUE(Item_Count, 9) = Replace(Split_Name(0), Chr(10), "")
                        BOM_VALUE(Item_Count, 10) = Replace(Split_Name(1), Chr(10), "")
                        BOM_VALUE(Item_Count, 11) = Replace(Split_Name(2), Chr(10), "")
                    End If
                    '---------- ---------- ---------- ---------- ---------- ---------- 재질 & 무게.
                    Dim sCreatedFrom As String = ""
                    sCreatedFrom = oVPMRef.ReferenceInstanceOf.GetAttributeValue("V_versionComment")
                    If Not sCreatedFrom = "" Then
                        Split_Name = Split(sCreatedFrom, "/")
                        BOM_VALUE(Item_Count, 4) = Split_Name(0)
                        BOM_VALUE(Item_Count, 8) = Left(Split_Name(1), Len(Split_Name(1)) - 2)  '무게의 Kg 글자 빼고 입력.
                    Else
                        BOM_VALUE(Item_Count, 4) = "ASSY"
                        BOM_VALUE(Item_Count, 8) = ""
                        BOM_VALUE(Item_Count, 9) = ""
                        BOM_VALUE(Item_Count, 10) = ""
                    End If

                    oTmp_Instances = oVPMRef.ReferenceInstanceOf
                    If oTmp_Instances.Instances.Count > 0 Then
                        Item_Count = SEPARATE_SYMBOL(oTmp_Instances, Item_Count)
                    End If
                End If
            End If
        Next
        On Error GoTo 0

        Bom_Exc_Lv -= 1
        Return Item_Count
    End Function

    Public Sub OVERLAP_COUNT(oTmp_Instances, IsOverLap_Num, tmp_LV, tmp_Num)
        On Error Resume Next
        For j = 1 To oTmp_Instances.Instances.Count
            Dim otmpRef As AnyObject = oTmp_Instances.Instances.Item(j)
            Dim otmpRef_Name As String = otmpRef.ReferenceInstanceOf.GetAttributeValue("V_Name")
            '---------- ---------- ---------- ---------- ---------- ---------- 품번과 레벨 비교.
            Dim IsOverLap_Num2 = Nothing
            Dim tmp_LV2 = Nothing
            For k = 1 To tmp_Num
                If (BOM_VALUE(IsOverLap_Num + k, 0) = otmpRef_Name) And _
                    (BOM_VALUE(IsOverLap_Num + k, 1) = tmp_LV + 1) Then

                    If BOM_VALUE(IsOverLap_Num + k, 6) >= 1 Then
                        BOM_VALUE(IsOverLap_Num + k, 6) += 1
                    End If
                    IsOverLap_Num2 = IsOverLap_Num + k
                    tmp_LV2 = BOM_VALUE(IsOverLap_Num + k, 1)
                    Exit For
                End If
            Next

            Dim oOverLap_Instance As VPMReference = otmpRef.ReferenceInstanceOf
            If Not oOverLap_Instance.Instances.Count = 0 Then
                Call OVERLAP_COUNT(oOverLap_Instance, IsOverLap_Num, tmp_LV2, tmp_Num)
            End If
        Next
        On Error GoTo 0
    End Sub
#End Region

#Region "---------- BOM Excel에 값 입력하기. "
    Public Sub INSERT_BOM_EXCEL(BOM_VALUE, Item_Count, Wire_Arr)
        On Error Resume Next
        Dim Start_Num As Integer = 2

        For i = 1 To Item_Count
            Dim Row_String As String = Microsoft.VisualBasic.ChrW(66 + BOM_VALUE(i, 1))
            xlSheets.Range(Row_String & i + Start_Num).Value() = "O"            '레벨
            xlSheets.Range("K" & i + Start_Num).Value() = BOM_VALUE(i, 2)       '품번
            xlSheets.Range("L" & i + Start_Num).Value() = BOM_VALUE(i, 5)       '리비전
            xlSheets.Range("M" & i + Start_Num).Value() = BOM_VALUE(i, 3)       '품명
            xlSheets.Range("N" & i + Start_Num).Value() = BOM_VALUE(i, 9)       '규격
            If BOM_VALUE(i, 10) = 0 Then
                xlSheets.Range("O" & i + Start_Num).Value() = ""
            Else
                xlSheets.Range("O" & i + Start_Num).Value() = BOM_VALUE(i, 10)      '길이
            End If
            xlSheets.Range("P" & i + Start_Num).Value() = BOM_VALUE(i, 6)       '제작 대수
            xlSheets.Range("Q" & i + Start_Num).Value() = BOM_VALUE(i, 7)       '기본 소요량
            xlSheets.Range("S" & i + Start_Num).Value() = BOM_VALUE(i, 4)       '재질
            xlSheets.Range("T" & i + Start_Num).Value() = BOM_VALUE(i, 8)       '중량
            xlSheets.Range("V" & i + Start_Num).Value() = BOM_VALUE(i, 11)      '비고
        Next

        Dim Lv_String As String
        Dim IsAssy As Boolean = False
        Dim Now_Lv As Integer = Nothing
        Dim Now_Row As Integer = 0

        For i = 3 To Item_Count + Start_Num
            Dim Result_Weight = ""
            Dim Total_Result_Weight = ""
            '---------- ---------- ---------- ---------- ---------- ---------- Root Assy.
            If i = 3 Then
                For j = 4 To Item_Count + Start_Num
                    If (xlSheets.Range("D" & j).Value = "O") Then
                        IsAssy = True
                        Result_Weight = Result_Weight & "T" & j & ", "
                        Total_Result_Weight = Total_Result_Weight & "U" & j & ", "
                    End If
                Next
                '---------- ---------- ---------- ---------- ---------- ---------- Product가 한개이면..
                If IsAssy = False Then
                    For j = 4 To Item_Count + Start_Num
                        Result_Weight = Result_Weight & "T" & j & ", "
                        Total_Result_Weight = Total_Result_Weight & "U" & j & ", "
                    Next
                End If
                '---------- ---------- ---------- ---------- ---------- ---------- 합산 값 입력.
                IsAssy = False
                xlSheets.Range("T" & i).Value = "=sum(" & Left(Result_Weight, Len(Result_Weight) - 2) & ")"
                xlSheets.Range("U" & i).Value = "=sum(" & Left(Total_Result_Weight, Len(Total_Result_Weight) - 2) & ")"
            Else
                '---------- ---------- ---------- ---------- ---------- ---------- 제일 처음 Assy 찾기.
                For j = i To Item_Count + Start_Num
                    If xlSheets.Range("S" & j).Value = "ASSY" Then
                        '---------- ---------- ---------- ---------- ---------- ---------- Lv 찾기.
                        For k = 1 To 10
                            Lv_String = Microsoft.VisualBasic.ChrW(66 + k)
                            If xlSheets.Range(Lv_String & j).Value = "O" Then
                                Now_Lv = k
                                Now_Row = j
                                IsAssy = True
                                Exit For
                            End If
                        Next
                    End If

                    If IsAssy = True Then
                        IsAssy = False
                        Exit For
                    End If
                Next

                '---------- ---------- ---------- ---------- ---------- ---------- 같은 Lv의 무게 합산.
                Lv_String = Microsoft.VisualBasic.ChrW(66 + Now_Lv + 1)
                For j = Now_Row + 1 To Item_Count + Start_Num
                    If xlSheets.Range(Lv_String & j).Value = "O" Then
                        Result_Weight = Result_Weight & "T" & j & ", "
                        Total_Result_Weight = Total_Result_Weight & "U" & j & ", "
                    Else
                        Dim Lv_String_tmp = Microsoft.VisualBasic.ChrW(66 + Now_Lv)
                        If xlSheets.Range(Lv_String_tmp & j).Value = "O" Then
                            Exit For
                        End If
                    End If
                Next

                '---------- ---------- ---------- ---------- ---------- ---------- 합산 값 입력.
                xlSheets.Range("T" & Now_Row).Value = "=sum(" & Left(Result_Weight, Len(Result_Weight) - 2) & ")"
                xlSheets.Range("U" & Now_Row).Value = "=sum(" & Left(Total_Result_Weight, Len(Total_Result_Weight) - 2) & ")"
            End If
        Next
        On Error GoTo 0

        Call CATIA_BASIC_V6()
        On Error Resume Next
        '---------- ---------- ---------- ---------- ---------- ---------- BOM에 WIRE 정보 입력.
        Dim oVPMOccurrences As VPMOccurrence = oActiveObject.Occurrences.Item(1)
        Dim oSkel_Part As AnyObject = oVPMOccurrences.RepOccurrences.Item(1).RelatedRepInstance.ReferenceInstanceOf.GetItem("Part")
        Dim oParameters As AnyObject = oSkel_Part.Parameters.RootParameterSet.DirectParameters
        Dim oParam_1st = oParameters.Item("Wire_Length_1st").ValueAsString
        Dim oParam_4th = oParameters.Item("Wire_Length_4th").ValueAsString

        For i = 1 To Val(Wire_Arr(0, 0))
            If Wire_Arr(3, i) = oParam_1st Then
                Dim tmp_Value = Split(Wire_Arr(5, i), "#")

                xlSheets.Range("D" & Item_Count + Start_Num + 1).Value = "O"                      '레벨
                xlSheets.Range("K" & Item_Count + Start_Num + 1).Value = tmp_Value(0)          '품번
                xlSheets.Range("M" & Item_Count + Start_Num + 1).Value = tmp_Value(1)         '품명
                xlSheets.Range("N" & Item_Count + Start_Num + 1).Value = Wire_Arr(2, i)          '규격
                xlSheets.Range("O" & Item_Count + Start_Num + 1).Value = Wire_Arr(3, i)          '길이
                xlSheets.Range("P" & Item_Count + Start_Num + 1).Value = "3"                      'Unit QTY
                xlSheets.Range("Q" & Item_Count + Start_Num + 1).Value = "1"                      'Product QTY
                xlSheets.Range("S" & Item_Count + Start_Num + 1).Value = "Standard"             '재질

                Dim tmp_Weight = Split(Wire_Arr(3, i), "mm")
                Dim asda = tmp_Weight(0) * 0.878 * 0.001
                xlSheets.Range("T" & Item_Count + Start_Num + 1).Value = tmp_Weight(0) * 0.878 * 0.001      '무게
                xlSheets.Range("V" & Item_Count + Start_Num + 1).Value = Wire_Arr(4, i)          '비고
                Exit For
            End If
        Next
        For i = 1 To Val(Wire_Arr(0, 0))
            If Wire_Arr(3, i) = oParam_4th Then
                Dim tmp_Value = Split(Wire_Arr(5, i), "#")

                If tmp_Value(0) = xlSheets.Range("K" & Item_Count + Start_Num + 1).Value Then
                    xlSheets.Range("P" & Item_Count + Start_Num + 1).Value = "6"                      'Unit QTY
                Else
                    xlSheets.Range("D" & Item_Count + Start_Num + 2).Value = "O"                      '레벨
                    xlSheets.Range("K" & Item_Count + Start_Num + 2).Value = tmp_Value(0)          '품번
                    xlSheets.Range("M" & Item_Count + Start_Num + 2).Value = tmp_Value(1)         '품명
                    xlSheets.Range("N" & Item_Count + Start_Num + 2).Value = Wire_Arr(2, i)          '규격
                    xlSheets.Range("O" & Item_Count + Start_Num + 2).Value = Wire_Arr(3, i)          '길이
                    xlSheets.Range("P" & Item_Count + Start_Num + 2).Value = "3"                      'Unit QTY
                    xlSheets.Range("Q" & Item_Count + Start_Num + 2).Value = "1"                      'Product QTY
                    xlSheets.Range("S" & Item_Count + Start_Num + 2).Value = "Standard"             '재질

                    Dim tmp_Weight = Split(Wire_Arr(3, i), "mm")
                    Dim asda = tmp_Weight(0) * 0.878 * 0.001
                    xlSheets.Range("T" & Item_Count + Start_Num + 2).Value = tmp_Weight(0) * 0.878 * 0.001      '무게
                    xlSheets.Range("V" & Item_Count + Start_Num + 2).Value = Wire_Arr(4, i)          '비고
                End If
                Exit For
            End If
        Next
        On Error GoTo 0

        '---------- ---------- ---------- ---------- ---------- ---------- 단위 붙이기.
        For i = 3 To Item_Count + Start_Num
            If Not xlSheets.Range("O" & i).Value = "" Then
                xlSheets.Range("O" & i).Value = xlSheets.Range("O" & i).Value & " mm"
            End If

            xlSheets.Range("U" & i).Value = xlSheets.Range("U" & i).Value & " Kg"
            xlSheets.Range("T" & i).Value = xlSheets.Range("T" & i).Value & " Kg"
        Next
    End Sub
#End Region

#Region "---------- 기계 부품 위치 값, Sub Assy 부품명 찾기. "
    Public Function MACHINE_LOCATION(Arr_Value As Object, Location_Start As Integer, _
                                     Option_Count As Integer, Option_Start As Integer, _
                                     TextBox_Main As Object, TextBox_Location As Object)
        'On Error Resume Next
        '---------- ---------- ---------- ---------- ---------- ---------- 좌표 값 계산.
        Dim i, j, k As Integer
        For i = 2 To Arr_Value(0, 0)
            If TextBox_Main(Option_Start).Text = Arr_Value(Location_Start, i) Then
                TextBox_Location(0).text = Arr_Value(Location_Start + 1, i)
                TextBox_Location(1).text = Arr_Value(Location_Start + 2, i)
                TextBox_Location(2).text = Arr_Value(Location_Start + 3, i)
                Exit For
            End If
        Next

        '---------- ---------- ---------- ---------- ---------- ---------- Part Name 검색.
        Dim iFile_Name_Position As Integer = 4
        If Left(TextBox_Main(1).Name, 8) = "CMB_TRAC" Then
            iFile_Name_Position = 13
        ElseIf Left(TextBox_Main(1).Name, 8) = "CMB_WEIG" Then
            iFile_Name_Position = 5
        ElseIf Left(TextBox_Main(1).Name, 8) = "CMB_LIFT" Then
            iFile_Name_Position = 10
        End If

        Dim IsSame As Boolean = False
        Dim sResult As String = ""

        For i = 2 To Val(Arr_Value(0, 0))
            If TextBox_Main(1).text = Arr_Value(2, i) Then
                If Option_Count >= 2 Then
                    For j = 2 To Val(Arr_Value(0, 0))
                        If (TextBox_Main(1).text = Arr_Value(2, j)) And (TextBox_Main(2).text = Arr_Value(3, j)) Then
                            If Option_Count >= 3 Then
                                For k = 2 To Val(Arr_Value(0, 0))
                                    If (TextBox_Main(1).text = Arr_Value(2, k)) And (TextBox_Main(2).text = Arr_Value(3, k)) And _
                                       (TextBox_Main(3).text = Arr_Value(4, k)) Then
                                        If Option_Count >= 4 Then
                                            For g = 2 To Val(Arr_Value(0, 0))
                                                If (TextBox_Main(1).text = Arr_Value(2, g)) And (TextBox_Main(2).text = Arr_Value(3, g)) And _
                                                   (TextBox_Main(3).text = Arr_Value(4, g)) And (TextBox_Main(4).text = Arr_Value(6, g)) Then
                                                    If Option_Count >= 5 Then
                                                        For f = 2 To Val(Arr_Value(0, 0))
                                                            If (TextBox_Main(1).text = Arr_Value(2, f)) And (TextBox_Main(2).text = Arr_Value(3, f)) And _
                                                               (TextBox_Main(3).text = Arr_Value(4, f)) And (TextBox_Main(4).text = Arr_Value(6, f)) And _
                                                               (TextBox_Main(5).text = Arr_Value(7, f)) Then
                                                                sResult = Arr_Value(10, f)
                                                                IsSame = True
                                                            End If

                                                            If IsSame = True Then
                                                                Exit For
                                                            End If
                                                        Next
                                                    Else
                                                        sResult = Arr_Value(iFile_Name_Position, j)
                                                        IsSame = True
                                                    End If
                                                End If

                                                If IsSame = True Then
                                                    Exit For
                                                End If
                                            Next
                                        Else
                                            sResult = Arr_Value(iFile_Name_Position, j)
                                            IsSame = True
                                        End If
                                    End If

                                    If IsSame = True Then
                                        Exit For
                                    End If
                                Next
                            Else
                                sResult = Arr_Value(iFile_Name_Position, j)
                                IsSame = True
                            End If
                        End If

                        If IsSame = True Then
                            Exit For
                        End If
                    Next
                Else
                    sResult = Arr_Value(3, i)
                    IsSame = True
                End If
            End If

            If IsSame = True Then
                Exit For
            End If
        Next
        'On Error GoTo 0

        Return sResult
    End Function
#End Region

#Region "---------- 스켈레톤이 있는지 여부 확인. "
    Public Sub SKELETON_SEARCH(IsShow As Boolean)
        On Error Resume Next
        Call CATIA_BASIC_V6()

        'oSelection.Add(oActiveObject)
        'CATIA.StartCommand("Expand All Levels")
        'CATIA.StartCommand("Collapse All")

        oSelection.Clear()
        oSelection.Search("Name='#VISUALIZATION', all")
        Skeleton_Body = oSelection.Item(1).Value
        oSelection.Clear()
        oSelection.Add(Skeleton_Body)

        Dim VisPropertySet As VisPropertySet = oSelection.VisProperties

        If IsShow = True Then
            VisPropertySet.SetShow(CatVisPropertyShow.catVisPropertyShowAttr)
        Else
            VisPropertySet.SetShow(CatVisPropertyShow.catVisPropertyNoShowAttr)
        End If
        oSelection.Clear()
        On Error GoTo 0
    End Sub
#End Region

#Region "---------- 프로그레스바 값 변경. "
    Public Sub PROGRESSBAR_CHANGE(State_Text As String, State_Value As Integer)
        Main_Form.ProgressBar1.Value = State_Value
        Main_Form.LBL_State.Text = State_Text

        If State_Value = 100 Then
            Main_Form.TopMost = True
        Else
            Main_Form.TopMost = False
        End If
    End Sub
#End Region

End Module


'On Error Resume Next
'On Error GoTo 0