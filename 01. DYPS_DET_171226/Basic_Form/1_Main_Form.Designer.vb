﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Main_Form
    Inherits System.Windows.Forms.Form

    'Form은 Dispose를 재정의하여 구성 요소 목록을 정리합니다.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows Form 디자이너에 필요합니다.
    Private components As System.ComponentModel.IContainer

    '참고: 다음 프로시저는 Windows Form 디자이너에 필요합니다.
    '수정하려면 Windows Form 디자이너를 사용하십시오.  
    '코드 편집기를 사용하여 수정하지 마십시오.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Main_Form))
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.BTN_EXIT = New System.Windows.Forms.Button()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.LBL_State = New System.Windows.Forms.Label()
        Me.LBL_State_tmp = New System.Windows.Forms.Label()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.GroupBox17 = New System.Windows.Forms.GroupBox()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.TXT_TOP_RV = New System.Windows.Forms.TextBox()
        Me.TXT_TOP_SD = New System.Windows.Forms.TextBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.GBX_FREECELL = New System.Windows.Forms.GroupBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.RBN_FREECELL_N = New System.Windows.Forms.RadioButton()
        Me.RBN_FREECELL_Y = New System.Windows.Forms.RadioButton()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.GroupBox13 = New System.Windows.Forms.GroupBox()
        Me.GBX_BOTTOM = New System.Windows.Forms.GroupBox()
        Me.RBN_BOTTOM_RV = New System.Windows.Forms.RadioButton()
        Me.RBN_BOTTOM_SD = New System.Windows.Forms.RadioButton()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.TXT_BOTTOM_RV = New System.Windows.Forms.TextBox()
        Me.TXT_BOTTOM_SD = New System.Windows.Forms.TextBox()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.GroupBox14 = New System.Windows.Forms.GroupBox()
        Me.BTN_TEMPLATE_OPEN = New System.Windows.Forms.Button()
        Me.BTN_BASIC_INFO = New System.Windows.Forms.Button()
        Me.BTN_CALCULATION = New System.Windows.Forms.Button()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.CMB_LIFT_5 = New System.Windows.Forms.ComboBox()
        Me.CMB_LIFT_4 = New System.Windows.Forms.ComboBox()
        Me.TXB_LIFT_Z = New System.Windows.Forms.TextBox()
        Me.TXB_LIFT_Y = New System.Windows.Forms.TextBox()
        Me.TXB_LIFT_X = New System.Windows.Forms.TextBox()
        Me.CMB_LIFT_3 = New System.Windows.Forms.ComboBox()
        Me.CMB_LIFT_2 = New System.Windows.Forms.ComboBox()
        Me.CMB_LIFT_1 = New System.Windows.Forms.ComboBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.GBX_SHEAVE_2 = New System.Windows.Forms.GroupBox()
        Me.TXB_IDLE2_Z = New System.Windows.Forms.TextBox()
        Me.TXB_IDLE2_Y = New System.Windows.Forms.TextBox()
        Me.TXB_IDLE2_X = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.CMB_IDLE_2 = New System.Windows.Forms.ComboBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.GBX_SHEAVE_1 = New System.Windows.Forms.GroupBox()
        Me.TXB_IDLE1_Z = New System.Windows.Forms.TextBox()
        Me.TXB_IDLE1_Y = New System.Windows.Forms.TextBox()
        Me.TXB_IDLE1_X = New System.Windows.Forms.TextBox()
        Me.CMB_IDLE_1 = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TXB_TRAC_Z = New System.Windows.Forms.TextBox()
        Me.TXB_TRAC_Y = New System.Windows.Forms.TextBox()
        Me.TXB_TRAC_X = New System.Windows.Forms.TextBox()
        Me.CMB_TRAC_2 = New System.Windows.Forms.ComboBox()
        Me.CMB_TRAC_1 = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.GroupBox11 = New System.Windows.Forms.GroupBox()
        Me.TXB_TURN_Z = New System.Windows.Forms.TextBox()
        Me.TXB_TURN_Y = New System.Windows.Forms.TextBox()
        Me.TXB_TURN_X = New System.Windows.Forms.TextBox()
        Me.CMB_TURN_2 = New System.Windows.Forms.ComboBox()
        Me.CMB_TURN_1 = New System.Windows.Forms.ComboBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.GroupBox10 = New System.Windows.Forms.GroupBox()
        Me.TXB_WEIG_Z = New System.Windows.Forms.TextBox()
        Me.TXB_WEIG_Y = New System.Windows.Forms.TextBox()
        Me.TXB_WEIG_X = New System.Windows.Forms.TextBox()
        Me.CMB_WEIG_2 = New System.Windows.Forms.ComboBox()
        Me.CMB_WEIG_1 = New System.Windows.Forms.ComboBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.GroupBox9 = New System.Windows.Forms.GroupBox()
        Me.TXB_PALL_Z = New System.Windows.Forms.TextBox()
        Me.TXB_PALL_Y = New System.Windows.Forms.TextBox()
        Me.TXB_PALL_X = New System.Windows.Forms.TextBox()
        Me.CMB_PALL = New System.Windows.Forms.ComboBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.BTN_MACHINE_POSITION = New System.Windows.Forms.Button()
        Me.BTN_MACHINE_CREATE = New System.Windows.Forms.Button()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.GroupBox20 = New System.Windows.Forms.GroupBox()
        Me.GroupBox18 = New System.Windows.Forms.GroupBox()
        Me.BTN_BOM_CREATE = New System.Windows.Forms.Button()
        Me.BTN_BOM_SELECTION = New System.Windows.Forms.Button()
        Me.TXT_BOM_SELECTION = New System.Windows.Forms.TextBox()
        Me.GroupBox15 = New System.Windows.Forms.GroupBox()
        Me.RBN_BOM_OPT2 = New System.Windows.Forms.RadioButton()
        Me.RBN_BOM_OPT1 = New System.Windows.Forms.RadioButton()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TabControl2 = New System.Windows.Forms.TabControl()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.GroupBox24 = New System.Windows.Forms.GroupBox()
        Me.TXT_SKEL = New System.Windows.Forms.TextBox()
        Me.TXT_ASSY = New System.Windows.Forms.TextBox()
        Me.TXT_PALLET_VIRTUAL = New System.Windows.Forms.TextBox()
        Me.TXT_CAR_RV = New System.Windows.Forms.TextBox()
        Me.TXT_CAR_SD = New System.Windows.Forms.TextBox()
        Me.TXT_FMIT = New System.Windows.Forms.TextBox()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.TXT_RESULT = New System.Windows.Forms.TextBox()
        Me.TXT_PIT = New System.Windows.Forms.TextBox()
        Me.TXT_GH = New System.Windows.Forms.TextBox()
        Me.TXT_EH = New System.Windows.Forms.TextBox()
        Me.TXT_BM = New System.Windows.Forms.TextBox()
        Me.TXT_OH = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.BASEMENT = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.GroupBox12 = New System.Windows.Forms.GroupBox()
        Me.LBX_Combobox_Sort = New System.Windows.Forms.ListBox()
        Me.TXT_PIT_S = New System.Windows.Forms.TextBox()
        Me.TXT_CAGE_S = New System.Windows.Forms.TextBox()
        Me.TXT_ENTER_S = New System.Windows.Forms.TextBox()
        Me.TXT_IDE2 = New System.Windows.Forms.TextBox()
        Me.TXT_IDE1 = New System.Windows.Forms.TextBox()
        Me.TXT_WEIG = New System.Windows.Forms.TextBox()
        Me.TXT_TRAC = New System.Windows.Forms.TextBox()
        Me.TXT_TURN = New System.Windows.Forms.TextBox()
        Me.TXT_PALL = New System.Windows.Forms.TextBox()
        Me.TXT_LIFT = New System.Windows.Forms.TextBox()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.GroupBox16 = New System.Windows.Forms.GroupBox()
        Me.TXT_EXCEL_PATH = New System.Windows.Forms.TextBox()
        Me.TXT_BOM_SELECTION22 = New System.Windows.Forms.TextBox()
        Me.TXT_PALLET_TMP = New System.Windows.Forms.TextBox()
        Me.TXT_BOM_EXCEL = New System.Windows.Forms.TextBox()
        Me.TXT_CONST_EXCEL = New System.Windows.Forms.TextBox()
        Me.TXT_EXCEL_FILE_NAME = New System.Windows.Forms.TextBox()
        Me.TXT_EXCEL_PATH2 = New System.Windows.Forms.TextBox()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.GroupBox2.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox17.SuspendLayout()
        Me.GBX_FREECELL.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox13.SuspendLayout()
        Me.GBX_BOTTOM.SuspendLayout()
        Me.GroupBox14.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.GBX_SHEAVE_2.SuspendLayout()
        Me.GBX_SHEAVE_1.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox11.SuspendLayout()
        Me.GroupBox10.SuspendLayout()
        Me.GroupBox9.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.GroupBox20.SuspendLayout()
        Me.GroupBox18.SuspendLayout()
        Me.GroupBox15.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.TabControl2.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.GroupBox24.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.GroupBox12.SuspendLayout()
        Me.TabPage6.SuspendLayout()
        Me.GroupBox16.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.BTN_EXIT)
        Me.GroupBox2.Controls.Add(Me.ProgressBar1)
        Me.GroupBox2.Controls.Add(Me.LBL_State)
        Me.GroupBox2.Controls.Add(Me.LBL_State_tmp)
        Me.GroupBox2.Font = New System.Drawing.Font("굴림", 10.0!)
        Me.GroupBox2.Location = New System.Drawing.Point(1, 685)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(738, 81)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "State"
        '
        'BTN_EXIT
        '
        Me.BTN_EXIT.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.BTN_EXIT.Font = New System.Drawing.Font("굴림", 10.0!)
        Me.BTN_EXIT.Location = New System.Drawing.Point(629, 23)
        Me.BTN_EXIT.Name = "BTN_EXIT"
        Me.BTN_EXIT.Size = New System.Drawing.Size(94, 44)
        Me.BTN_EXIT.TabIndex = 3
        Me.BTN_EXIT.Text = "종료"
        Me.BTN_EXIT.UseVisualStyleBackColor = True
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(18, 43)
        Me.ProgressBar1.MarqueeAnimationSpeed = 1000
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(595, 23)
        Me.ProgressBar1.TabIndex = 2
        '
        'LBL_State
        '
        Me.LBL_State.AutoSize = True
        Me.LBL_State.Location = New System.Drawing.Point(26, 23)
        Me.LBL_State.Name = "LBL_State"
        Me.LBL_State.Size = New System.Drawing.Size(21, 17)
        Me.LBL_State.TabIndex = 1
        Me.LBL_State.Text = "HI"
        '
        'LBL_State_tmp
        '
        Me.LBL_State_tmp.AutoSize = True
        Me.LBL_State_tmp.Location = New System.Drawing.Point(15, 23)
        Me.LBL_State_tmp.Name = "LBL_State_tmp"
        Me.LBL_State_tmp.Size = New System.Drawing.Size(16, 17)
        Me.LBL_State_tmp.TabIndex = 0
        Me.LBL_State_tmp.Text = "-"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Location = New System.Drawing.Point(1, 1)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(740, 682)
        Me.TabControl1.TabIndex = 8
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.GroupBox17)
        Me.TabPage1.Controls.Add(Me.GBX_FREECELL)
        Me.TabPage1.Controls.Add(Me.PictureBox2)
        Me.TabPage1.Controls.Add(Me.GroupBox13)
        Me.TabPage1.Controls.Add(Me.GroupBox14)
        Me.TabPage1.Location = New System.Drawing.Point(4, 27)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(732, 651)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "타워 높이"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'GroupBox17
        '
        Me.GroupBox17.Controls.Add(Me.Label51)
        Me.GroupBox17.Controls.Add(Me.Label50)
        Me.GroupBox17.Controls.Add(Me.TXT_TOP_RV)
        Me.GroupBox17.Controls.Add(Me.TXT_TOP_SD)
        Me.GroupBox17.Controls.Add(Me.Label36)
        Me.GroupBox17.Controls.Add(Me.Label37)
        Me.GroupBox17.Location = New System.Drawing.Point(14, 10)
        Me.GroupBox17.Name = "GroupBox17"
        Me.GroupBox17.Size = New System.Drawing.Size(308, 151)
        Me.GroupBox17.TabIndex = 3
        Me.GroupBox17.TabStop = False
        Me.GroupBox17.Text = "격납층 - 상부"
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Location = New System.Drawing.Point(222, 95)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(25, 17)
        Me.Label51.TabIndex = 4
        Me.Label51.Text = "대"
        Me.Label51.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Location = New System.Drawing.Point(222, 49)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(25, 17)
        Me.Label50.TabIndex = 4
        Me.Label50.Text = "대"
        Me.Label50.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'TXT_TOP_RV
        '
        Me.TXT_TOP_RV.Location = New System.Drawing.Point(116, 92)
        Me.TXT_TOP_RV.Name = "TXT_TOP_RV"
        Me.TXT_TOP_RV.Size = New System.Drawing.Size(100, 27)
        Me.TXT_TOP_RV.TabIndex = 3
        Me.TXT_TOP_RV.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TXT_TOP_SD
        '
        Me.TXT_TOP_SD.Location = New System.Drawing.Point(116, 46)
        Me.TXT_TOP_SD.Name = "TXT_TOP_SD"
        Me.TXT_TOP_SD.Size = New System.Drawing.Size(100, 27)
        Me.TXT_TOP_SD.TabIndex = 2
        Me.TXT_TOP_SD.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(72, 95)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(38, 17)
        Me.Label36.TabIndex = 2
        Me.Label36.Text = "RV :"
        Me.Label36.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(39, 49)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(69, 17)
        Me.Label37.TabIndex = 2
        Me.Label37.Text = "SEDAN :"
        Me.Label37.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'GBX_FREECELL
        '
        Me.GBX_FREECELL.Controls.Add(Me.Label35)
        Me.GBX_FREECELL.Controls.Add(Me.RBN_FREECELL_N)
        Me.GBX_FREECELL.Controls.Add(Me.RBN_FREECELL_Y)
        Me.GBX_FREECELL.Enabled = False
        Me.GBX_FREECELL.Location = New System.Drawing.Point(14, 167)
        Me.GBX_FREECELL.Name = "GBX_FREECELL"
        Me.GBX_FREECELL.Size = New System.Drawing.Size(308, 121)
        Me.GBX_FREECELL.TabIndex = 15
        Me.GBX_FREECELL.TabStop = False
        Me.GBX_FREECELL.Text = "E. H"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(51, 58)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(108, 17)
        Me.Label35.TabIndex = 2
        Me.Label35.Text = "프리셀 유무 :"
        Me.Label35.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'RBN_FREECELL_N
        '
        Me.RBN_FREECELL_N.AutoSize = True
        Me.RBN_FREECELL_N.Checked = True
        Me.RBN_FREECELL_N.Location = New System.Drawing.Point(189, 72)
        Me.RBN_FREECELL_N.Name = "RBN_FREECELL_N"
        Me.RBN_FREECELL_N.Size = New System.Drawing.Size(50, 21)
        Me.RBN_FREECELL_N.TabIndex = 1
        Me.RBN_FREECELL_N.TabStop = True
        Me.RBN_FREECELL_N.Text = "No"
        Me.RBN_FREECELL_N.UseVisualStyleBackColor = True
        '
        'RBN_FREECELL_Y
        '
        Me.RBN_FREECELL_Y.AutoSize = True
        Me.RBN_FREECELL_Y.Location = New System.Drawing.Point(189, 42)
        Me.RBN_FREECELL_Y.Name = "RBN_FREECELL_Y"
        Me.RBN_FREECELL_Y.Size = New System.Drawing.Size(57, 21)
        Me.RBN_FREECELL_Y.TabIndex = 0
        Me.RBN_FREECELL_Y.Text = "Yes"
        Me.RBN_FREECELL_Y.UseVisualStyleBackColor = True
        '
        'PictureBox2
        '
        Me.PictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(332, 20)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(387, 515)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 14
        Me.PictureBox2.TabStop = False
        '
        'GroupBox13
        '
        Me.GroupBox13.Controls.Add(Me.GBX_BOTTOM)
        Me.GroupBox13.Controls.Add(Me.Label3)
        Me.GroupBox13.Controls.Add(Me.Label33)
        Me.GroupBox13.Controls.Add(Me.TXT_BOTTOM_RV)
        Me.GroupBox13.Controls.Add(Me.TXT_BOTTOM_SD)
        Me.GroupBox13.Controls.Add(Me.Label52)
        Me.GroupBox13.Controls.Add(Me.Label53)
        Me.GroupBox13.Location = New System.Drawing.Point(14, 294)
        Me.GroupBox13.Name = "GroupBox13"
        Me.GroupBox13.Size = New System.Drawing.Size(308, 241)
        Me.GroupBox13.TabIndex = 4
        Me.GroupBox13.TabStop = False
        Me.GroupBox13.Text = "격납층 - 하부"
        '
        'GBX_BOTTOM
        '
        Me.GBX_BOTTOM.Controls.Add(Me.RBN_BOTTOM_RV)
        Me.GBX_BOTTOM.Controls.Add(Me.RBN_BOTTOM_SD)
        Me.GBX_BOTTOM.Enabled = False
        Me.GBX_BOTTOM.Location = New System.Drawing.Point(11, 159)
        Me.GBX_BOTTOM.Name = "GBX_BOTTOM"
        Me.GBX_BOTTOM.Size = New System.Drawing.Size(286, 69)
        Me.GBX_BOTTOM.TabIndex = 11
        Me.GBX_BOTTOM.TabStop = False
        Me.GBX_BOTTOM.Text = "지하 1단"
        '
        'RBN_BOTTOM_RV
        '
        Me.RBN_BOTTOM_RV.AutoSize = True
        Me.RBN_BOTTOM_RV.Location = New System.Drawing.Point(176, 31)
        Me.RBN_BOTTOM_RV.Name = "RBN_BOTTOM_RV"
        Me.RBN_BOTTOM_RV.Size = New System.Drawing.Size(49, 21)
        Me.RBN_BOTTOM_RV.TabIndex = 16
        Me.RBN_BOTTOM_RV.Text = "RV"
        Me.RBN_BOTTOM_RV.UseVisualStyleBackColor = True
        '
        'RBN_BOTTOM_SD
        '
        Me.RBN_BOTTOM_SD.AutoSize = True
        Me.RBN_BOTTOM_SD.Checked = True
        Me.RBN_BOTTOM_SD.Location = New System.Drawing.Point(60, 31)
        Me.RBN_BOTTOM_SD.Name = "RBN_BOTTOM_SD"
        Me.RBN_BOTTOM_SD.Size = New System.Drawing.Size(80, 21)
        Me.RBN_BOTTOM_SD.TabIndex = 15
        Me.RBN_BOTTOM_SD.TabStop = True
        Me.RBN_BOTTOM_SD.Text = "SEDAN"
        Me.RBN_BOTTOM_SD.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(222, 104)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(25, 17)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "대"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(222, 58)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(25, 17)
        Me.Label33.TabIndex = 10
        Me.Label33.Text = "대"
        Me.Label33.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'TXT_BOTTOM_RV
        '
        Me.TXT_BOTTOM_RV.Location = New System.Drawing.Point(116, 101)
        Me.TXT_BOTTOM_RV.Name = "TXT_BOTTOM_RV"
        Me.TXT_BOTTOM_RV.Size = New System.Drawing.Size(100, 27)
        Me.TXT_BOTTOM_RV.TabIndex = 8
        Me.TXT_BOTTOM_RV.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TXT_BOTTOM_SD
        '
        Me.TXT_BOTTOM_SD.Location = New System.Drawing.Point(116, 55)
        Me.TXT_BOTTOM_SD.Name = "TXT_BOTTOM_SD"
        Me.TXT_BOTTOM_SD.Size = New System.Drawing.Size(100, 27)
        Me.TXT_BOTTOM_SD.TabIndex = 5
        Me.TXT_BOTTOM_SD.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Location = New System.Drawing.Point(72, 104)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(38, 17)
        Me.Label52.TabIndex = 6
        Me.Label52.Text = "RV :"
        Me.Label52.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Location = New System.Drawing.Point(39, 61)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(69, 17)
        Me.Label53.TabIndex = 7
        Me.Label53.Text = "SEDAN :"
        Me.Label53.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'GroupBox14
        '
        Me.GroupBox14.Controls.Add(Me.BTN_TEMPLATE_OPEN)
        Me.GroupBox14.Controls.Add(Me.BTN_BASIC_INFO)
        Me.GroupBox14.Controls.Add(Me.BTN_CALCULATION)
        Me.GroupBox14.Location = New System.Drawing.Point(6, 578)
        Me.GroupBox14.Name = "GroupBox14"
        Me.GroupBox14.Size = New System.Drawing.Size(713, 67)
        Me.GroupBox14.TabIndex = 2
        Me.GroupBox14.TabStop = False
        '
        'BTN_TEMPLATE_OPEN
        '
        Me.BTN_TEMPLATE_OPEN.Location = New System.Drawing.Point(423, 19)
        Me.BTN_TEMPLATE_OPEN.Name = "BTN_TEMPLATE_OPEN"
        Me.BTN_TEMPLATE_OPEN.Size = New System.Drawing.Size(137, 37)
        Me.BTN_TEMPLATE_OPEN.TabIndex = 10
        Me.BTN_TEMPLATE_OPEN.Text = "템플릿 열기"
        Me.BTN_TEMPLATE_OPEN.UseVisualStyleBackColor = True
        '
        'BTN_BASIC_INFO
        '
        Me.BTN_BASIC_INFO.Font = New System.Drawing.Font("굴림", 10.0!)
        Me.BTN_BASIC_INFO.Location = New System.Drawing.Point(280, 19)
        Me.BTN_BASIC_INFO.Name = "BTN_BASIC_INFO"
        Me.BTN_BASIC_INFO.Size = New System.Drawing.Size(137, 37)
        Me.BTN_BASIC_INFO.TabIndex = 9
        Me.BTN_BASIC_INFO.Text = "기본정보 갱신"
        Me.BTN_BASIC_INFO.UseVisualStyleBackColor = True
        '
        'BTN_CALCULATION
        '
        Me.BTN_CALCULATION.Font = New System.Drawing.Font("굴림", 10.0!)
        Me.BTN_CALCULATION.Location = New System.Drawing.Point(566, 19)
        Me.BTN_CALCULATION.Name = "BTN_CALCULATION"
        Me.BTN_CALCULATION.Size = New System.Drawing.Size(137, 37)
        Me.BTN_CALCULATION.TabIndex = 6
        Me.BTN_CALCULATION.Text = "높이 계산"
        Me.BTN_CALCULATION.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.GroupBox8)
        Me.TabPage2.Controls.Add(Me.GBX_SHEAVE_2)
        Me.TabPage2.Controls.Add(Me.GBX_SHEAVE_1)
        Me.TabPage2.Controls.Add(Me.GroupBox5)
        Me.TabPage2.Controls.Add(Me.GroupBox11)
        Me.TabPage2.Controls.Add(Me.GroupBox10)
        Me.TabPage2.Controls.Add(Me.GroupBox9)
        Me.TabPage2.Controls.Add(Me.GroupBox3)
        Me.TabPage2.Location = New System.Drawing.Point(4, 27)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(732, 651)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "기계 장치"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.CMB_LIFT_5)
        Me.GroupBox8.Controls.Add(Me.CMB_LIFT_4)
        Me.GroupBox8.Controls.Add(Me.TXB_LIFT_Z)
        Me.GroupBox8.Controls.Add(Me.TXB_LIFT_Y)
        Me.GroupBox8.Controls.Add(Me.TXB_LIFT_X)
        Me.GroupBox8.Controls.Add(Me.CMB_LIFT_3)
        Me.GroupBox8.Controls.Add(Me.CMB_LIFT_2)
        Me.GroupBox8.Controls.Add(Me.CMB_LIFT_1)
        Me.GroupBox8.Controls.Add(Me.Label17)
        Me.GroupBox8.Controls.Add(Me.Label18)
        Me.GroupBox8.Controls.Add(Me.Label19)
        Me.GroupBox8.Controls.Add(Me.Label20)
        Me.GroupBox8.Location = New System.Drawing.Point(7, 151)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(713, 129)
        Me.GroupBox8.TabIndex = 19
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "LIFT CAGE"
        '
        'CMB_LIFT_5
        '
        Me.CMB_LIFT_5.Font = New System.Drawing.Font("굴림", 10.0!)
        Me.CMB_LIFT_5.FormattingEnabled = True
        Me.CMB_LIFT_5.Location = New System.Drawing.Point(67, 88)
        Me.CMB_LIFT_5.Name = "CMB_LIFT_5"
        Me.CMB_LIFT_5.Size = New System.Drawing.Size(143, 25)
        Me.CMB_LIFT_5.TabIndex = 3
        '
        'CMB_LIFT_4
        '
        Me.CMB_LIFT_4.Font = New System.Drawing.Font("굴림", 10.0!)
        Me.CMB_LIFT_4.FormattingEnabled = True
        Me.CMB_LIFT_4.Location = New System.Drawing.Point(218, 57)
        Me.CMB_LIFT_4.Name = "CMB_LIFT_4"
        Me.CMB_LIFT_4.Size = New System.Drawing.Size(143, 25)
        Me.CMB_LIFT_4.TabIndex = 4
        '
        'TXB_LIFT_Z
        '
        Me.TXB_LIFT_Z.Font = New System.Drawing.Font("굴림", 10.0!)
        Me.TXB_LIFT_Z.Location = New System.Drawing.Point(633, 57)
        Me.TXB_LIFT_Z.Name = "TXB_LIFT_Z"
        Me.TXB_LIFT_Z.Size = New System.Drawing.Size(65, 27)
        Me.TXB_LIFT_Z.TabIndex = 2
        Me.TXB_LIFT_Z.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TXB_LIFT_Y
        '
        Me.TXB_LIFT_Y.Enabled = False
        Me.TXB_LIFT_Y.Font = New System.Drawing.Font("굴림", 10.0!)
        Me.TXB_LIFT_Y.Location = New System.Drawing.Point(528, 57)
        Me.TXB_LIFT_Y.Name = "TXB_LIFT_Y"
        Me.TXB_LIFT_Y.Size = New System.Drawing.Size(65, 27)
        Me.TXB_LIFT_Y.TabIndex = 2
        Me.TXB_LIFT_Y.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TXB_LIFT_X
        '
        Me.TXB_LIFT_X.Enabled = False
        Me.TXB_LIFT_X.Font = New System.Drawing.Font("굴림", 10.0!)
        Me.TXB_LIFT_X.Location = New System.Drawing.Point(423, 57)
        Me.TXB_LIFT_X.Name = "TXB_LIFT_X"
        Me.TXB_LIFT_X.Size = New System.Drawing.Size(65, 27)
        Me.TXB_LIFT_X.TabIndex = 2
        Me.TXB_LIFT_X.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'CMB_LIFT_3
        '
        Me.CMB_LIFT_3.Font = New System.Drawing.Font("굴림", 10.0!)
        Me.CMB_LIFT_3.FormattingEnabled = True
        Me.CMB_LIFT_3.Location = New System.Drawing.Point(67, 57)
        Me.CMB_LIFT_3.Name = "CMB_LIFT_3"
        Me.CMB_LIFT_3.Size = New System.Drawing.Size(143, 25)
        Me.CMB_LIFT_3.TabIndex = 1
        '
        'CMB_LIFT_2
        '
        Me.CMB_LIFT_2.Font = New System.Drawing.Font("굴림", 10.0!)
        Me.CMB_LIFT_2.FormattingEnabled = True
        Me.CMB_LIFT_2.Location = New System.Drawing.Point(218, 26)
        Me.CMB_LIFT_2.Name = "CMB_LIFT_2"
        Me.CMB_LIFT_2.Size = New System.Drawing.Size(143, 25)
        Me.CMB_LIFT_2.TabIndex = 1
        '
        'CMB_LIFT_1
        '
        Me.CMB_LIFT_1.Font = New System.Drawing.Font("굴림", 10.0!)
        Me.CMB_LIFT_1.FormattingEnabled = True
        Me.CMB_LIFT_1.Location = New System.Drawing.Point(67, 26)
        Me.CMB_LIFT_1.Name = "CMB_LIFT_1"
        Me.CMB_LIFT_1.Size = New System.Drawing.Size(143, 25)
        Me.CMB_LIFT_1.TabIndex = 1
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(9, 29)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(52, 17)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "옵션 :"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(599, 60)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(28, 17)
        Me.Label18.TabIndex = 0
        Me.Label18.Text = "Z :"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(494, 60)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(28, 17)
        Me.Label19.TabIndex = 0
        Me.Label19.Text = "Y :"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(388, 60)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(29, 17)
        Me.Label20.TabIndex = 0
        Me.Label20.Text = "X :"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'GBX_SHEAVE_2
        '
        Me.GBX_SHEAVE_2.Controls.Add(Me.TXB_IDLE2_Z)
        Me.GBX_SHEAVE_2.Controls.Add(Me.TXB_IDLE2_Y)
        Me.GBX_SHEAVE_2.Controls.Add(Me.TXB_IDLE2_X)
        Me.GBX_SHEAVE_2.Controls.Add(Me.Label14)
        Me.GBX_SHEAVE_2.Controls.Add(Me.Label15)
        Me.GBX_SHEAVE_2.Controls.Add(Me.Label16)
        Me.GBX_SHEAVE_2.Controls.Add(Me.CMB_IDLE_2)
        Me.GBX_SHEAVE_2.Controls.Add(Me.Label13)
        Me.GBX_SHEAVE_2.Location = New System.Drawing.Point(6, 506)
        Me.GBX_SHEAVE_2.Name = "GBX_SHEAVE_2"
        Me.GBX_SHEAVE_2.Size = New System.Drawing.Size(713, 67)
        Me.GBX_SHEAVE_2.TabIndex = 16
        Me.GBX_SHEAVE_2.TabStop = False
        Me.GBX_SHEAVE_2.Text = "IDLE SHEAVE 2"
        Me.GBX_SHEAVE_2.Visible = False
        '
        'TXB_IDLE2_Z
        '
        Me.TXB_IDLE2_Z.Enabled = False
        Me.TXB_IDLE2_Z.Location = New System.Drawing.Point(633, 26)
        Me.TXB_IDLE2_Z.Name = "TXB_IDLE2_Z"
        Me.TXB_IDLE2_Z.Size = New System.Drawing.Size(65, 27)
        Me.TXB_IDLE2_Z.TabIndex = 6
        Me.TXB_IDLE2_Z.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TXB_IDLE2_Y
        '
        Me.TXB_IDLE2_Y.Location = New System.Drawing.Point(528, 26)
        Me.TXB_IDLE2_Y.Name = "TXB_IDLE2_Y"
        Me.TXB_IDLE2_Y.Size = New System.Drawing.Size(65, 27)
        Me.TXB_IDLE2_Y.TabIndex = 7
        Me.TXB_IDLE2_Y.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TXB_IDLE2_X
        '
        Me.TXB_IDLE2_X.Enabled = False
        Me.TXB_IDLE2_X.Location = New System.Drawing.Point(423, 26)
        Me.TXB_IDLE2_X.Name = "TXB_IDLE2_X"
        Me.TXB_IDLE2_X.Size = New System.Drawing.Size(65, 27)
        Me.TXB_IDLE2_X.TabIndex = 8
        Me.TXB_IDLE2_X.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(599, 29)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(28, 17)
        Me.Label14.TabIndex = 3
        Me.Label14.Text = "Z :"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(494, 29)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(28, 17)
        Me.Label15.TabIndex = 4
        Me.Label15.Text = "Y :"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(388, 29)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(29, 17)
        Me.Label16.TabIndex = 5
        Me.Label16.Text = "X :"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'CMB_IDLE_2
        '
        Me.CMB_IDLE_2.FormattingEnabled = True
        Me.CMB_IDLE_2.Location = New System.Drawing.Point(67, 26)
        Me.CMB_IDLE_2.Name = "CMB_IDLE_2"
        Me.CMB_IDLE_2.Size = New System.Drawing.Size(294, 25)
        Me.CMB_IDLE_2.TabIndex = 1
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(9, 29)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(52, 17)
        Me.Label13.TabIndex = 0
        Me.Label13.Text = "옵션 :"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'GBX_SHEAVE_1
        '
        Me.GBX_SHEAVE_1.Controls.Add(Me.TXB_IDLE1_Z)
        Me.GBX_SHEAVE_1.Controls.Add(Me.TXB_IDLE1_Y)
        Me.GBX_SHEAVE_1.Controls.Add(Me.TXB_IDLE1_X)
        Me.GBX_SHEAVE_1.Controls.Add(Me.CMB_IDLE_1)
        Me.GBX_SHEAVE_1.Controls.Add(Me.Label9)
        Me.GBX_SHEAVE_1.Controls.Add(Me.Label10)
        Me.GBX_SHEAVE_1.Controls.Add(Me.Label11)
        Me.GBX_SHEAVE_1.Controls.Add(Me.Label12)
        Me.GBX_SHEAVE_1.Location = New System.Drawing.Point(6, 433)
        Me.GBX_SHEAVE_1.Name = "GBX_SHEAVE_1"
        Me.GBX_SHEAVE_1.Size = New System.Drawing.Size(713, 67)
        Me.GBX_SHEAVE_1.TabIndex = 17
        Me.GBX_SHEAVE_1.TabStop = False
        Me.GBX_SHEAVE_1.Text = "IDLE SHEAVE 1"
        Me.GBX_SHEAVE_1.Visible = False
        '
        'TXB_IDLE1_Z
        '
        Me.TXB_IDLE1_Z.Enabled = False
        Me.TXB_IDLE1_Z.Location = New System.Drawing.Point(633, 26)
        Me.TXB_IDLE1_Z.Name = "TXB_IDLE1_Z"
        Me.TXB_IDLE1_Z.Size = New System.Drawing.Size(65, 27)
        Me.TXB_IDLE1_Z.TabIndex = 2
        Me.TXB_IDLE1_Z.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TXB_IDLE1_Y
        '
        Me.TXB_IDLE1_Y.Location = New System.Drawing.Point(528, 26)
        Me.TXB_IDLE1_Y.Name = "TXB_IDLE1_Y"
        Me.TXB_IDLE1_Y.Size = New System.Drawing.Size(65, 27)
        Me.TXB_IDLE1_Y.TabIndex = 2
        Me.TXB_IDLE1_Y.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TXB_IDLE1_X
        '
        Me.TXB_IDLE1_X.Enabled = False
        Me.TXB_IDLE1_X.Location = New System.Drawing.Point(423, 26)
        Me.TXB_IDLE1_X.Name = "TXB_IDLE1_X"
        Me.TXB_IDLE1_X.Size = New System.Drawing.Size(65, 27)
        Me.TXB_IDLE1_X.TabIndex = 2
        Me.TXB_IDLE1_X.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'CMB_IDLE_1
        '
        Me.CMB_IDLE_1.FormattingEnabled = True
        Me.CMB_IDLE_1.Location = New System.Drawing.Point(67, 26)
        Me.CMB_IDLE_1.Name = "CMB_IDLE_1"
        Me.CMB_IDLE_1.Size = New System.Drawing.Size(294, 25)
        Me.CMB_IDLE_1.TabIndex = 1
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(9, 29)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(52, 17)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "옵션 :"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(599, 29)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(28, 17)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Z :"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(494, 29)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(28, 17)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "Y :"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(388, 29)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(29, 17)
        Me.Label12.TabIndex = 0
        Me.Label12.Text = "X :"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.Label4)
        Me.GroupBox5.Controls.Add(Me.TXB_TRAC_Z)
        Me.GroupBox5.Controls.Add(Me.TXB_TRAC_Y)
        Me.GroupBox5.Controls.Add(Me.TXB_TRAC_X)
        Me.GroupBox5.Controls.Add(Me.CMB_TRAC_2)
        Me.GroupBox5.Controls.Add(Me.CMB_TRAC_1)
        Me.GroupBox5.Controls.Add(Me.Label8)
        Me.GroupBox5.Controls.Add(Me.Label7)
        Me.GroupBox5.Controls.Add(Me.Label6)
        Me.GroupBox5.Location = New System.Drawing.Point(7, 78)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(713, 67)
        Me.GroupBox5.TabIndex = 18
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "MAIN TRACTION"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(9, 29)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 17)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "옵션 :"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'TXB_TRAC_Z
        '
        Me.TXB_TRAC_Z.Enabled = False
        Me.TXB_TRAC_Z.Location = New System.Drawing.Point(633, 26)
        Me.TXB_TRAC_Z.Name = "TXB_TRAC_Z"
        Me.TXB_TRAC_Z.Size = New System.Drawing.Size(65, 27)
        Me.TXB_TRAC_Z.TabIndex = 2
        Me.TXB_TRAC_Z.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TXB_TRAC_Y
        '
        Me.TXB_TRAC_Y.Location = New System.Drawing.Point(528, 26)
        Me.TXB_TRAC_Y.Name = "TXB_TRAC_Y"
        Me.TXB_TRAC_Y.Size = New System.Drawing.Size(65, 27)
        Me.TXB_TRAC_Y.TabIndex = 2
        Me.TXB_TRAC_Y.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TXB_TRAC_X
        '
        Me.TXB_TRAC_X.Enabled = False
        Me.TXB_TRAC_X.Location = New System.Drawing.Point(423, 26)
        Me.TXB_TRAC_X.Name = "TXB_TRAC_X"
        Me.TXB_TRAC_X.Size = New System.Drawing.Size(65, 27)
        Me.TXB_TRAC_X.TabIndex = 2
        Me.TXB_TRAC_X.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'CMB_TRAC_2
        '
        Me.CMB_TRAC_2.FormattingEnabled = True
        Me.CMB_TRAC_2.Location = New System.Drawing.Point(218, 28)
        Me.CMB_TRAC_2.Name = "CMB_TRAC_2"
        Me.CMB_TRAC_2.Size = New System.Drawing.Size(143, 25)
        Me.CMB_TRAC_2.TabIndex = 1
        '
        'CMB_TRAC_1
        '
        Me.CMB_TRAC_1.FormattingEnabled = True
        Me.CMB_TRAC_1.Location = New System.Drawing.Point(67, 28)
        Me.CMB_TRAC_1.Name = "CMB_TRAC_1"
        Me.CMB_TRAC_1.Size = New System.Drawing.Size(143, 25)
        Me.CMB_TRAC_1.TabIndex = 1
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(599, 29)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(28, 17)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Z :"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(494, 29)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(28, 17)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Y :"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(388, 29)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(29, 17)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "X :"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'GroupBox11
        '
        Me.GroupBox11.Controls.Add(Me.TXB_TURN_Z)
        Me.GroupBox11.Controls.Add(Me.TXB_TURN_Y)
        Me.GroupBox11.Controls.Add(Me.TXB_TURN_X)
        Me.GroupBox11.Controls.Add(Me.CMB_TURN_2)
        Me.GroupBox11.Controls.Add(Me.CMB_TURN_1)
        Me.GroupBox11.Controls.Add(Me.Label29)
        Me.GroupBox11.Controls.Add(Me.Label30)
        Me.GroupBox11.Controls.Add(Me.Label31)
        Me.GroupBox11.Controls.Add(Me.Label32)
        Me.GroupBox11.Location = New System.Drawing.Point(7, 359)
        Me.GroupBox11.Name = "GroupBox11"
        Me.GroupBox11.Size = New System.Drawing.Size(713, 67)
        Me.GroupBox11.TabIndex = 0
        Me.GroupBox11.TabStop = False
        Me.GroupBox11.Text = "TURN TABLE"
        '
        'TXB_TURN_Z
        '
        Me.TXB_TURN_Z.Enabled = False
        Me.TXB_TURN_Z.Location = New System.Drawing.Point(633, 25)
        Me.TXB_TURN_Z.Name = "TXB_TURN_Z"
        Me.TXB_TURN_Z.Size = New System.Drawing.Size(65, 27)
        Me.TXB_TURN_Z.TabIndex = 2
        Me.TXB_TURN_Z.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TXB_TURN_Y
        '
        Me.TXB_TURN_Y.Enabled = False
        Me.TXB_TURN_Y.Location = New System.Drawing.Point(528, 25)
        Me.TXB_TURN_Y.Name = "TXB_TURN_Y"
        Me.TXB_TURN_Y.Size = New System.Drawing.Size(65, 27)
        Me.TXB_TURN_Y.TabIndex = 2
        Me.TXB_TURN_Y.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TXB_TURN_X
        '
        Me.TXB_TURN_X.Enabled = False
        Me.TXB_TURN_X.Location = New System.Drawing.Point(423, 25)
        Me.TXB_TURN_X.Name = "TXB_TURN_X"
        Me.TXB_TURN_X.Size = New System.Drawing.Size(65, 27)
        Me.TXB_TURN_X.TabIndex = 2
        Me.TXB_TURN_X.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'CMB_TURN_2
        '
        Me.CMB_TURN_2.FormattingEnabled = True
        Me.CMB_TURN_2.Location = New System.Drawing.Point(254, 25)
        Me.CMB_TURN_2.Name = "CMB_TURN_2"
        Me.CMB_TURN_2.Size = New System.Drawing.Size(107, 25)
        Me.CMB_TURN_2.TabIndex = 1
        '
        'CMB_TURN_1
        '
        Me.CMB_TURN_1.FormattingEnabled = True
        Me.CMB_TURN_1.Location = New System.Drawing.Point(67, 26)
        Me.CMB_TURN_1.Name = "CMB_TURN_1"
        Me.CMB_TURN_1.Size = New System.Drawing.Size(181, 25)
        Me.CMB_TURN_1.TabIndex = 1
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(9, 29)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(52, 17)
        Me.Label29.TabIndex = 0
        Me.Label29.Text = "옵션 :"
        Me.Label29.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(599, 28)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(28, 17)
        Me.Label30.TabIndex = 0
        Me.Label30.Text = "Z :"
        Me.Label30.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(494, 28)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(28, 17)
        Me.Label31.TabIndex = 0
        Me.Label31.Text = "Y :"
        Me.Label31.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(388, 28)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(29, 17)
        Me.Label32.TabIndex = 0
        Me.Label32.Text = "X :"
        Me.Label32.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'GroupBox10
        '
        Me.GroupBox10.Controls.Add(Me.TXB_WEIG_Z)
        Me.GroupBox10.Controls.Add(Me.TXB_WEIG_Y)
        Me.GroupBox10.Controls.Add(Me.TXB_WEIG_X)
        Me.GroupBox10.Controls.Add(Me.CMB_WEIG_2)
        Me.GroupBox10.Controls.Add(Me.CMB_WEIG_1)
        Me.GroupBox10.Controls.Add(Me.Label25)
        Me.GroupBox10.Controls.Add(Me.Label26)
        Me.GroupBox10.Controls.Add(Me.Label27)
        Me.GroupBox10.Controls.Add(Me.Label28)
        Me.GroupBox10.Location = New System.Drawing.Point(7, 286)
        Me.GroupBox10.Name = "GroupBox10"
        Me.GroupBox10.Size = New System.Drawing.Size(713, 67)
        Me.GroupBox10.TabIndex = 0
        Me.GroupBox10.TabStop = False
        Me.GroupBox10.Text = "COUNTER WEIGHT"
        '
        'TXB_WEIG_Z
        '
        Me.TXB_WEIG_Z.Location = New System.Drawing.Point(632, 26)
        Me.TXB_WEIG_Z.Name = "TXB_WEIG_Z"
        Me.TXB_WEIG_Z.Size = New System.Drawing.Size(65, 27)
        Me.TXB_WEIG_Z.TabIndex = 2
        Me.TXB_WEIG_Z.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TXB_WEIG_Y
        '
        Me.TXB_WEIG_Y.Enabled = False
        Me.TXB_WEIG_Y.Location = New System.Drawing.Point(527, 26)
        Me.TXB_WEIG_Y.Name = "TXB_WEIG_Y"
        Me.TXB_WEIG_Y.Size = New System.Drawing.Size(65, 27)
        Me.TXB_WEIG_Y.TabIndex = 2
        Me.TXB_WEIG_Y.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TXB_WEIG_X
        '
        Me.TXB_WEIG_X.Enabled = False
        Me.TXB_WEIG_X.Location = New System.Drawing.Point(422, 26)
        Me.TXB_WEIG_X.Name = "TXB_WEIG_X"
        Me.TXB_WEIG_X.Size = New System.Drawing.Size(65, 27)
        Me.TXB_WEIG_X.TabIndex = 2
        Me.TXB_WEIG_X.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CMB_WEIG_2
        '
        Me.CMB_WEIG_2.FormattingEnabled = True
        Me.CMB_WEIG_2.Location = New System.Drawing.Point(253, 26)
        Me.CMB_WEIG_2.Name = "CMB_WEIG_2"
        Me.CMB_WEIG_2.Size = New System.Drawing.Size(107, 25)
        Me.CMB_WEIG_2.TabIndex = 1
        '
        'CMB_WEIG_1
        '
        Me.CMB_WEIG_1.FormattingEnabled = True
        Me.CMB_WEIG_1.Location = New System.Drawing.Point(66, 26)
        Me.CMB_WEIG_1.Name = "CMB_WEIG_1"
        Me.CMB_WEIG_1.Size = New System.Drawing.Size(181, 25)
        Me.CMB_WEIG_1.TabIndex = 1
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(8, 29)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(52, 17)
        Me.Label25.TabIndex = 0
        Me.Label25.Text = "옵션 :"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(598, 29)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(28, 17)
        Me.Label26.TabIndex = 0
        Me.Label26.Text = "Z :"
        Me.Label26.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(493, 29)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(28, 17)
        Me.Label27.TabIndex = 0
        Me.Label27.Text = "Y :"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(387, 29)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(29, 17)
        Me.Label28.TabIndex = 0
        Me.Label28.Text = "X :"
        Me.Label28.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'GroupBox9
        '
        Me.GroupBox9.Controls.Add(Me.TXB_PALL_Z)
        Me.GroupBox9.Controls.Add(Me.TXB_PALL_Y)
        Me.GroupBox9.Controls.Add(Me.TXB_PALL_X)
        Me.GroupBox9.Controls.Add(Me.CMB_PALL)
        Me.GroupBox9.Controls.Add(Me.Label21)
        Me.GroupBox9.Controls.Add(Me.Label22)
        Me.GroupBox9.Controls.Add(Me.Label23)
        Me.GroupBox9.Controls.Add(Me.Label24)
        Me.GroupBox9.Location = New System.Drawing.Point(7, 5)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(713, 67)
        Me.GroupBox9.TabIndex = 0
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Text = "PALLET"
        '
        'TXB_PALL_Z
        '
        Me.TXB_PALL_Z.Location = New System.Drawing.Point(633, 26)
        Me.TXB_PALL_Z.Name = "TXB_PALL_Z"
        Me.TXB_PALL_Z.Size = New System.Drawing.Size(65, 27)
        Me.TXB_PALL_Z.TabIndex = 2
        Me.TXB_PALL_Z.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TXB_PALL_Y
        '
        Me.TXB_PALL_Y.Location = New System.Drawing.Point(528, 26)
        Me.TXB_PALL_Y.Name = "TXB_PALL_Y"
        Me.TXB_PALL_Y.Size = New System.Drawing.Size(65, 27)
        Me.TXB_PALL_Y.TabIndex = 2
        Me.TXB_PALL_Y.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TXB_PALL_X
        '
        Me.TXB_PALL_X.Location = New System.Drawing.Point(423, 24)
        Me.TXB_PALL_X.Name = "TXB_PALL_X"
        Me.TXB_PALL_X.Size = New System.Drawing.Size(65, 27)
        Me.TXB_PALL_X.TabIndex = 2
        Me.TXB_PALL_X.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'CMB_PALL
        '
        Me.CMB_PALL.FormattingEnabled = True
        Me.CMB_PALL.Location = New System.Drawing.Point(67, 26)
        Me.CMB_PALL.Name = "CMB_PALL"
        Me.CMB_PALL.Size = New System.Drawing.Size(294, 25)
        Me.CMB_PALL.TabIndex = 1
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(9, 29)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(52, 17)
        Me.Label21.TabIndex = 0
        Me.Label21.Text = "옵션 :"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(599, 29)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(28, 17)
        Me.Label22.TabIndex = 0
        Me.Label22.Text = "Z :"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(494, 29)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(28, 17)
        Me.Label23.TabIndex = 0
        Me.Label23.Text = "Y :"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(388, 27)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(29, 17)
        Me.Label24.TabIndex = 0
        Me.Label24.Text = "X :"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Button1)
        Me.GroupBox3.Controls.Add(Me.BTN_MACHINE_POSITION)
        Me.GroupBox3.Controls.Add(Me.BTN_MACHINE_CREATE)
        Me.GroupBox3.Location = New System.Drawing.Point(6, 579)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(713, 67)
        Me.GroupBox3.TabIndex = 0
        Me.GroupBox3.TabStop = False
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(280, 19)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(137, 37)
        Me.Button1.TabIndex = 12
        Me.Button1.Text = "테스트용"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'BTN_MACHINE_POSITION
        '
        Me.BTN_MACHINE_POSITION.Font = New System.Drawing.Font("굴림", 10.0!)
        Me.BTN_MACHINE_POSITION.Location = New System.Drawing.Point(566, 19)
        Me.BTN_MACHINE_POSITION.Name = "BTN_MACHINE_POSITION"
        Me.BTN_MACHINE_POSITION.Size = New System.Drawing.Size(137, 37)
        Me.BTN_MACHINE_POSITION.TabIndex = 11
        Me.BTN_MACHINE_POSITION.Text = "위치 수정"
        Me.BTN_MACHINE_POSITION.UseVisualStyleBackColor = True
        '
        'BTN_MACHINE_CREATE
        '
        Me.BTN_MACHINE_CREATE.Font = New System.Drawing.Font("굴림", 10.0!)
        Me.BTN_MACHINE_CREATE.Location = New System.Drawing.Point(423, 19)
        Me.BTN_MACHINE_CREATE.Name = "BTN_MACHINE_CREATE"
        Me.BTN_MACHINE_CREATE.Size = New System.Drawing.Size(137, 37)
        Me.BTN_MACHINE_CREATE.TabIndex = 11
        Me.BTN_MACHINE_CREATE.Text = "기계장치 생성"
        Me.BTN_MACHINE_CREATE.UseVisualStyleBackColor = True
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.GroupBox20)
        Me.TabPage3.Location = New System.Drawing.Point(4, 27)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(732, 651)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "SIMULATION & BOM"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'GroupBox20
        '
        Me.GroupBox20.Controls.Add(Me.GroupBox18)
        Me.GroupBox20.Controls.Add(Me.GroupBox15)
        Me.GroupBox20.Location = New System.Drawing.Point(7, 6)
        Me.GroupBox20.Name = "GroupBox20"
        Me.GroupBox20.Size = New System.Drawing.Size(716, 606)
        Me.GroupBox20.TabIndex = 27
        Me.GroupBox20.TabStop = False
        Me.GroupBox20.Text = "BOM"
        '
        'GroupBox18
        '
        Me.GroupBox18.Controls.Add(Me.BTN_BOM_CREATE)
        Me.GroupBox18.Controls.Add(Me.BTN_BOM_SELECTION)
        Me.GroupBox18.Controls.Add(Me.TXT_BOM_SELECTION)
        Me.GroupBox18.Location = New System.Drawing.Point(17, 144)
        Me.GroupBox18.Name = "GroupBox18"
        Me.GroupBox18.Size = New System.Drawing.Size(679, 211)
        Me.GroupBox18.TabIndex = 39
        Me.GroupBox18.TabStop = False
        '
        'BTN_BOM_CREATE
        '
        Me.BTN_BOM_CREATE.Font = New System.Drawing.Font("굴림", 10.0!)
        Me.BTN_BOM_CREATE.Location = New System.Drawing.Point(506, 125)
        Me.BTN_BOM_CREATE.Name = "BTN_BOM_CREATE"
        Me.BTN_BOM_CREATE.Size = New System.Drawing.Size(137, 37)
        Me.BTN_BOM_CREATE.TabIndex = 42
        Me.BTN_BOM_CREATE.Text = "BOM 생성"
        Me.BTN_BOM_CREATE.UseVisualStyleBackColor = True
        '
        'BTN_BOM_SELECTION
        '
        Me.BTN_BOM_SELECTION.Font = New System.Drawing.Font("굴림", 10.0!)
        Me.BTN_BOM_SELECTION.Location = New System.Drawing.Point(506, 56)
        Me.BTN_BOM_SELECTION.Name = "BTN_BOM_SELECTION"
        Me.BTN_BOM_SELECTION.Size = New System.Drawing.Size(137, 37)
        Me.BTN_BOM_SELECTION.TabIndex = 40
        Me.BTN_BOM_SELECTION.Text = "선택"
        Me.BTN_BOM_SELECTION.UseVisualStyleBackColor = True
        '
        'TXT_BOM_SELECTION
        '
        Me.TXT_BOM_SELECTION.Location = New System.Drawing.Point(31, 63)
        Me.TXT_BOM_SELECTION.Name = "TXT_BOM_SELECTION"
        Me.TXT_BOM_SELECTION.Size = New System.Drawing.Size(455, 27)
        Me.TXT_BOM_SELECTION.TabIndex = 39
        '
        'GroupBox15
        '
        Me.GroupBox15.Controls.Add(Me.RBN_BOM_OPT2)
        Me.GroupBox15.Controls.Add(Me.RBN_BOM_OPT1)
        Me.GroupBox15.Location = New System.Drawing.Point(17, 46)
        Me.GroupBox15.Name = "GroupBox15"
        Me.GroupBox15.Size = New System.Drawing.Size(679, 92)
        Me.GroupBox15.TabIndex = 39
        Me.GroupBox15.TabStop = False
        Me.GroupBox15.Text = "출력 대상"
        '
        'RBN_BOM_OPT2
        '
        Me.RBN_BOM_OPT2.AutoSize = True
        Me.RBN_BOM_OPT2.Location = New System.Drawing.Point(375, 39)
        Me.RBN_BOM_OPT2.Name = "RBN_BOM_OPT2"
        Me.RBN_BOM_OPT2.Size = New System.Drawing.Size(158, 21)
        Me.RBN_BOM_OPT2.TabIndex = 37
        Me.RBN_BOM_OPT2.Text = "선택된 PRODUCT"
        Me.RBN_BOM_OPT2.UseVisualStyleBackColor = True
        '
        'RBN_BOM_OPT1
        '
        Me.RBN_BOM_OPT1.AutoSize = True
        Me.RBN_BOM_OPT1.Checked = True
        Me.RBN_BOM_OPT1.Location = New System.Drawing.Point(141, 39)
        Me.RBN_BOM_OPT1.Name = "RBN_BOM_OPT1"
        Me.RBN_BOM_OPT1.Size = New System.Drawing.Size(141, 21)
        Me.RBN_BOM_OPT1.TabIndex = 36
        Me.RBN_BOM_OPT1.TabStop = True
        Me.RBN_BOM_OPT1.Text = "전체 PRODUCT"
        Me.RBN_BOM_OPT1.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Label2)
        Me.GroupBox4.Controls.Add(Me.Label1)
        Me.GroupBox4.Controls.Add(Me.PictureBox1)
        Me.GroupBox4.Location = New System.Drawing.Point(1, 754)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(738, 39)
        Me.GroupBox4.TabIndex = 9
        Me.GroupBox4.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("굴림", 7.0!)
        Me.Label2.Location = New System.Drawing.Point(16, 18)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(108, 12)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "CATIA Automation"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("굴림", 7.0!)
        Me.Label1.Location = New System.Drawing.Point(552, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(111, 12)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Copyrightⓒ ISPark"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(669, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(54, 23)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TabControl2)
        Me.GroupBox1.Location = New System.Drawing.Point(745, 1)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(381, 792)
        Me.GroupBox1.TabIndex = 10
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "관리자 메뉴"
        '
        'TabControl2
        '
        Me.TabControl2.Controls.Add(Me.TabPage4)
        Me.TabControl2.Controls.Add(Me.TabPage5)
        Me.TabControl2.Controls.Add(Me.TabPage6)
        Me.TabControl2.Location = New System.Drawing.Point(6, 26)
        Me.TabControl2.Name = "TabControl2"
        Me.TabControl2.SelectedIndex = 0
        Me.TabControl2.Size = New System.Drawing.Size(371, 757)
        Me.TabControl2.TabIndex = 11
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.GroupBox24)
        Me.TabPage4.Location = New System.Drawing.Point(4, 27)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(363, 726)
        Me.TabPage4.TabIndex = 0
        Me.TabPage4.Text = "타워 높이"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'GroupBox24
        '
        Me.GroupBox24.Controls.Add(Me.TXT_SKEL)
        Me.GroupBox24.Controls.Add(Me.TXT_ASSY)
        Me.GroupBox24.Controls.Add(Me.TXT_PALLET_VIRTUAL)
        Me.GroupBox24.Controls.Add(Me.TXT_CAR_RV)
        Me.GroupBox24.Controls.Add(Me.TXT_CAR_SD)
        Me.GroupBox24.Controls.Add(Me.TXT_FMIT)
        Me.GroupBox24.Controls.Add(Me.Label44)
        Me.GroupBox24.Controls.Add(Me.Label43)
        Me.GroupBox24.Controls.Add(Me.Label68)
        Me.GroupBox24.Controls.Add(Me.Label69)
        Me.GroupBox24.Controls.Add(Me.Label70)
        Me.GroupBox24.Controls.Add(Me.TXT_RESULT)
        Me.GroupBox24.Controls.Add(Me.TXT_PIT)
        Me.GroupBox24.Controls.Add(Me.TXT_GH)
        Me.GroupBox24.Controls.Add(Me.TXT_EH)
        Me.GroupBox24.Controls.Add(Me.TXT_BM)
        Me.GroupBox24.Controls.Add(Me.TXT_OH)
        Me.GroupBox24.Controls.Add(Me.Label34)
        Me.GroupBox24.Controls.Add(Me.Label49)
        Me.GroupBox24.Controls.Add(Me.Label41)
        Me.GroupBox24.Controls.Add(Me.Label5)
        Me.GroupBox24.Controls.Add(Me.BASEMENT)
        Me.GroupBox24.Controls.Add(Me.Label40)
        Me.GroupBox24.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox24.Name = "GroupBox24"
        Me.GroupBox24.Size = New System.Drawing.Size(350, 713)
        Me.GroupBox24.TabIndex = 13
        Me.GroupBox24.TabStop = False
        Me.GroupBox24.Text = "관리자 메뉴"
        '
        'TXT_SKEL
        '
        Me.TXT_SKEL.Location = New System.Drawing.Point(9, 576)
        Me.TXT_SKEL.Name = "TXT_SKEL"
        Me.TXT_SKEL.Size = New System.Drawing.Size(333, 27)
        Me.TXT_SKEL.TabIndex = 24
        Me.TXT_SKEL.Text = "#DET#SKELETON"
        '
        'TXT_ASSY
        '
        Me.TXT_ASSY.Location = New System.Drawing.Point(9, 526)
        Me.TXT_ASSY.Name = "TXT_ASSY"
        Me.TXT_ASSY.Size = New System.Drawing.Size(333, 27)
        Me.TXT_ASSY.TabIndex = 24
        Me.TXT_ASSY.Text = "DET#ASSY"
        '
        'TXT_PALLET_VIRTUAL
        '
        Me.TXT_PALLET_VIRTUAL.Location = New System.Drawing.Point(9, 476)
        Me.TXT_PALLET_VIRTUAL.Name = "TXT_PALLET_VIRTUAL"
        Me.TXT_PALLET_VIRTUAL.Size = New System.Drawing.Size(333, 27)
        Me.TXT_PALLET_VIRTUAL.TabIndex = 24
        Me.TXT_PALLET_VIRTUAL.Text = "#PALLET ASSY#VIRTUAL"
        '
        'TXT_CAR_RV
        '
        Me.TXT_CAR_RV.Location = New System.Drawing.Point(9, 426)
        Me.TXT_CAR_RV.Name = "TXT_CAR_RV"
        Me.TXT_CAR_RV.Size = New System.Drawing.Size(333, 27)
        Me.TXT_CAR_RV.TabIndex = 22
        Me.TXT_CAR_RV.Text = "#RV#KORANDO"
        '
        'TXT_CAR_SD
        '
        Me.TXT_CAR_SD.Location = New System.Drawing.Point(9, 393)
        Me.TXT_CAR_SD.Name = "TXT_CAR_SD"
        Me.TXT_CAR_SD.Size = New System.Drawing.Size(333, 27)
        Me.TXT_CAR_SD.TabIndex = 23
        Me.TXT_CAR_SD.Text = "#SEDAN#EQUUS"
        '
        'TXT_FMIT
        '
        Me.TXT_FMIT.Location = New System.Drawing.Point(9, 343)
        Me.TXT_FMIT.Name = "TXT_FMIT"
        Me.TXT_FMIT.Size = New System.Drawing.Size(333, 27)
        Me.TXT_FMIT.TabIndex = 21
        Me.TXT_FMIT.Text = "DET-FMIT-L44"
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(6, 556)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(136, 17)
        Me.Label44.TabIndex = 18
        Me.Label44.Text = "- DET SKELETON"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Location = New System.Drawing.Point(6, 506)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(95, 17)
        Me.Label43.TabIndex = 18
        Me.Label43.Text = "- DET ASSY"
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.Location = New System.Drawing.Point(6, 456)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(149, 17)
        Me.Label68.TabIndex = 18
        Me.Label68.Text = "- PALLET_VIRTUAL"
        '
        'Label69
        '
        Me.Label69.AutoSize = True
        Me.Label69.Location = New System.Drawing.Point(6, 373)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(55, 17)
        Me.Label69.TabIndex = 19
        Me.Label69.Text = "- 차종"
        '
        'Label70
        '
        Me.Label70.AutoSize = True
        Me.Label70.Location = New System.Drawing.Point(6, 323)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(55, 17)
        Me.Label70.TabIndex = 20
        Me.Label70.Text = "- 철골"
        '
        'TXT_RESULT
        '
        Me.TXT_RESULT.Location = New System.Drawing.Point(9, 293)
        Me.TXT_RESULT.Name = "TXT_RESULT"
        Me.TXT_RESULT.Size = New System.Drawing.Size(333, 27)
        Me.TXT_RESULT.TabIndex = 14
        '
        'TXT_PIT
        '
        Me.TXT_PIT.Location = New System.Drawing.Point(9, 43)
        Me.TXT_PIT.Name = "TXT_PIT"
        Me.TXT_PIT.Size = New System.Drawing.Size(333, 27)
        Me.TXT_PIT.TabIndex = 14
        '
        'TXT_GH
        '
        Me.TXT_GH.Location = New System.Drawing.Point(9, 193)
        Me.TXT_GH.Name = "TXT_GH"
        Me.TXT_GH.Size = New System.Drawing.Size(333, 27)
        Me.TXT_GH.TabIndex = 14
        '
        'TXT_EH
        '
        Me.TXT_EH.Location = New System.Drawing.Point(9, 143)
        Me.TXT_EH.Name = "TXT_EH"
        Me.TXT_EH.Size = New System.Drawing.Size(333, 27)
        Me.TXT_EH.TabIndex = 14
        '
        'TXT_BM
        '
        Me.TXT_BM.Location = New System.Drawing.Point(9, 93)
        Me.TXT_BM.Name = "TXT_BM"
        Me.TXT_BM.Size = New System.Drawing.Size(333, 27)
        Me.TXT_BM.TabIndex = 14
        '
        'TXT_OH
        '
        Me.TXT_OH.Location = New System.Drawing.Point(9, 243)
        Me.TXT_OH.Name = "TXT_OH"
        Me.TXT_OH.Size = New System.Drawing.Size(333, 27)
        Me.TXT_OH.TabIndex = 14
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(6, 273)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(94, 17)
        Me.Label34.TabIndex = 12
        Me.Label34.Text = "- 전체 높이"
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Location = New System.Drawing.Point(6, 23)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(43, 17)
        Me.Label49.TabIndex = 12
        Me.Label49.Text = "- PIT"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(6, 173)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(97, 17)
        Me.Label41.TabIndex = 12
        Me.Label41.Text = "- 격납층-상"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 123)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(41, 17)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "- EH"
        '
        'BASEMENT
        '
        Me.BASEMENT.AutoSize = True
        Me.BASEMENT.Location = New System.Drawing.Point(6, 73)
        Me.BASEMENT.Name = "BASEMENT"
        Me.BASEMENT.Size = New System.Drawing.Size(104, 17)
        Me.BASEMENT.TabIndex = 12
        Me.BASEMENT.Text = "- BASEMENT"
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(6, 223)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(43, 17)
        Me.Label40.TabIndex = 12
        Me.Label40.Text = "- OH"
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.GroupBox12)
        Me.TabPage5.Location = New System.Drawing.Point(4, 27)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage5.Size = New System.Drawing.Size(363, 726)
        Me.TabPage5.TabIndex = 1
        Me.TabPage5.Text = "기계 장치"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'GroupBox12
        '
        Me.GroupBox12.Controls.Add(Me.LBX_Combobox_Sort)
        Me.GroupBox12.Controls.Add(Me.TXT_PIT_S)
        Me.GroupBox12.Controls.Add(Me.TXT_CAGE_S)
        Me.GroupBox12.Controls.Add(Me.TXT_ENTER_S)
        Me.GroupBox12.Controls.Add(Me.TXT_IDE2)
        Me.GroupBox12.Controls.Add(Me.TXT_IDE1)
        Me.GroupBox12.Controls.Add(Me.TXT_WEIG)
        Me.GroupBox12.Controls.Add(Me.TXT_TRAC)
        Me.GroupBox12.Controls.Add(Me.TXT_TURN)
        Me.GroupBox12.Controls.Add(Me.TXT_PALL)
        Me.GroupBox12.Controls.Add(Me.TXT_LIFT)
        Me.GroupBox12.Controls.Add(Me.Label55)
        Me.GroupBox12.Controls.Add(Me.Label66)
        Me.GroupBox12.Controls.Add(Me.Label65)
        Me.GroupBox12.Controls.Add(Me.Label64)
        Me.GroupBox12.Controls.Add(Me.Label61)
        Me.GroupBox12.Controls.Add(Me.Label54)
        Me.GroupBox12.Controls.Add(Me.Label56)
        Me.GroupBox12.Controls.Add(Me.Label57)
        Me.GroupBox12.Controls.Add(Me.Label58)
        Me.GroupBox12.Controls.Add(Me.Label59)
        Me.GroupBox12.Controls.Add(Me.Label60)
        Me.GroupBox12.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox12.Name = "GroupBox12"
        Me.GroupBox12.Size = New System.Drawing.Size(350, 713)
        Me.GroupBox12.TabIndex = 14
        Me.GroupBox12.TabStop = False
        Me.GroupBox12.Text = "관리자 메뉴"
        '
        'LBX_Combobox_Sort
        '
        Me.LBX_Combobox_Sort.FormattingEnabled = True
        Me.LBX_Combobox_Sort.ItemHeight = 17
        Me.LBX_Combobox_Sort.Items.AddRange(New Object() {"삭제 금지"})
        Me.LBX_Combobox_Sort.Location = New System.Drawing.Point(9, 553)
        Me.LBX_Combobox_Sort.Name = "LBX_Combobox_Sort"
        Me.LBX_Combobox_Sort.Size = New System.Drawing.Size(333, 123)
        Me.LBX_Combobox_Sort.TabIndex = 17
        '
        'TXT_PIT_S
        '
        Me.TXT_PIT_S.Location = New System.Drawing.Point(9, 493)
        Me.TXT_PIT_S.Name = "TXT_PIT_S"
        Me.TXT_PIT_S.Size = New System.Drawing.Size(333, 27)
        Me.TXT_PIT_S.TabIndex = 21
        '
        'TXT_CAGE_S
        '
        Me.TXT_CAGE_S.Location = New System.Drawing.Point(9, 443)
        Me.TXT_CAGE_S.Name = "TXT_CAGE_S"
        Me.TXT_CAGE_S.Size = New System.Drawing.Size(333, 27)
        Me.TXT_CAGE_S.TabIndex = 21
        '
        'TXT_ENTER_S
        '
        Me.TXT_ENTER_S.Location = New System.Drawing.Point(9, 393)
        Me.TXT_ENTER_S.Name = "TXT_ENTER_S"
        Me.TXT_ENTER_S.Size = New System.Drawing.Size(333, 27)
        Me.TXT_ENTER_S.TabIndex = 21
        '
        'TXT_IDE2
        '
        Me.TXT_IDE2.Location = New System.Drawing.Point(9, 343)
        Me.TXT_IDE2.Name = "TXT_IDE2"
        Me.TXT_IDE2.Size = New System.Drawing.Size(333, 27)
        Me.TXT_IDE2.TabIndex = 21
        '
        'TXT_IDE1
        '
        Me.TXT_IDE1.Location = New System.Drawing.Point(9, 293)
        Me.TXT_IDE1.Name = "TXT_IDE1"
        Me.TXT_IDE1.Size = New System.Drawing.Size(333, 27)
        Me.TXT_IDE1.TabIndex = 20
        '
        'TXT_WEIG
        '
        Me.TXT_WEIG.Location = New System.Drawing.Point(9, 193)
        Me.TXT_WEIG.Name = "TXT_WEIG"
        Me.TXT_WEIG.Size = New System.Drawing.Size(333, 27)
        Me.TXT_WEIG.TabIndex = 18
        '
        'TXT_TRAC
        '
        Me.TXT_TRAC.Location = New System.Drawing.Point(9, 243)
        Me.TXT_TRAC.Name = "TXT_TRAC"
        Me.TXT_TRAC.Size = New System.Drawing.Size(333, 27)
        Me.TXT_TRAC.TabIndex = 19
        '
        'TXT_TURN
        '
        Me.TXT_TURN.Location = New System.Drawing.Point(9, 143)
        Me.TXT_TURN.Name = "TXT_TURN"
        Me.TXT_TURN.Size = New System.Drawing.Size(333, 27)
        Me.TXT_TURN.TabIndex = 17
        '
        'TXT_PALL
        '
        Me.TXT_PALL.Location = New System.Drawing.Point(9, 93)
        Me.TXT_PALL.Name = "TXT_PALL"
        Me.TXT_PALL.Size = New System.Drawing.Size(333, 27)
        Me.TXT_PALL.TabIndex = 16
        '
        'TXT_LIFT
        '
        Me.TXT_LIFT.Location = New System.Drawing.Point(9, 43)
        Me.TXT_LIFT.Name = "TXT_LIFT"
        Me.TXT_LIFT.Size = New System.Drawing.Size(333, 27)
        Me.TXT_LIFT.TabIndex = 15
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Location = New System.Drawing.Point(6, 533)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(145, 17)
        Me.Label55.TabIndex = 12
        Me.Label55.Text = "- 콤보박스 정렬용"
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.Location = New System.Drawing.Point(6, 473)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(96, 17)
        Me.Label66.TabIndex = 12
        Me.Label66.Text = "- Pit Sensor"
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.Location = New System.Drawing.Point(6, 423)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(116, 17)
        Me.Label65.TabIndex = 12
        Me.Label65.Text = "- Cage Sensor"
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Location = New System.Drawing.Point(6, 373)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(152, 17)
        Me.Label64.TabIndex = 12
        Me.Label64.Text = "- Enterance Sensor"
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.Location = New System.Drawing.Point(6, 323)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(117, 17)
        Me.Label61.TabIndex = 12
        Me.Label61.Text = "- Idle Sheave 2"
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Location = New System.Drawing.Point(6, 273)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(117, 17)
        Me.Label54.TabIndex = 12
        Me.Label54.Text = "- Idle Sheave 1"
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Location = New System.Drawing.Point(6, 173)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(119, 17)
        Me.Label56.TabIndex = 12
        Me.Label56.Text = "- Count Weight"
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Location = New System.Drawing.Point(6, 223)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(119, 17)
        Me.Label57.TabIndex = 12
        Me.Label57.Text = "- Main Traction"
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Location = New System.Drawing.Point(6, 123)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(97, 17)
        Me.Label58.TabIndex = 12
        Me.Label58.Text = "- Turn Table"
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Location = New System.Drawing.Point(6, 73)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(60, 17)
        Me.Label59.TabIndex = 12
        Me.Label59.Text = "- Pallet"
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Location = New System.Drawing.Point(6, 23)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(86, 17)
        Me.Label60.TabIndex = 13
        Me.Label60.Text = "- Lift Cage"
        '
        'TabPage6
        '
        Me.TabPage6.Controls.Add(Me.GroupBox16)
        Me.TabPage6.Location = New System.Drawing.Point(4, 27)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Size = New System.Drawing.Size(363, 726)
        Me.TabPage6.TabIndex = 2
        Me.TabPage6.Text = "ETC"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'GroupBox16
        '
        Me.GroupBox16.Controls.Add(Me.TXT_EXCEL_PATH)
        Me.GroupBox16.Controls.Add(Me.TXT_BOM_SELECTION22)
        Me.GroupBox16.Controls.Add(Me.TXT_PALLET_TMP)
        Me.GroupBox16.Controls.Add(Me.TXT_BOM_EXCEL)
        Me.GroupBox16.Controls.Add(Me.TXT_CONST_EXCEL)
        Me.GroupBox16.Controls.Add(Me.TXT_EXCEL_FILE_NAME)
        Me.GroupBox16.Controls.Add(Me.TXT_EXCEL_PATH2)
        Me.GroupBox16.Controls.Add(Me.Label63)
        Me.GroupBox16.Controls.Add(Me.Label62)
        Me.GroupBox16.Controls.Add(Me.Label42)
        Me.GroupBox16.Controls.Add(Me.Label39)
        Me.GroupBox16.Controls.Add(Me.Label38)
        Me.GroupBox16.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox16.Name = "GroupBox16"
        Me.GroupBox16.Size = New System.Drawing.Size(350, 713)
        Me.GroupBox16.TabIndex = 15
        Me.GroupBox16.TabStop = False
        Me.GroupBox16.Text = "관리자 메뉴"
        '
        'TXT_EXCEL_PATH
        '
        Me.TXT_EXCEL_PATH.Location = New System.Drawing.Point(9, 73)
        Me.TXT_EXCEL_PATH.Name = "TXT_EXCEL_PATH"
        Me.TXT_EXCEL_PATH.Size = New System.Drawing.Size(333, 27)
        Me.TXT_EXCEL_PATH.TabIndex = 26
        Me.TXT_EXCEL_PATH.Text = "\\Dyps-r2017x\AUTOMATION\DET"
        '
        'TXT_BOM_SELECTION22
        '
        Me.TXT_BOM_SELECTION22.Location = New System.Drawing.Point(9, 436)
        Me.TXT_BOM_SELECTION22.Name = "TXT_BOM_SELECTION22"
        Me.TXT_BOM_SELECTION22.Size = New System.Drawing.Size(333, 27)
        Me.TXT_BOM_SELECTION22.TabIndex = 22
        Me.TXT_BOM_SELECTION22.Text = "DET_BOM.xlsx"
        '
        'TXT_PALLET_TMP
        '
        Me.TXT_PALLET_TMP.Location = New System.Drawing.Point(9, 345)
        Me.TXT_PALLET_TMP.Name = "TXT_PALLET_TMP"
        Me.TXT_PALLET_TMP.Size = New System.Drawing.Size(333, 27)
        Me.TXT_PALLET_TMP.TabIndex = 22
        '
        'TXT_BOM_EXCEL
        '
        Me.TXT_BOM_EXCEL.Location = New System.Drawing.Point(9, 223)
        Me.TXT_BOM_EXCEL.Name = "TXT_BOM_EXCEL"
        Me.TXT_BOM_EXCEL.Size = New System.Drawing.Size(333, 27)
        Me.TXT_BOM_EXCEL.TabIndex = 22
        Me.TXT_BOM_EXCEL.Text = "DET_BOM.xlsx"
        '
        'TXT_CONST_EXCEL
        '
        Me.TXT_CONST_EXCEL.Location = New System.Drawing.Point(9, 173)
        Me.TXT_CONST_EXCEL.Name = "TXT_CONST_EXCEL"
        Me.TXT_CONST_EXCEL.Size = New System.Drawing.Size(333, 27)
        Me.TXT_CONST_EXCEL.TabIndex = 23
        Me.TXT_CONST_EXCEL.Text = "DET_Constraints.xlsx"
        '
        'TXT_EXCEL_FILE_NAME
        '
        Me.TXT_EXCEL_FILE_NAME.Location = New System.Drawing.Point(9, 123)
        Me.TXT_EXCEL_FILE_NAME.Name = "TXT_EXCEL_FILE_NAME"
        Me.TXT_EXCEL_FILE_NAME.Size = New System.Drawing.Size(333, 27)
        Me.TXT_EXCEL_FILE_NAME.TabIndex = 24
        Me.TXT_EXCEL_FILE_NAME.Text = "DET.xlsx"
        '
        'TXT_EXCEL_PATH2
        '
        Me.TXT_EXCEL_PATH2.Location = New System.Drawing.Point(9, 43)
        Me.TXT_EXCEL_PATH2.Name = "TXT_EXCEL_PATH2"
        Me.TXT_EXCEL_PATH2.Size = New System.Drawing.Size(333, 27)
        Me.TXT_EXCEL_PATH2.TabIndex = 25
        Me.TXT_EXCEL_PATH2.Text = "c:\tmp"
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Location = New System.Drawing.Point(6, 416)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(134, 17)
        Me.Label63.TabIndex = 18
        Me.Label63.Text = "- BOM 출력 대상"
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.Location = New System.Drawing.Point(6, 203)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(105, 17)
        Me.Label62.TabIndex = 18
        Me.Label62.Text = "- BOM_Excel"
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(6, 153)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(140, 17)
        Me.Label42.TabIndex = 19
        Me.Label42.Text = "- Constraints_List"
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(6, 103)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(97, 17)
        Me.Label39.TabIndex = 20
        Me.Label39.Text = "- Basic_Info"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(6, 23)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(103, 17)
        Me.Label38.TabIndex = 21
        Me.Label38.Text = "- Excel_Path"
        '
        'Main_Form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(120.0!, 120.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.CancelButton = Me.BTN_EXIT
        Me.ClientSize = New System.Drawing.Size(1433, 793)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.TabControl1)
        Me.Font = New System.Drawing.Font("굴림", 10.0!)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "Main_Form"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "ISPark Automation"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.GroupBox17.ResumeLayout(False)
        Me.GroupBox17.PerformLayout()
        Me.GBX_FREECELL.ResumeLayout(False)
        Me.GBX_FREECELL.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox13.ResumeLayout(False)
        Me.GroupBox13.PerformLayout()
        Me.GBX_BOTTOM.ResumeLayout(False)
        Me.GBX_BOTTOM.PerformLayout()
        Me.GroupBox14.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        Me.GBX_SHEAVE_2.ResumeLayout(False)
        Me.GBX_SHEAVE_2.PerformLayout()
        Me.GBX_SHEAVE_1.ResumeLayout(False)
        Me.GBX_SHEAVE_1.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox11.ResumeLayout(False)
        Me.GroupBox11.PerformLayout()
        Me.GroupBox10.ResumeLayout(False)
        Me.GroupBox10.PerformLayout()
        Me.GroupBox9.ResumeLayout(False)
        Me.GroupBox9.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.GroupBox20.ResumeLayout(False)
        Me.GroupBox18.ResumeLayout(False)
        Me.GroupBox18.PerformLayout()
        Me.GroupBox15.ResumeLayout(False)
        Me.GroupBox15.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.TabControl2.ResumeLayout(False)
        Me.TabPage4.ResumeLayout(False)
        Me.GroupBox24.ResumeLayout(False)
        Me.GroupBox24.PerformLayout()
        Me.TabPage5.ResumeLayout(False)
        Me.GroupBox12.ResumeLayout(False)
        Me.GroupBox12.PerformLayout()
        Me.TabPage6.ResumeLayout(False)
        Me.GroupBox16.ResumeLayout(False)
        Me.GroupBox16.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents LBL_State As System.Windows.Forms.Label
    Friend WithEvents LBL_State_tmp As System.Windows.Forms.Label
    Friend WithEvents BTN_EXIT As System.Windows.Forms.Button
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents BTN_MACHINE_POSITION As System.Windows.Forms.Button
    Friend WithEvents BTN_MACHINE_CREATE As System.Windows.Forms.Button
    Friend WithEvents GroupBox11 As System.Windows.Forms.GroupBox
    Friend WithEvents TXB_TURN_Z As System.Windows.Forms.TextBox
    Friend WithEvents TXB_TURN_Y As System.Windows.Forms.TextBox
    Friend WithEvents TXB_TURN_X As System.Windows.Forms.TextBox
    Friend WithEvents CMB_TURN_1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents GroupBox10 As System.Windows.Forms.GroupBox
    Friend WithEvents TXB_WEIG_Z As System.Windows.Forms.TextBox
    Friend WithEvents TXB_WEIG_Y As System.Windows.Forms.TextBox
    Friend WithEvents TXB_WEIG_X As System.Windows.Forms.TextBox
    Friend WithEvents CMB_WEIG_1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
    Friend WithEvents TXB_PALL_Z As System.Windows.Forms.TextBox
    Friend WithEvents TXB_PALL_Y As System.Windows.Forms.TextBox
    Friend WithEvents TXB_PALL_X As System.Windows.Forms.TextBox
    Friend WithEvents CMB_PALL As System.Windows.Forms.ComboBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents GroupBox14 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox20 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox17 As System.Windows.Forms.GroupBox
    Friend WithEvents TXT_TOP_RV As System.Windows.Forms.TextBox
    Friend WithEvents TXT_TOP_SD As System.Windows.Forms.TextBox
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents GroupBox13 As System.Windows.Forms.GroupBox
    Friend WithEvents BTN_CALCULATION As System.Windows.Forms.Button
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents TXT_BOTTOM_RV As System.Windows.Forms.TextBox
    Friend WithEvents TXT_BOTTOM_SD As System.Windows.Forms.TextBox
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents GBX_BOTTOM As System.Windows.Forms.GroupBox
    Friend WithEvents RBN_BOTTOM_RV As System.Windows.Forms.RadioButton
    Friend WithEvents RBN_BOTTOM_SD As System.Windows.Forms.RadioButton
    Friend WithEvents GBX_FREECELL As System.Windows.Forms.GroupBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents RBN_FREECELL_N As System.Windows.Forms.RadioButton
    Friend WithEvents RBN_FREECELL_Y As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents TXB_TRAC_Z As System.Windows.Forms.TextBox
    Friend WithEvents TXB_TRAC_Y As System.Windows.Forms.TextBox
    Friend WithEvents TXB_TRAC_X As System.Windows.Forms.TextBox
    Friend WithEvents CMB_TRAC_2 As System.Windows.Forms.ComboBox
    Friend WithEvents CMB_TRAC_1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents BTN_BASIC_INFO As System.Windows.Forms.Button
    Friend WithEvents CMB_TURN_2 As System.Windows.Forms.ComboBox
    Friend WithEvents CMB_WEIG_2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents BTN_TEMPLATE_OPEN As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TabControl2 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox24 As System.Windows.Forms.GroupBox
    Friend WithEvents TXT_RESULT As System.Windows.Forms.TextBox
    Friend WithEvents TXT_PIT As System.Windows.Forms.TextBox
    Friend WithEvents TXT_GH As System.Windows.Forms.TextBox
    Friend WithEvents TXT_EH As System.Windows.Forms.TextBox
    Friend WithEvents TXT_BM As System.Windows.Forms.TextBox
    Friend WithEvents TXT_OH As System.Windows.Forms.TextBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents BASEMENT As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox12 As System.Windows.Forms.GroupBox
    Friend WithEvents LBX_Combobox_Sort As System.Windows.Forms.ListBox
    Friend WithEvents TXT_IDE2 As System.Windows.Forms.TextBox
    Friend WithEvents TXT_IDE1 As System.Windows.Forms.TextBox
    Friend WithEvents TXT_WEIG As System.Windows.Forms.TextBox
    Friend WithEvents TXT_TRAC As System.Windows.Forms.TextBox
    Friend WithEvents TXT_TURN As System.Windows.Forms.TextBox
    Friend WithEvents TXT_PALL As System.Windows.Forms.TextBox
    Friend WithEvents TXT_LIFT As System.Windows.Forms.TextBox
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents Label61 As System.Windows.Forms.Label
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox16 As System.Windows.Forms.GroupBox
    Friend WithEvents TXT_PALLET_VIRTUAL As System.Windows.Forms.TextBox
    Friend WithEvents TXT_CAR_RV As System.Windows.Forms.TextBox
    Friend WithEvents TXT_CAR_SD As System.Windows.Forms.TextBox
    Friend WithEvents TXT_FMIT As System.Windows.Forms.TextBox
    Friend WithEvents Label68 As System.Windows.Forms.Label
    Friend WithEvents Label69 As System.Windows.Forms.Label
    Friend WithEvents Label70 As System.Windows.Forms.Label
    Friend WithEvents TXT_BOM_EXCEL As System.Windows.Forms.TextBox
    Friend WithEvents TXT_CONST_EXCEL As System.Windows.Forms.TextBox
    Friend WithEvents TXT_EXCEL_FILE_NAME As System.Windows.Forms.TextBox
    Friend WithEvents TXT_EXCEL_PATH2 As System.Windows.Forms.TextBox
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents TXT_BOM_SELECTION22 As System.Windows.Forms.TextBox
    Friend WithEvents Label63 As System.Windows.Forms.Label
    Friend WithEvents TXT_PIT_S As System.Windows.Forms.TextBox
    Friend WithEvents TXT_CAGE_S As System.Windows.Forms.TextBox
    Friend WithEvents Label66 As System.Windows.Forms.Label
    Friend WithEvents GroupBox18 As System.Windows.Forms.GroupBox
    Friend WithEvents BTN_BOM_CREATE As System.Windows.Forms.Button
    Friend WithEvents BTN_BOM_SELECTION As System.Windows.Forms.Button
    Friend WithEvents TXT_BOM_SELECTION As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox15 As System.Windows.Forms.GroupBox
    Friend WithEvents RBN_BOM_OPT2 As System.Windows.Forms.RadioButton
    Friend WithEvents RBN_BOM_OPT1 As System.Windows.Forms.RadioButton
    Friend WithEvents TXT_EXCEL_PATH As System.Windows.Forms.TextBox
    Friend WithEvents Label65 As System.Windows.Forms.Label
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents TXB_LIFT_Z As System.Windows.Forms.TextBox
    Friend WithEvents TXB_LIFT_Y As System.Windows.Forms.TextBox
    Friend WithEvents TXB_LIFT_X As System.Windows.Forms.TextBox
    Friend WithEvents CMB_LIFT_3 As System.Windows.Forms.ComboBox
    Friend WithEvents CMB_LIFT_2 As System.Windows.Forms.ComboBox
    Friend WithEvents CMB_LIFT_1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents CMB_LIFT_5 As System.Windows.Forms.ComboBox
    Friend WithEvents CMB_LIFT_4 As System.Windows.Forms.ComboBox
    Friend WithEvents TXT_PALLET_TMP As System.Windows.Forms.TextBox
    Friend WithEvents GBX_SHEAVE_2 As System.Windows.Forms.GroupBox
    Friend WithEvents TXB_IDLE2_Z As System.Windows.Forms.TextBox
    Friend WithEvents TXB_IDLE2_Y As System.Windows.Forms.TextBox
    Friend WithEvents TXB_IDLE2_X As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents CMB_IDLE_2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents GBX_SHEAVE_1 As System.Windows.Forms.GroupBox
    Friend WithEvents TXB_IDLE1_Z As System.Windows.Forms.TextBox
    Friend WithEvents TXB_IDLE1_Y As System.Windows.Forms.TextBox
    Friend WithEvents TXB_IDLE1_X As System.Windows.Forms.TextBox
    Friend WithEvents CMB_IDLE_1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents TXT_ENTER_S As System.Windows.Forms.TextBox
    Friend WithEvents Label64 As System.Windows.Forms.Label
    Friend WithEvents TXT_SKEL As System.Windows.Forms.TextBox
    Friend WithEvents TXT_ASSY As System.Windows.Forms.TextBox
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label

End Class
